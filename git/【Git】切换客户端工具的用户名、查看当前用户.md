# 【Git】切换客户端工具的用户名、查看当前用户

切换Git客户端连接工具的登录账号，需要到控制面板那修改

![img](https://img-blog.csdnimg.cn/20190830145930192.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTIzNzMyODE=,size_16,color_FFFFFF,t_70)

 

![img](https://img-blog.csdnimg.cn/20190830150015110.png)

重新编辑，输入新的用户名和密码就可以了

客户端可能需要重启下，我自己试的时候，重启才生效。

还要设置下邮箱和用户名

git config --global user.name "username"

git config --global user.email "email"

查看当前用户和邮箱

git config user.name

git config user.email