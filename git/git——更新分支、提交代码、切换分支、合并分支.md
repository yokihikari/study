[**git——更新分支、提交代码、切换分支、合并分支**](https://www.cnblogs.com/gaoquanquan/p/9501981.html)

　　还是直接贴教程吧：https://git-scm.com/book/zh/v2

 如何把本地idea上的项目上传到github上：https://www.cnblogs.com/gougouyangzi/articles/9429280.html

1.owner在远程库更新了分支，但是在我这里git branch -a查看不到新的分支，

　　解决办法：git fetch origin --prune 更新一下分支信息，然后再git branch -a就能看到新的分支了~

2.提交代码到远程库：

　　git status             # 查看本地代码状态

　　git add .              # 添加修改代码到缓存

　　git commit -m "一些信息"  # 提交

　　git push 仓库地址        # push进去了！

3.切换分支：

　　git checkout -b 分支名 #新建分支

　　git branch -a

　　git checkout 分支名

4.多人协作的工作模式通常是这样：

1. 首先，可以试图用git push origin <branch-name>推送自己的修改；
2. 如果推送失败，则因为远程分支比你的本地更新，需要先用git pull试图合并；
3. 如果合并有冲突，则解决冲突，并在本地提交；
4. 没有冲突或者解决掉冲突后，再用git push origin <branch-name>推送就能成功！

　　 5. 如果git pull提示no tracking information，则说明本地分支和远程分支的链接关系没有创建，用命令git branch --set-upstream-to <branch-name> origin/<branch-name>。

5.将本地代码与远程库关联

　　先要在github上新建一个项目

　　然后 git push -u 项目地址 分支名称