搜索ElasticSearch镜像

`docker search elasticsearch`

![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tLnFmY3d4LnRvcC9kb2NrZXIvMy8xLmpwZw?x-oss-process=image/format,png)

使用上面的命令即可搜索所有的Docker镜像
拉取镜像

```
docker pull docker.io/elasticsearch:版本号
```

拉取镜像的时候，可以指定版本，如果不指定，默认使用latest。
查看镜像

```
docker images
```

运行容器

```
docker run -d --name es2 -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" 5acf0e8da90b
```


通过镜像启动容器，然后查看容器。

```
docker ps   # 列出正在运行的容器
docker ps -a    # 列出所有容器，包括未运行的
```


明明启动了容器，为什么会自动退出了呢？这里通过查看日志来看看是什么原因。

```
docker logs 676164fb24b3   	# 查看某个容器的日志
```

![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tLnFmY3d4LnRvcC9kb2NrZXIvMy81LnBuZw?x-oss-process=image/format,png)

经过相关资料证实，这是因为内存不足导致的。所以在启动的时候，调整内存的分配，重新启动。

```
“ES_JAVA_OPTS=-Xms512m -Xmx512m”
```

![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tLnFmY3d4LnRvcC9kb2NrZXIvMy82LnBuZw?x-oss-process=image/format,png)

通过上图可知，容器已经启动完成。现在就可以在浏览器输入 ip + 端口号 访问了。

![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tLnFmY3d4LnRvcC9kb2NrZXIvMy83LnBuZw?x-oss-process=image/format,png)

配置跨域

通过项目中的配置，发现 cluster.name 这个与刚刚启动的不一致，ElasticSearch默认的cluster.name为elasticsearch，所以，这里需要进行修改。
首先进入容器。

```
docker exec -it es2 /bin/bash
```

![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tLnFmY3d4LnRvcC9kb2NrZXIvMy84LnBuZw?x-oss-process=image/format,png)

我们需要对红框框圈中的文件"elasticsearch.yml"进行修改。

![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tLnFmY3d4LnRvcC9kb2NrZXIvMy85LmpwZw?x-oss-process=image/format,png)

这里使用vim和vi命令，都提示"没有发现这个命令"，这是因为Docker容器内部没有安装。所以，这里需要进行安装。

```
apt-get update  # 获取最新的软件包
apt-get install vim   # 下载
```

依次使用上面的命令即可安装成功，然后对"elasticsearch.yml"这个文件修改，增加如下内容。

```
cluster.name: "qfcwx-cluster"   
network.host: 0.0.0.0
http.cors.enabled: true
http.cors.allow-origin: "*"
```

![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tLnFmY3d4LnRvcC9kb2NrZXIvMy8xMC5wbmc?x-oss-process=image/format,png)

cluster.name：自定义集群名称。
network.host：当前es节点绑定的ip地址，默认127.0.0.1，如果需要开放对外访问这个属性必须设置。
http.cors.enabled：是否支持跨域，默认为false。
http.cors.allow-origin：当设置允许跨域，默认为*，表示支持所有域名，如果我们只是允许某些网站能访问，那么可以使用正则表达式。
重启ElasticSearch容器

使用exit命令从容器内部退出。

注意，这里是重启ElasticSearch容器，并不是重新启动一个新的容器。

```
docker restart es2
```



使用浏览器进行访问。

![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tLnFmY3d4LnRvcC9kb2NrZXIvMy8xMS5wbmc?x-oss-process=image/format,png)

可以看到集群的名称已经变成我们自己设置的了。
查看ElasticSearch内部信息

如果没有指定版本号，就从Docker镜像仓库中拉取镜像的话，你是不知道版本号的。如果在Java中操作ElasticSearch，需要导入相应的约束或者jar包。这就需要和ElasticSearch版本号对应，不然就会报莫名的异常，所以，就需要查看当前ElasticSearch的本版号了，这里有两种方法。

①、通过 ip+端口号 可以看到返回的json数据中，version下面的number就是版本号。
②、通过查看ElasticSearch容器内部信息。

```
docker inspect 5acf0e8da90b     # 查看容器内部信息
```

![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tLnFmY3d4LnRvcC9kb2NrZXIvMy8xMy5qcGc?x-oss-process=image/format,png)

ElasticSearch Head管理界面

![img](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tLnFmY3d4LnRvcC9kb2NrZXIvMy8xMi5wbmc?x-oss-process=image/format,png)

可以通过Docker拉取一个ElasticSearch Head镜像，但是感觉很麻烦，所以这里可以使用谷歌浏览器插件：ElasticSearch Head
通过谷歌应用商店即可安装。安装完成后，就能查看当前集群的信息。

讲到这里，关于Docker如何安装ElasticSearch就讲完了，至于ElasticSearch怎么操作，可以参考官方文档或下面的文章：

    https://blog.csdn.net/u014073556/article/details/80654099