最近尝试用springalibaba的Sentinel进行流量监控，发现无法监控到端口。最终找到了原因和解决方案。

相关配置如下：
**application.yml**

![springcloudalibaba限流降级sentinel无法监控到接口问题，实时监控没有接口显示-LMLPHP](https://c1.lmlphp.com/user/master/2020/08/30/son_1/48f42965f44c86068e87e567726920e4.xjpg)
**pom.xml文件：**
![springcloudalibaba限流降级sentinel无法监控到接口问题，实时监控没有接口显示-LMLPHP](https://c1.lmlphp.com/user/master/2020/08/30/son_1/44385213582efa301333ee66ca553d5a.xjpg)
**Controller:**
![springcloudalibaba限流降级sentinel无法监控到接口问题，实时监控没有接口显示-LMLPHP](https://c1.lmlphp.com/user/master/2020/08/30/son_1/ab91be777076ab7e3df205bc7508f0ad.xjpg)







# 原因：

原因是由于Controller下面的请求地址有两个或者以上层级， 导致无法被监控到（为啥如此暂时未知，如有知道的大神还请评论告知）



# 解决方案：

请求只设置一个层级，如下图：
![springcloudalibaba限流降级sentinel无法监控到接口问题，实时监控没有接口显示-LMLPHP](https://c1.lmlphp.com/user/master/2020/08/30/son_1/1499b2af620f2d924ff1b4758202d78b.xjpg)



### 此时请求，test1没法实时监控，test2可以监控到，如图：

![springcloudalibaba限流降级sentinel无法监控到接口问题，实时监控没有接口显示-LMLPHP](https://c1.lmlphp.com/user/master/2020/08/30/son_1/7d1d408c35b84ebffead48a13086dfe5.xjpg)



### 最后将test1请求接口也改成一层级，发现也可以被监控到了。

test1的请求接口由二层级"/test/test1"改成一层级"/test"

![springcloudalibaba限流降级sentinel无法监控到接口问题，实时监控没有接口显示-LMLPHP](https://c1.lmlphp.com/user/master/2020/08/30/son_1/2374fbade354e4502715a1549a64b261.xjpg)
![springcloudalibaba限流降级sentinel无法监控到接口问题，实时监控没有接口显示-LMLPHP](https://c1.lmlphp.com/user/master/2020/08/30/son_1/5853f3127e33cbb90a1c08497f870b57.xjpg)



# 结论：

**如果不加@SentinelResource注解， 当请求接口为2层级或者更多层级时， sentinel无法对接口进行实时监控， 只有1层级才能实时监控到。**

本文链接: [https://blog.csdn.net/qq_42813386/article/details/108286072](https://www.lmlphp.com/r?x=z7wm8D4x%2F%2BnBcAFaMxu2sjxmgVL5UmHFt0zNzxxMo9c5dfQvIiN4rfrbzy1Jnlei0Qw2sc%2F99rDOsxyiCeRJvpv%2BxLUePnm%2BHdcd0XeJCunkmM5lP5r2BaDkGB5j8ue%2F).
