大家在使用sentinel过程中如果出现Failed to fetch metric from 的错误,具体表现如下：

```
Failed to fetch metric from <http://192.168.136.1:8719/metric?startTime=1563865044000&endTime=1563865050000&refetch=false>
 (ConnectionException: Connection refused: no further information)
```

这个时候你需要去检查下sentinel控制台的服务列表，确认是否跟你ip一致,还有port8719端口是否开放（我之前是装过虚拟机，sentinel一直抓取的是我虚拟的ip，不知道为什么。。。）

如果发现监听的地址不对的话，可以在sentinel客户端配置中加入客户端ip配置

```
spring:
  cloud:
    sentinel:
      transport:
        client-ip: 192.168.0.108
```

