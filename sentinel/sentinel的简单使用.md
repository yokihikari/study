##### 2. `feign`集成`sentinel`

[spring-cloud-alibaba-sentinel github 官方文档](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Falibaba%2Fspring-cloud-alibaba%2Fblob%2Fmaster%2Fspring-cloud-alibaba-examples%2Fsentinel-example%2Fsentinel-feign-example%2Freadme-zh.md)

1. 核心依赖

   1. 依赖

      

      ```xml
      <dependencies>
          <dependency>
              <groupId>com.alibaba.cloud</groupId>
              <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
          </dependency>
      </dependencies>
      ```

   2. `spring-cloud-alibaba-sentinel`的父依赖

      

      ```xml
      <properties>
           <spring-cloud-alibaba.version>2.2.1.RELEASE</spring-cloud-alibaba.version>
      </properties>
      
      <dependencyManagement>
          <!-- spring-cloud-alibaba 父依赖 -->
          <dependency>
              <groupId>com.alibaba.cloud</groupId>
              <artifactId>spring-cloud-alibaba-dependencies</artifactId>
              <version>${spring-cloud-alibaba.version}</version>
              <type>pom</type>
              <scope>import</scope>
          </dependency>
      </dependencyManagement>    
      ```

2. 配置文件

   1. 提供者`sentinel-provider`的`application.yml`主配置文件及启动类

      1. `application.yml`

         

         ```yml
         server:
           # 项目访问端口号
           port: 8081
         spring:
           application:
             # 注册到注册中心的服务名
             name: sentinel-provider
           cloud:
             nacos:
               discovery:
                 # 注册中心的地址
                 server-addr: 120.25.207.44:8848
             sentinel:
               transport:
                 # 推送数据，sentinel熔断器的地址
                 dashboard: 120.25.207.44:8217
                 # 接收数据，当在 sentinel 的面板中配置一些参数，会通过该端口传送过来
                 port: 8719
         ```

      2. 启动类

         

         ```java
         import org.springframework.boot.SpringApplication;
         import org.springframework.boot.autoconfigure.SpringBootApplication;
         import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
         
         // 开启 nacos 注册中心扫描，发现该服务
         @EnableDiscoveryClient
         @SpringBootApplication
         public class SentinelProviderApplication {
             public static void main(String[] args) {
                 SpringApplication.run(SentinelProviderApplication.class, args);
             }
         }
         ```

   2. 消费者`sentinel-consumer`的`application.yml`主配置文件及启动类

      1. `application.yml`

         

         ```yml
         server:
           # 项目访问端口
           port: 8082
           servlet:
             context-path: /api/v1
         spring:
           application:
             # 注册到注册中心的服务名
             name: sentinel-consumer
           cloud:
             nacos:
               discovery:
                 # 注册中心的地址
                 server-addr: 120.25.207.44:8848
                 # 不将自己注册到注册中心
                 register-enabled: false
             sentinel:
               transport:
                 # 推送数据，sentinel熔断器的地址
                 dashboard: 120.25.207.44:8217
                 # 接收数据，当在 sentinel 的面板中配置一些参数，会通过该端口传送过来
                 port: 8720
         feign:
           okhttp:
             # 开启 okhttp，性能最好
             enabled: true
           sentinel:
             # 开启 feign 对 sentinel 的支持
             enabled: true
         ```

      2. 启动类

         

         ```java
         import org.springframework.boot.SpringApplication;
         import org.springframework.boot.autoconfigure.SpringBootApplication;
         import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
         import org.springframework.cloud.openfeign.EnableFeignClients;
         
         // 开启 feign 的客户端
         @EnableFeignClients
         // 开启 nacos 注册中心扫描，发现该服务
         @EnableDiscoveryClient
         @SpringBootApplication
         public class SentinelConsumerApplication {
             public static void main(String[] args) {
                 SpringApplication.run(SentinelConsumerApplication.class, args);
             }
         }
         ```

3. `feign`集成`sentinel`栗子

   1. 提供者`sentinel-provider`的`ProviderController`

      

      ```java
      import org.springframework.web.bind.annotation.*;
      
      @RestController
      @RequestMapping("/provider")
      public class ProviderController {
          @GetMapping("/list/{param}")
          public String list(@PathVariable String param) {
              return "provider_list: " + param;
          }
      
          @GetMapping("/test")
          public String test(String param) {
              return "provider_test: " + param;
          }
      }
      ```

   2. 消费者`sentinel-consumer`

      1. 远程调用类`ProviderService`

         

         ```java
         import com.sheng.cloud.consumer.service.fallback.ProviderServiceFallBackImpl;
         import org.springframework.cloud.openfeign.FeignClient;
         import org.springframework.stereotype.Service;
         import org.springframework.web.bind.annotation.GetMapping;
         import org.springframework.web.bind.annotation.PathVariable;
         import org.springframework.web.bind.annotation.RequestParam;
         
         /**
          * 说明：FeignClient(value = "sentinel-provider", path = "/provider", , fallback = ProviderServiceFallBackImpl.class)
          * value：提供者注册到注册中心的服务名，path：访问路径，fallback：熔断之后的服务降级类
          *
          * @author sheng
          */
         @Service
         @FeignClient(value = "sentinel-provider", path = "/provider", fallback = ProviderServiceFallBackImpl.class)
         public interface ProviderService {
             /**
              * 对应提供者的 public String list(@PathVariable String param){}
              *
              * @param param 测试参数
              * @return 字符串 provider_list: param
              */
             @GetMapping("/list/{param}")
             String list(@PathVariable String param);
         
             /**
              * 对应 提供者的 public String test(String param){}
              *
              * @param param 测试参数，在这里必须用@RequestParam修饰，默认在请求体中传递
              * @return 字符串 provider_test: param
              */
             @GetMapping("/test")
             String test(@RequestParam String param);
         
         }
         ```

      2. 服务降级类`ProviderServiceFallBackImpl`

         [官方文档](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Falibaba%2FSentinel%2Fwiki%2F%E7%86%94%E6%96%AD%E9%99%8D%E7%BA%A7)

         

         ```java
         import com.sheng.cloud.consumer.service.ProviderService;
         import org.springframework.stereotype.Component;
         
         /**
          * 说明：必须注册到容器中，不然报错
          *
          * @author sheng
          */
         @Component
         public class ProviderServiceFallBackImpl implements ProviderService {
             @Override
             public String list(String param) {
                 return "熔断了： fallBack_list: " + param;
             }
         
             @Override
             public String test(String param) {
                 return "熔断了： fallBack_test: " + param;
             }
         }
         ```

      3. `ConsumerController`

         

         ```java
         import com.sheng.cloud.consumer.service.ProviderService;
         import org.springframework.web.bind.annotation.GetMapping;
         import org.springframework.web.bind.annotation.PathVariable;
         import org.springframework.web.bind.annotation.RequestMapping;
         import org.springframework.web.bind.annotation.RestController;
         
         import javax.annotation.Resource;
         
         /**
          * 说明：
          *
          * @author sheng
          */
         @RestController
         @RequestMapping("/consumer")
         public class ConsumerController {
             @Resource
             private ProviderService providerService;
         
             @GetMapping("/list/{param}")
             public String list(@PathVariable String param) {
                 return providerService.list(param);
             }
         
             @GetMapping("/test")
             public String test(String param) {
                 return providerService.test(param);
             }
         }
         ```

4. 结果

   1. 正常运行

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-1af8b77b0eec709f?imageMogr2/auto-orient/strip|imageView2/2/w/964/format/webp)

      正常运行

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-ddc756146d45acb3?imageMogr2/auto-orient/strip|imageView2/2/w/970/format/webp)

      正常运行

   2. 提供者`sentinel-provider`项目`停止运行`后，走了`熔断类`

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-b396b23600e626f8?imageMogr2/auto-orient/strip|imageView2/2/w/997/format/webp)

      提供者停止运行

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-3debcc471f087fbd?imageMogr2/auto-orient/strip|imageView2/2/w/971/format/webp)

      提供者停止运行

5. [sentinel-demo 源码](https://links.jianshu.com/go?to=https%3A%2F%2Fgitee.com%2Frmy_20%2Fsentinel-demo.git)

##### 3. 控制面板：[官方文档](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Falibaba%2FSentinel%2Fwiki%2F%E6%B5%81%E9%87%8F%E6%8E%A7%E5%88%B6)

1. `windows`中启动`sentinel`

   1. `java -Dserver.port=8217 -Dcsp.sentinel.dashboard.server=8217 -Dproject.name=sentinel-dashboard -jar sentinel-dashboard-1.7.2.jar`
   2. `sentinel`默认端口为`8080`，改端口为`8217`

   ![img](https:////upload-images.jianshu.io/upload_images/9372781-3dd7dc82b54d5ed7?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

   启动sentinel

2. 浏览器访问

   1. 地址：`127.0.0.1:8217`或`localhost:8217`

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-26a3e79507be422f?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

      访问

   2. `账号/密码`：`sentinel`

   ![img](https:////upload-images.jianshu.io/upload_images/9372781-3731da9ad364b2ab?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

   控制面板

3. 启动项目，实时监控

   1. 启动项目后`立刻`刷新`sentinel`控制面板，并不会有监控，原因在于没有访问项目的资源

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-a6263e5f619831d9?imageMogr2/auto-orient/strip|imageView2/2/w/951/format/webp)

      启动项目

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-4ae92abd24527f3a?imageMogr2/auto-orient/strip|imageView2/2/w/934/format/webp)

      监控面板

   2. 访问资源，查看监控

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-31d432b2ffcb6a2a?imageMogr2/auto-orient/strip|imageView2/2/w/1065/format/webp)

      访问资源

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-f5d0adbdcaffc211?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

      实时监控

4. 控制面板的功能

   1. **`实时监控`**

      1. 用于查看接口调用的`时间`、`QPS(Query Per Second)`、`平均响应时间`

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-adef9c8ded8c6e64?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

      实时监控

   2. `簇点链路`

      1. 查看当前追踪的所有的访问接口
      2. 可以给接口添加`流控规则`、`降级规则`、`热点规则`、`授权规则`

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-20d99fc8621a262b?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

      簇点链路

   3. `流控规则`

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-9844ce8bd52ac3c7?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

      新增流控规则

      1. ```
         资源名
         ```

         1. 需要流控的资源的名字
         2. 例如：`/consumer/list/{param}`

      2. ```
         针对来源
         ```

         1. 默认为`default`，表示所有
         2. 可以设置为特定的服务

      3. ```
         阈值类型
         ```

         1. 指对该资源如何进行限制
         2. 两种选择：`QPS` 或 `线程`

      4. ```
         单机阈值
         ```

         1. 对应在`阈值类型`的选择
         2. 指控制`QPS` 或 `线程`的数量

      5. ```
         流控模式
         ```

         1. `直接`：表示对指定资源进行限制
         2. `链路`：当被关联的资源达到`阈值`的时候，指定资源将会被限制访问
         3. `链路`：是更加细粒度的控制，控制指定资源对链路的限制

      6. ```
         流控效果
         ```

         1. `快速失败`：当`无法访问`的时候`立刻`给用户一个`错误`响应
         2. `Warm Up`：即`预热`，指经过`指定的时间`后才达到指定的`阈值`(初始的`QPS`从`阈值 / 3`开始，经过预热的时长`逐渐`提升到指定的`QPS阈值`)
         3. `排队等待`：指`匀速`的通过`每秒指定的QPS`，其他的请求进行`排队`，但是并`不会`一直排下去，`超时`就会失效。**`阈值`类型必须为`QPS`**

   4. `降级规则`

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-0844b023879b3249?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

      降级规则

      1. ```
         资源名
         ```

         1. 需要`降级`的资源名
         2. 如 `/consumer/list/{param}`

      2. ```
         降级策略
         ```

         1. `RT`

            1. 平均响应时间
            2. 如果`1秒钟`之内进入的请求的`平均响应时间`大于设置的`RT`值(单位为`毫秒`)，那么在指定的`时间窗口`内(`降级时间间隔`，单位为`秒`)的所有的请求都会熔断降级。

         2. `异常比例`

            1. 如果`1秒钟`之内进入的`请求数`的`异常比例`大于指定的`异常比例`(数值区间为 `0.0 ~ 1.0`)，那么在指定的`时间窗口`内(`降级时间间隔`，单位为`秒`)的所有的请求都会熔断降级。

            ![img](https:////upload-images.jianshu.io/upload_images/9372781-62b1e632bfd56551?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

            异常比例

         3. `异常数`

            1. 如果`1秒钟`之内，进入的`请求数`的`异常数`大于指定的`异常数`，那么在指定的`时间窗口`内(`降级时间间隔`，单位为`秒`)的所有的请求都会熔断降级。
            2. 注意：`时间窗口`的值一般要 `大于 60`，否则可能会一直处于熔断状态。

            ![img](https:////upload-images.jianshu.io/upload_images/9372781-366c2d68ada0de2c?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

            异常数

   5. `热点规则`

      1. 针对`具体的请求参数`进行设置

         

         ```java
         // @SentinelResource必须要设置
         @SentinelResource("modify")
         @RequestMapping("/modify")
         public String modify(@RequestParam(required = false) String id,
                              @RequestParam(required = false) String status) {
             return "modify_" + id + "_" + password;
         }
         ```

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-43996ba837da05a8?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

      热点规则

      1. `资源名`：要限制方法`@SentinelResource`中设置的值
      2. `参数索引`：具体对`哪一个`参数进行`QPS限制`，通过索引来指定（从`0`开始）。
      3. `单机阈值`：指定在`统计时长`内的`阈值`
      4. `统计窗口时长`：统计`QPS`的时长

   6. `系统规则`

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-7a26dfec1a843624?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

      系统规则

      1. `LOAD`：仅对`Linux/Unix-like` 机器生效，参考值一般是 `CPU cores * 2.5`
      2. `RT`：当`单台机器`上所有入口流量的`平均 RT`达到`阈值`即触发系统保护，单位是`毫秒`
      3. `线程数`：当`单台机器`上所有入口流量的`并发线程数`达到`阈值`即触发系统保护
      4. `入口 QPS`：当`单台机器`上所有入口流量的`QPS`达到`阈值`即触发系统保护

   7. `授权规则`

      1. 指可以将`特定的访问应用`加入`黑名单`或者`白名单`，但是必须在`访问`的时候携带应用的名称

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-0b120558e26827ff?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

      授权规则

   8. `集群流控`

      ![img](https:////upload-images.jianshu.io/upload_images/9372781-bc87609592a19879?imageMogr2/auto-orient/strip|imageView2/2/w/926/format/webp)

      集群流控

      1. `是否集群`：是否采用集群
      2. `均摊阈值`：`每个`集群节点`每秒`的`QPS`
      3. `集群阈值模式`：`单机均摊`是集群中`每个`节点`每秒`的`QPS`, `总体阈值`是`整个集群`每秒的`QPS`