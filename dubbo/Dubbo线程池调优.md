Dubbo通过配置`threadpool`来配置线程池的类型：

1. `fixed`固定大小线程池，启动时建立线程，不关闭，一直持有（缺省）
2. `cached`缓存线程池，空闲一分钟自动删除，需要时重建
3. `limited`可伸缩线程池，但池中的线程数只会增长不会收缩（为避免收缩时突然来了大流量引起的性能问题）

默认是固定大小线程池，如果服务消费者太多时会出现等待，甚至超时：

```
Caused by: com.alibaba.dubbo.remoting.TimeoutException: Waiting server-side response timeout by scan timer. start time: 2016-04-22 10:36:58.471, end time: 2016-04-22 10:37:18.535, client elapsed: 342 ms, server elapsed: 19722 ms, timeout: 20000 ms, request: Request [id=178, version=2.0.0, twoway=true, event=false, broken=false, data=RpcInvocation [methodName=install, parameterTypes=[class com.acgist.Demo, class java.lang.String], arguments=[com.acgist.Demo@7fe5c339, ], attachments={path=com.acgist.IDemo, interface=com.acgist.IDemo, timeout=20000, version=0.0.0}]], channel: /169.254.183.37:60024 -> /169.254.183.37:20882
	at com.alibaba.dubbo.remoting.exchange.support.DefaultFuture.returnFromResponse(DefaultFuture.java:188)
	at com.alibaba.dubbo.remoting.exchange.support.DefaultFuture.get(DefaultFuture.java:110)
	at com.alibaba.dubbo.remoting.exchange.support.DefaultFuture.get(DefaultFuture.java:84)
	at com.alibaba.dubbo.rpc.protocol.dubbo.DubboInvoker.doInvoke(DubboInvoker.java:96)
	at com.alibaba.dubbo.rpc.protocol.AbstractInvoker.invoke(AbstractInvoker.java:144)
	at com.alibaba.dubbo.rpc.listener.ListenerInvokerWrapper.invoke(ListenerInvokerWrapper.java:74)
	at com.alibaba.dubbo.monitor.support.MonitorFilter.invoke(MonitorFilter.java:75)
	at com.alibaba.dubbo.rpc.protocol.ProtocolFilterWrapper$1.invoke(ProtocolFilterWrapper.java:91)
	at com.alibaba.dubbo.rpc.protocol.dubbo.filter.FutureFilter.invoke(FutureFilter.java:53)
	at com.alibaba.dubbo.rpc.protocol.ProtocolFilterWrapper$1.invoke(ProtocolFilterWrapper.java:91)
	at com.alibaba.dubbo.rpc.filter.ConsumerContextFilter.invoke(ConsumerContextFilter.java:48)
	at com.alibaba.dubbo.rpc.protocol.ProtocolFilterWrapper$1.invoke(ProtocolFilterWrapper.java:91)
	at com.alibaba.dubbo.rpc.protocol.InvokerWrapper.invoke(InvokerWrapper.java:53)
	at com.alibaba.dubbo.rpc.cluster.support.FailoverClusterInvoker.doInvoke(FailoverClusterInvoker.java:77)
	... 5 more
```

服务端提示繁忙：

```
com.alibaba.dubbo.remoting.ExecutionException: class com.alibaba.dubbo.remoting.transport.dispatcher.all.AllChannelHandler error when process caught event .
	at com.alibaba.dubbo.remoting.transport.dispatcher.all.AllChannelHandler.caught(AllChannelHandler.java:67)
	at com.alibaba.dubbo.remoting.transport.AbstractChannelHandlerDelegate.caught(AbstractChannelHandlerDelegate.java:44)
	at com.alibaba.dubbo.remoting.transport.AbstractChannelHandlerDelegate.caught(AbstractChannelHandlerDelegate.java:44)
	at com.alibaba.dubbo.remoting.transport.AbstractPeer.caught(AbstractPeer.java:127)
	at com.alibaba.dubbo.remoting.transport.netty.NettyHandler.exceptionCaught(NettyHandler.java:112)
	at com.alibaba.dubbo.remoting.transport.netty.NettyCodecAdapter$InternalDecoder.exceptionCaught(NettyCodecAdapter.java:165)
	at org.jboss.netty.channel.Channels.fireExceptionCaught(Channels.java:432)
	at org.jboss.netty.channel.AbstractChannelSink.exceptionCaught(AbstractChannelSink.java:52)
	at org.jboss.netty.channel.Channels.fireMessageReceived(Channels.java:302)
	at com.alibaba.dubbo.remoting.transport.netty.NettyCodecAdapter$InternalDecoder.messageReceived(NettyCodecAdapter.java:148)
	at org.jboss.netty.channel.Channels.fireMessageReceived(Channels.java:274)
	at org.jboss.netty.channel.Channels.fireMessageReceived(Channels.java:261)
	at org.jboss.netty.channel.socket.nio.NioWorker.read(NioWorker.java:349)
	at org.jboss.netty.channel.socket.nio.NioWorker.processSelectedKeys(NioWorker.java:280)
	at org.jboss.netty.channel.socket.nio.NioWorker.run(NioWorker.java:200)
	at java.util.concurrent.ThreadPoolExecutor$Worker.runTask(ThreadPoolExecutor.java:895)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:918)
	at java.lang.Thread.run(Thread.java:662)
Caused by: java.util.concurrent.RejectedExecutionException: Thread pool is EXHAUSTED! Thread Name: DubboServerHandler-169.254.183.37:20882, Pool Size: 200 (active: 200, core: 200, max: 200, largest: 200), Task: 1108 (completed: 908), Executor status:(isShutdown:false, isTerminated:false, isTerminating:false), in dubbo://169.254.183.37:20882!
	at com.alibaba.dubbo.common.threadpool.support.AbortPolicyWithReport.rejectedExecution(AbortPolicyWithReport.java:53)
	at java.util.concurrent.ThreadPoolExecutor.reject(ThreadPoolExecutor.java:768)
	at java.util.concurrent.ThreadPoolExecutor.execute(ThreadPoolExecutor.java:656)
	at com.alibaba.dubbo.remoting.transport.dispatcher.all.AllChannelHandler.caught(AllChannelHandler.java:65)
	... 17 more
```

这时可以两种选择， ***增加线程的数量或者修改线程池为缓存线程池\*** 。

# 二、Dubbo提供的线程模型

- all 所有消息都派发到线程池，包括请求，响应，连接事件，断开事件，心跳等,模型如下图

![image.png](https://upload-images.jianshu.io/upload_images/5879294-e7e4984072455ccb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

- direct 所有消息都不派发到线程池，全部在 IO 线程上直接执行，模型如下图
  ![image.png](https://upload-images.jianshu.io/upload_images/5879294-ed3f770529f6234d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
- execution 只请求消息派发到线程池，不含响应，响应和其它连接断开事件，心跳等消息，直接在 IO 线程上执行，模型如下图
  ![image.png](https://upload-images.jianshu.io/upload_images/5879294-abe9d382cff83198.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
- connection 在 IO 线程上，将连接断开事件放入队列，有序逐个执行，其它消息派发到线程池。
  ![image.png](https://upload-images.jianshu.io/upload_images/5879294-261156f2a9e0ae8b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
- **message（MessageOnlyDispatcher类）**：只有请求响应消息派发到业务线程池，其他消息如连接事件、断开事件、心跳事件等，直接在I/O线程上执行。

,项目是springbot  依赖中用的是

<dependency>
    <groupId>com.alibaba.boot</groupId>
    <artifactId>dubbo-spring-boot-starter</artifactId>
    <version>0.2.0</version>
</dependency>
网上找到的很多配置都没有用，研究了一下

dubbo 默认线程池大小是200

调整线程池大小配置是

dubbo.protocol.threads = 5000
调整线程池类型配置是


dubbo.protocol.threadpool = cached
调整事件处理方式配置是

dubbo.protocol.dispatcher = message



 在介绍参数之前，我们先了解下dubbo中配置的优先级，以免出现调优参数设置了却没发现效果实际是配置被覆盖导致这样的问题。dubbo分为consumer和provider端，在配置各个参数时，其优先级如下：

1、consumer的method配置 

2、provider的method配置

3、consumer的reference配置

4、provider的service配置

5、consumer的consumer节点配置

6、provider的provider节点配置

可以看到，方法级的配置优先级高于接口级，consumer的优先级高于provider。同时，在本地参数配置还存在一层优先级：

1、系统参数(-D)，如-Ddubbo.protocol.port=20881

2、xml配置

3、property文件

​     了解了这两个优先级，调优起来才会更加清晰，省去了一些诸如配置设置了不生效这样的麻烦。注意，其实dubbo中还可以通过将配置写入注册中心的方式覆盖用户配置（优先级高于系统参数），这里不展开，有兴趣的同学可以去看官方文档。接下来我们看看dubbo的几个比较重要的调优参数，及其影响的方式和大概实现。

​     

| 参数名      | 作用范围 | 默认值    | 说明                                                         | 备注                     |
| ----------- | -------- | --------- | ------------------------------------------------------------ | ------------------------ |
| actives     | consumer | 0         | 每服务消费者每服务每方法最大并发调用数                       | 0表示不限制              |
| connections | consumer |           | 对每个提供者的最大连接数，rmi、http、hessian等短连接协议表示限制连接数，dubbo等长连接协表示建立的长连接个数 | dubbo时为1，及复用单链接 |
| accepts     | provider | 0         | 服务提供方最大可接受连接数                                   | 0表示不限制              |
| iothreads   | provider | cpu个数+1 | io线程池大小(固定大小)                                       |                          |
| threads     | provider | 200       | 业务线程池大小(固定大小)                                     |                          |
| executes    | provider | 0         | 服务提供者每服务每方法最大可并行执行请求数                   | 0表示不限制              |
| tps         | provider |           | 指定时间内（默认60s）最大的可执行次数，注意与executes的区别  | 默认不开启               |

 

 

![img](http://img.blog.csdn.net/20160711232203898?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQv/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

​    注意表中参数与图中的对应关系：

​    1、当consumer发起一个请求时，首先经过active limit(参数actives）进行方法级别的限制，其实现方式为CHM中存放计数器(AtomicInteger)，请求时加1，请求完成（包括异常）减1,如果超过actives则等待有其他请求完成后重试或者超时后失败；

​    2、从多个连接(connections）中选择一个连接发送数据，对于默认的netty实现来说，由于可以复用连接，默认一个连接就可以。不过如果你在压测，且只有一个consumer,一个provider，此时适当的加大connections确实能够增强网络传输能力。但线上业务由于有多个consumer多个provider，因此不建议增加connections参数；

​    3、连接到达provider时（如dubbo的初次连接），首先会判断总连接数是否超限（acceps），超过限制连接将被拒绝；

​    4、连接成功后，具体的请求交给io thread处理。io threads虽然是处理数据的读写，但io部分为异步，更多的消耗的是cpu，因此iothreads默认cpu个数+1是比较合理的设置，不建议调整此参数;

​    5、数据读取并反序列化以后，交给业务线程池处理，默认情况下线程池为fixed，且排队队列为0(queues)，这种情况下，最大并发等于业务线程池大小(threads)，如果希望有请求的堆积能力，可以调整queues参数。如果希望快速失败由其他节点处理（官方推荐方式），则不修改queues，只调整threads;

​    6、execute limit（参数executes）是方法级别的并发限制，原理与actives类似，只是少了等待的过程，即受限后立即失败；

​    7、tps，控制指定时间内（默认60s）的请求数。注意目前dubbo默认没有支持该参数，需要加一个META-INF/dubbo/com.alibaba.dubbo.rpc.Filter文件，文件内容为：

​          tps=com.alibaba.dubbo.rpc.filter.TpsLimitFilter

 

​    从上面的分析，可以看出如果consumer数*actives>provider数*threads且queues=0，则会存在部分请求无法申请到资源，重试也有很大几率失败。 当需要对一个接口的不同方法进行不同的并发控制时使用executes，否则调整threads就可以。