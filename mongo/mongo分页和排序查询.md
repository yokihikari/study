# 一、使用limit和skip进行分页查询

```java
public List<User> pageList(int pageNum ,int pageSize){
    List<User> userList = new ArrayList<>();
    Mongo mg = new Mongo();
    DB db = mg.getDB("data");
    DBCollection coll = db.getCollection("t_user");
    DBCursor limit = coll.find().skip((pageNum-1)*pageSize).sort(new BasicDBObject()).limit(pageSize);
    while (limit.hasNext()){
        userList.add(parse(new User(),limit.next()));
    }
    return userList;
}

private User parse(User user,DBObject obj){
    user.setAge((int) obj.get("age"));
    user.setName((String)obj.get("name"));
    user.setPwd((String)obj.get("pwd"));
    return user;
}

//查询结果 1,2
[
  {
    "id": null,
    "name": "ljl",
    "pwd": "123456",
    "age": 24
  },
  {
    "id": null,
    "name": "lsr",
    "pwd": "123456",
    "age": 18
  }
]123456789101112131415161718192021222324252627282930313233
```

通过skip和limit方法可以简单的实现分页操作，但是如果数据量特别巨大的时候，会出现性能的问题，建议不使用！

# 二、通过原生的方法实现条件查询、分页和排序

```java
public Page<User> getPageByOriginalFunction(int age,int pageNUmber,int pageSize){
    //查询条件,可以传递多个查询条件
    User user = new User();
    user.setAge(age);
    Example<User> example = Example.of(user);

    //分页条件
    //Pageable pageable = new PageRequest(pageNUmber,pageSize);
    Pageable pageable = PageRequest.of(pageNUmber,pageSize);

    return secondRepository.findAll(example,pageable);

}

//查询结果
{
  "content": [
    {
      "id": "5cfb69ee4332ce07b864d12e",
      "name": "lsr",
      "pwd": "123456",
      "age": 18
    }
  ],
  "pageable": {
    "sort": {
      "sorted": false,
      "unsorted": true
    },
    "offset": 0,
    "pageSize": 2,
    "pageNumber": 0,
    "unpaged": false,
    "paged": true
  },
  "last": true,
  "totalPages": 1,
  "totalElements": 1,
  "number": 0,
  "size": 2,
  "sort": {
    "sorted": false,
    "unsorted": true
  },
  "first": true,
  "numberOfElements": 1
}12345678910111213141516171819202122232425262728293031323334353637383940414243444546
```

# 三、通过实现Pageable接口，自定义

1.创建自定义分页类，实现Pageable接口

```java
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import javax.validation.constraints.Min;

@NoArgsConstructor
@AllArgsConstructor
public class SpringDataPageAble implements Pageable {

    @Min(1)
    private Integer pageNumber = 1;
    @Min(1)
    private Integer pageSize = 10;
    private Sort sort;

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    // 当前页面
    @Override
    public int getPageNumber() {
        return this.pageNumber;
    }

    // 每一页显示的条数

    @Override
    public int getPageSize() {
        return getPagesize();
    }

    // 第二页所需要增加的数量

    @Override
    public long getOffset() {
        return (getPageNumber() - 1) * getPagesize();
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    public void setPagenumber(Integer pagenumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPagesize() {
        return this.pageSize;
    }

    public void setPagesize(Integer pagesize) {
        this.pageSize = pagesize;
    }

    @Override
    public Pageable next() {
        return null;
    }

    @Override
    public Pageable previousOrFirst() {
        return null;
    }

    @Override
    public Pageable first() {
        return null;
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }
}12345678910111213141516171819202122232425262728293031323334353637383940414243444546474849505152535455565758596061626364656667686970717273747576
```

2.在repository层定义分页方法

```java
import com.tedu.huawei.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserFirstRepository extends MongoRepository<User,String> {
    Page<User> getUserByAgeGreaterThan(int age, Pageable pageAble);
}1234567
```

3.service层调用方法

```java
public Page<User> getUserByAgeGraterThan(int age, int pageNumber,int pageSize){
    SpringDataPageAble springDataPageAble = new SpringDataPageAble(pageNumber,pageSize,new Sort(Sort.Direction.ASC,"age"));
    return firstRepository.getUserByAgeGreaterThan(age,springDataPageAble);
}123
```

查询结果显示

```java
{
  "content": [
    {
      "id": "5cfb66114332ce07b864d12d",
      "name": "lsr",
      "pwd": "123456",
      "age": 18
    },
    {
      "id": "5cfb85084332ce4ffca97907",
      "name": "panzi",
      "pwd": "654321",
      "age": 24
    }
  ],
  "pageable": {
    "pageNumber": 1,
    "pageSize": 2,
    "sort": {
      "sorted": true,
      "unsorted": false
    },
    "offset": 0,
    "pagesize": 2,
    "unpaged": false,
    "paged": true
  },
  "last": true,
  "totalPages": 1,
  "totalElements": 2,
  "number": 1,
  "size": 2,
  "sort": {
    "sorted": true,
    "unsorted": false
  },
  "first": false,
  "numberOfElements": 2
}

```

## 查询排序

```
Query query = new Query();



 



query.with(



 



    new Sort(new Sort.Order(Sort.Direction.DESC,"modifiedtime"))



 



);
```

 