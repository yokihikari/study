这是一个简单的pojo：

```
public class Description {
    private String code;
    private String name;
    private String norwegian;
    private String english;
}
```

并且请参阅以下代码，`upsert`以通过春季MongoTemplate将MongoDb应用于MongoDb：

```
Query query = new Query(Criteria.where("code").is(description.getCode()));
Update update = new Update().set("name", description.getName()).set("norwegian", description.getNorwegian()).set("english", description.getEnglish());
mongoTemplate.upsert(query, update, "descriptions");
```

生成`Update`对象的行`Item`手动指定了类的每个字段。

但是，如果我的`Item`对象发生变化，那么我的刀层就会破裂。

那么有没有一种方法可以避免这样做，以便将我`Item`班级中的所有字段都自动应用于更新？

例如

```
Update update = new Update().fromObject(item);
```

请注意，我的pojo不会扩展`DBObject`。

我遇到了同样的问题。在当前的Spring Data MongoDB版本中，没有这样的东西。您必须手动更新单独的字段。

但是，可以使用另一个框架：Morphia。

该框架具有DAO功能的包装器：[https](https://github.com/mongodb/morphia/wiki/DAOSupport) : [//github.com/mongodb/morphia/wiki/DAOSupport](https://github.com/mongodb/morphia/wiki/DAOSupport)

您可以使用DAO API执行以下操作：

```
SomePojo pojo = daoInstance.findOne("some-field", "some-value");
pojo.setAProperty("changing this property");
daoInstance.save(pojo);
```



我为这个问题找到了一个很好的解决方案

```
//make a new description here
Description d = new Description();
d.setCode("no");
d.setName("norwegian");
d.setNorwegian("norwegian");
d.setEnglish("english");

//build query
Query query = new Query(Criteria.where("code").is(description.getCode()));

//build update
DBObject dbDoc = new BasicDBObject();
mongoTemplate.getConverter().write(d, dbDoc); //it is the one spring use for convertions.
Update update = Update.fromDBObject(dbDoc);

//run it!
mongoTemplate.upsert(query, update, "descriptions");
```

请注意，Update.fromDBObject返回一个包含dbDoc中所有字段的更新对象。如果只想更新非空字段，则应编写一种新方法来排除空字段。

例如，前端发布如下文档：

```
//make a new description here
Description d = new Description();
d.setCode("no");
d.setEnglish("norwegian");
```

我们只需要更新字段“语言”：

```
//return Update object
public static Update fromDBObjectExcludeNullFields(DBObject object) {
    Update update = new Update();       
    for (String key : object.keySet()) {
        Object value = object.get(key);
        if(value!=null){
            update.set(key, value);
        }
    }
    return update;
}

//build udpate
Update update = fromDBObjectExcludeNullFields(dbDoc);
```



新的spring-data-mongodb版本2.XX的解决方案

API已经发展，从2.XX版本开始，有：

```
Update.fromDocument(org.bson.Document object, String... exclude)
```

代替（1.XX）：

```
Update.fromDBObject(com.mongodb.DBObject object, String... exclude)
```

完整的解决方案：

```
//make a new description here
Description d = new Description();
d.setCode("no");
d.setName("norwegian");
d.setNorwegian("norwegian");
d.setEnglish("english");
Query query = new Query(Criteria.where("code").is(description.getCode()));

Document doc = new Document(); // org.bson.Document
mongoTemplate.getConverter().write(item, doc);
Update update = Update.fromDocument(doc);

mongoTemplate.upsert(query, update, "descriptions");
```

有用！



您可以使用save ：（如果不存在=插入else = upsert）

> save（Object objectToSave，字符串collectionName）



这是我目前正在做的事情。没有那么优雅的方法，但是它确实节省了宝贵的数据库调用：

```
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

/**
 * Perform an upsert operation to update ALL FIELDS in an object using native mongo driver's methods
 * since mongoTemplate's upsert method doesn't allow it
 * @param upsertQuery
 * @param object
 * @param collectionName
 */
private void performUpsert(Query upsertQuery, Object object, String collectionName){

    ObjectMapper mapper = new ObjectMapper();

    try {
        String jsonStr = mapper.writeValueAsString(object);
        DB db = mongoTemplate.getDb();
        DBCollection collection = db.getCollection(collectionName);
        DBObject query = upsertQuery.getQueryObject();
        DBObject update = new BasicDBObject("$set", JSON.parse(jsonStr));
        collection.update(query, update, true, false);
    } catch (IOException e) {
        LOGGER.error("Unable to persist the metrics in DB. Error while parsing object: {}", e);
    }
}
```



如果您想升级Pojos incl。属性，`String id;`您必须在fromDBObject方法中排除_id字段`Update.fromDBObject(dbDoc,"_id"`）。

否则，您将获得异常：

```
org.springframework.dao.DuplicateKeyException: { "serverUsed" : "127.0.0.1:27017" , "ok" : 1 , "n" : 0 , "updatedExisting" : false , "err" : "E11000 duplicate key error collection: db.description index: _id_ dup key: { : null }" , "code" : 11000}; nested exception is com.mongodb.MongoException$DuplicateKey: { "serverUsed" : "127.0.0.1:27017" , "ok" : 1 , "n" : 0 , "updatedExisting" : false , "err" : "E11000 duplicate key error collection: db.description index: _id_ dup key: { : null }" , "code" : 11000}
```

因为第一个的_id字段为null

```
{
    "_id" : null,
...

}
```

基于@PaniniGelato答案的完整代码将是

```
public class Description(){
    public String id;
...
}

Description d = new Description();
d.setCode("no");
d.setName("norwegian");
d.setNorwegian("norwegian");
d.setEnglish("english");

//build query
Query query = new Query(Criteria.where("code").is(description.getCode()));

//build update
DBObject dbDoc = new BasicDBObject();
mongoTemplate.getConverter().write(d, dbDoc); //it is the one spring use for convertions.
Update update = Update.fromDBObject(dbDoc, "_id");

//run it!
mongoTemplate.upsert(query, update, "descriptions");
```

然后，upsert在插入**和**更新的情况下起作用。欢迎更正和想法;）



- 警告：如果您`@Version`在实体中使用带-标记的字段，则此方法将干扰spring的版本增加，从而产生异常“ Invalid BSON field name $ inc”。在这种情况下，必须遍历dbDoc并`update.set`像@PaniniGelatos答案中那样使用。 –[拉达尔先生](https://stackoverflow.com/users/1437307/mr-radar) [17](https://stackoverflow.com/questions/20001627/mongotemplate-upsert-easy-way-to-make-update-from-pojo-which-user-has-editted#comment72921782_39290410)年 [3月20日在18:32](https://stackoverflow.com/questions/20001627/mongotemplate-upsert-easy-way-to-make-update-from-pojo-which-user-has-editted#comment72921782_39290410) 



就像之前的回答一样，请使用mongoTemplate.getConverter（）。write（）和Update.fromDocument（）函数。但是我发现Update.fromDocument（）不会添加“ $ set”键并且无法直接工作，解决方案是自己添加“ $ set”，如下所示（PS：我使用的是2.2.1.RELEASE版本）：

```
public static Update updateFromObject(Object object, MongoTemplate mongoTemplate) {
    Document doc = new Document();
    mongoTemplate.getConverter().write(object, doc);
    return Update.fromDocument(new Document("$set", doc));
}
```



这里需要区分两种情况：

1. 更新以前从数据库中获取的项目。
2. 更新或插入（更新）您通过代码创建的项目。

在情况1中，您可以简单地使用[mongoTemplate.save（pojo，“ collection”）](http://docs.spring.io/spring-data/mongodb/docs/current/api/org/springframework/data/mongodb/core/MongoOperations.html#save-java.lang.Object-java.lang.String-)，因为您的POJO在其id字段中已经具有一个已填充的ObjectID。

在情况2中，您必须向mongo解释在域模型的情况下“已经存在”的含义：默认情况下，如果存在一个具有相同ObjectId的项目，则mongoTemplate.save（）方法将更新现有项目。但是对于新实例化的POJO，您没有该ID。因此，mongoTemplate.upsert（）方法具有一个查询参数，您可以像这样创建：

```
MyDomainClass pojo = new MyDomainClass(...);

Query query = Query.query(Criteria.where("email").is("user1@domain.com"));

DBObject dbDoc = new BasicDBObject();
mongoTemplate.getConverter().write(pojo, dbDoc);   //it is the one spring use for convertions.
dbDoc.removeField("_id");    // just to be sure to not create any duplicates
Update update = Update.fromDBObject(dbDoc);

WriteResult writeResult = mongoTemplate.upsert(query, update, UserModel.class);
```



我认为：说明添加属性

```
@Id
private String id;
```

然后根据查询条件获取文档，并通过文档ID设置描述ID。并保存



只需使用`ReflectionDBObject`-如果对其进行`Description`扩展，则应仅将对象的字段`Update`自动反射地反射。上面关于更新中包含的空字段的注释仍然适用。

```
public void saveOrUpdate(String json) {
    try {
        JSONObject jsonObject = new JSONObject(json);
        DBObject update1 = new BasicDBObject("$set", JSON.parse(json));
        mongoTemplate.getCollection("collectionName").update(new Query(Criteria.where("name").is(jsonObject.getString("name"))).getQueryObject(), update1, true, false);
    } catch (Exception e) {
        throw new GenericServiceException("Error while save/udpate. Error msg: " + e.getMessage(), e);
    }

}
```

这是使用mongodb和spring将json字符串保存到集合中的非常简单的方法。可以重写此方法以用作JSONObject。



```
@Override
public void updateInfo(UpdateObject algorithm) {
    Document document = new Document();
    mongoTemplate.getConverter().write(algorithm, document);
    Update update = Update.fromDocument(document);
    mongoTemplate.updateFirst(query(where("_id").is(algorithm.get_id())), update, UpdateObject.class);
}
```

mongodb中的update的形式是这样的：

db.collectionName.update(query, obj, upsert, multi);

对于upsert(默认为false)：如果upsert=true，如果query找到了符合条件的行，则修改这些行，如果没有找到，则追加一行符合query和obj的行。如果upsert为false，找不到时，不追加。

对于multi(默认为false): 如果multi=true，则修改所有符合条件的行，否则只修改第一条符合条件的行。