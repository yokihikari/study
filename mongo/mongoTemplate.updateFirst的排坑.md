最近用springboot自带的MongoTemplate来做订单模块的一些操作，过程中发现里面的坑，比如如下需求

根据订单状态修改时间最近的一条订单状态

那么条件就是：1.订单状态 2.根据时间排序

然后我是这样写的

Query query = new Query();
query.addCriteria(Criteria.where("orderstatus").is(orderstatus));
query.with(Sort.by(Sort.Order.desc("ordertime")));
Update update = new Update().set("orderstatus", orderStatus);
mongoTemplate.updateFirst(query, update, Order.class);
用mongoTemplate.find查询出来的数据和预期的效果一样，但是发现，mongoTemplate.updateFirst修改的第一条数据始终都只会修改列表中的第一条订单，逻辑是没有问题的，经过各种测试，最后得知，原来query.with(Sort.by(Sort.Order.desc("ordertime")));这个排序并没有加到查询条件里面去

最后解决方案

先查询，然后再修改

Query query = new Query();
query.addCriteria(Criteria.where("orderstatus").is(orderstatus));
query.with(Sort.by(Sort.Order.desc("ordertime")));
//获取集合的第一条数据，然后再去修改
Order order= mongoTemplate.find(query, Order.class).get(0);
Query query2 = new Query();
query2.addCriteria(Criteria.where("orderid").is(order.getOrderId()));
Update update = new Update().set("orderstatus", orderStatus);
mongoTemplate.updateFirst(query2, update, Order.class);
