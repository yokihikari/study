**建立一个数据库来存放文章的评论数据,数据格式如下**
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113220754707.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)

## 数据库操作

### 默认的数据库介绍

- **admin**： 从权限的角度来看，这是"root"数据库。要是将一个用户添加到这个数据库，这个用户自动继承所有数据库的权限。一些特
  定的服务器端命令也只能从这个数据库运行，比如列出所有的数据库或者关闭服务器。
- **local**: 这个数据永远不会被复制，可以用来存储限于本地单台服务器的任意集合
- **config**: 当Mongo用于分片设置时，config数据库在内部使用，用于保存分片的相关信息。

### 数据库的操作(库级操作语句)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113220731330.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
在MongoDB中, 数据库的创建不需要使用 createdatabase这种命令,当输入`use 数据库名`的时候, 若数据库存在,则切换到数据库,若数据库不存在,则创建数据库
**需要注意的是,这里创建数据库仅仅是在内存中创建,只有当数据库中真正有集合的时候才会写入磁盘**
*简单的说,创建数据库后,还需要插入一个记录,才算真的创建*

## 集合操作

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113221307103.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
集合，类似关系型数据库中的表。
可以显示的创建，也可以隐式的创建。

### 集合的显示创建

```json
db.createCollection(name)
1
```

`name`: 要创建的集合名词

### 集合的隐式创建

当向一个集合中插入一个文档的时候，如果集合不存在，则会自动创建集合。
通常我们使用隐式创建文档即可。

### 集合的删除

```json
db.集合.drop()
1
```

如删除 student 集合

```
db.student.drop()
1
```

## 文档操作

文档（document）的数据结构和 JSON 基本一样。

*所有存储在集合中的数据都是 BSON 格式。*

*BSON就是用二进制编码储存的JSON数据*

### 文档的添加

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113223546220.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
**示例**
**向comment的集合(表)中插入一条测试数据：**

```bash
db.comment.insert({"articleid":"100000","content":"今天天气真好，阳光明 媚","userid":"1001","nickname":"Rose","createdatetime":new Date(),"likenum":NumberInt(10),"state":null})
1
```

提示：

- 1）comment集合如果不存在，则会隐式创建
- 2）mongo中的数字，默认情况下是double类型，如果要存整型，必须使用函数**NumberInt**(整型数字)，否则取出来就有问题了。
- 3）插入当前日期使用 new Date()
- 4）插入的数据没有指定 _id ，会自动生成主键值
- 5）如果某字段没值，可以赋值为null，或不写该字段。

**成功**
![在这里插入图片描述](https://img-blog.csdnimg.cn/202101132241454.png)

#### 批量插入

```json
db.collection.insertMany()
1
db.comment.insertMany([ {"_id":"1","articleid":"100001","content":"我们不应该把清晨浪费在手机上，健康很重要，一杯温水幸福你我 他。","userid":"1002","nickname":"相忘于江湖","createdatetime":new Date("2019-08- 05T22:08:15.522Z"),"likenum":NumberInt(1000),"state":"1"}, {"_id":"2","articleid":"100001","content":"我夏天空腹喝凉开水，冬天喝温开水","userid":"1005","nickname":"伊人憔 悴","createdatetime":new Date("2019-08-05T23:58:51.485Z"),"likenum":NumberInt(888),"state":"1"}, {"_id":"3","articleid":"100001","content":"我一直喝凉开水，冬天夏天都喝。","userid":"1004","nickname":"杰克船 长","createdatetime":new Date("2019-08-06T01:05:06.321Z"),"likenum":NumberInt(666),"state":"1"}, {"_id":"4","articleid":"100001","content":"专家说不能空腹吃饭，影响健康。","userid":"1003","nickname":"凯 撒","createdatetime":new Date("2019-08-06T08:18:35.288Z"),"likenum":NumberInt(2000),"state":"1"}, {"_id":"5","articleid":"100001","content":"研究表明，刚烧开的水千万不能喝，因为烫 嘴。","userid":"1003","nickname":"凯撒","createdatetime":new Date("2019-08- 06T11:01:02.521Z"),"likenum":NumberInt(3000),"state":"1"} ]);
1
```

**注意**
插入时一般不指定`_id`
如果`_id`重复的话会终止插入,但是插入成功的数据不会回滚
可以使用try catch进行异常捕捉处理

### 文档的普通查询

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113224617270.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)

#### 基本查询

**查询所有**

```json
db.comment.find()
db.comment.find().pretty()
12
```

**示例**
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113224644988.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
**查询指定的键**
*如查询userid为1003*

```json
db.comment.find({userid:'1003'})
1
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113224911857.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
*如上图会返回两条,如果只要第一条记录*
**查询单条记录**

```json
db.comment.findOne({userid:'1003'})
1
```

#### 投影查询

像上面的查询操作类似 `select * from comment`
而投影查询只返回指定的字段

**如查询userid=1003 并且只显示nickname**

```json
 db.comment.find({userid:'1003'},{nickname:1})
1
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113225225511.png)
如上图,默认的返回结果会带有`_id`,如果不要`_id`的话

```json
 db.comment.find({userid:'1003'},{nickname:1,_id:0})
1
```

**查询所有数据，但只显示 _id、userid、nickname :**

```json
db.comment.find({},{userid:1,nickname:1})
1
```

### 文档的更新

![在这里插入图片描述](https://img-blog.csdnimg.cn/2021011322553213.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
下面说明一下上面几个语句的意思
![例如](https://img-blog.csdnimg.cn/20210113225614284.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
例如上述已有的数据,想要将其中的`userid=1003`的`likenum`都变成666
**覆盖修改**
如果执行

```json
db.comment.update({userid:"1003"},{likenum:NumberInt(666)})
1
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/2021011323005535.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
那么会发现,只有第一条数据被修改了,并且仅仅保留了likenum这个字段
显然这不是我们要的结果
**局部修改**
正确的语句应该是

```json
db.comment.update({userid:"1003"},{$set:{likenum:NumberInt(666)}})
1
```

但是这样也只能修改第一条数据
如果想要修改所有的userid为1003的数据
**批量修改**
要跟上参数`multi:true}`

```json
//默认只修改第一条数据 
db.comment.update({userid:"1003"},{$set:{nickname:"凯撒2"}}) //修改所有符合条件的数据 
db.comment.update({userid:"1003"},{$set:{nickname:"凯撒大帝"}},{multi:true})
123
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113230639237.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
但是实际中我们需要的是让点赞数加一,而不是直接赋值
因此如下
**列值增长的修改**
使用`$inc`来实现 (increase)

```json
db.comment.update({_id:"3"},{$inc:{likenum:NumberInt(1)}})
1
```

相当于对_id=3的数据的likenum ++ 的操作

### 文档的删除

```json
db.集合名称.remove(条件)
1
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113230854438.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
**全部删除**
相当于删库跑路

```json
db.comment.remove({})
1
```

**删除指定条件**
删除_id=1的所有文档

```json
db.comment.remove({_id:"1"})
1
```

**删除符合条件的第一条**
删除articleid为100001的第一条文档

```json
db.comment.remove({articleid:"100001"},{justOne:true})
1
```

### 文档的分页查询

#### **统计**

```json
db.collection.count(query, options)
1
```

统计所有记录数

```json
db.collection.count()
1
```

统计userid为1003的记录条数

```json
db.comment.count({userid:"1003"})
1
```

#### 分页列表查询(切片)

**查询前三条结果**

```json
db.comment.find().limit(3)
1
```

但是这样只能返回前三条
如果要返回3,4,5条结果得使用如下的语句

```json
db.comment.find().limit(3).skip(2)
1
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113231558109.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)

#### 排序查询

```json
db.COLLECTION_NAME.find().sort({KEY:1})
db.集合名称.find().sort(排序方式)
12
```

-1代表降序,1代表升序

对userid降序排列，并对访问量进行升序排列

```json
db.comment.find().sort({userid:-1,likenum:1})
1
```

**注意: **
`skip()`, `limilt()`, `sort()`三个放在一起执行的时候，执行的顺序是先 `sort()`, 然后是 `skip()`，最后是显示的 `limit()`，和命令编写顺序无关。

### 文档的更多查询

#### 正则的复杂条件查询

有点类似于js中的正则表达式格式

```json
db.collection.find({field:/正则表达式/}) 
或
db.集合.find({字段:/正则表达式/})
123
```

**实例**
查询评论内容包含“开水”的所有文档

```json
 db.comment.find({content:/开水/})
1
```

查询评论的内容中以“专家”开头的

```json
db.comment.find({content:/^专家/})
1
```

#### 查询中的噩梦条件

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210113232102803.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dlYXJ5X1BK,size_16,color_FFFFFF,t_70)
**案例**

比较评论点赞数大于700的

```json
db.comment.find({likenum:{$gt:NumberInt(700)}})
1
```

查询评论的集合中userid字段包含1003或1004的文档

```json
db.comment.find({userid:{$in:["1003","1004"]}})
1
```

查询评论集合中userid字段不包含1003和1004的文档

```json
db.comment.find({userid:{$nin:["1003","1004"]}})
1
```

**and,or查询格式如下**

```json
$and/or:[ { },{ },{ } ]
1
```

查询评论集合中likenum大于等于700 并且小于2000的文档

```json
db.comment.find({$and:[{likenum:{$gte:NumberInt(700)}},{likenum:{$lt:NumberInt(2000)}}]})
1
```

查询评论集合中userid为1003，或者点赞数小于1000的文档记录

```json
db.comment.find({$or:[ {userid:"1003"} ,{likenum:{$lt:1000} }]})
1
```

### MongoDB常用的命令小结

- 选择切换数据库：`use articledb`
- 插入数据：`db.comment.insert({bson数据})`
- 查询所有数据：`db.comment.find();`
- 条件查询数据：`db.comment.find({条件})`
- 查询符合条件的第一条记录：`db.comment.findOne({条件})`
- 查询符合条件的前几条记录：`db.comment.find({条件}).limit(条数)`
- 查询符合条件的跳过的记录：`db.comment.find({条件}).skip(条数)`
- 修改数据：`db.comment.update({条件},{修改后的数据})` 或`db.comment.update({条件},{$set:{要修改部分的字段:数据})`
- 修改数据并自增某字段值：`db.comment.update({条件},{$inc:{自增的字段:步进值}})`
- 删除数据：`db.comment.remove({条件})`
- 统计查询：`db.comment.count({条件})`
- 模糊查询：`db.comment.find({字段名:/正则表达式/})`
- 条件比较运算：`db.comment.find({字段名:{$gt:值}})
- 包含查询：`db.comment.find({字段名:{$in:[值1，值2]}})`或`db.comment.find({字段名:{$nin:[值1，值2]}})`
- 条件连接查询：`db.comment.find({$and:[{条件1},{条件2}]})`或`db.comment.find({$or:[{条件1},{条件2}]})`