如果在MongoDB存储的数据是有序的插入的，且以MongoDB自动生成的_id为集合文档的主键，那么查询集合最新一条插入的数据可以用一下方式：

db.collection.find({}).sort({_id:-1}).limit(1)
默认情况下，_id 字段的类型为 ObjectID，是 MongoDB 的 BSON 类型之一.
ObjectID 长度为 12 字节，由几个 2-4 字节的链组成。每个链代表并指定文档身份的具体内容。以下的值构成了完整的 12 字节组合：
一个 4 字节的值，表示自 Unix 纪元以来的秒数
一个 3 字节的机器标识符
一个 2 字节的进程 ID
一个 3 字节的计数器，以随机值开始

将_id进行倒序排列，再取第一条，就可以获取最新一条插入的数据。

SpringBoot+MongoDB 中获取代码为：

Query query = new Query();
query.with(Sort.by(Sort.Order.desc("_id"))).limit(1);
mongoTemplate.find(query,entityClass,collectionName);