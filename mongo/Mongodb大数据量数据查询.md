因为公司业务需要，需要从Mongodb内读取数据。

 ![img](https://img-blog.csdnimg.cn/20210318221706302.png) 

一开始直接调用MongodbTemplate的api进行查询

 ![img](https://img-blog.csdnimg.cn/20210318221820404.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80Mjc2NTc5Mg==,size_16,color_FFFFFF,t_70) 

 ![img](https://img-blog.csdnimg.cn/20210318222012248.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80Mjc2NTc5Mg==,size_16,color_FFFFFF,t_70) 

后面在网上查询之后才知道如果不让Mongodb帮我们映射查询结果效率会更高一点，于是有了第二次查询。

![img](https://img-blog.csdnimg.cn/20210318222535267.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80Mjc2NTc5Mg==,size_16,color_FFFFFF,t_70) 

查询耗时

 ![img](https://img-blog.csdnimg.cn/20210318222842106.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80Mjc2NTc5Mg==,size_16,color_FFFFFF,t_70) 



