1.环境

阿里云centos 7 4核 8G

安装：docker 版本

Docker version 18.09.7, build 2d0083d

mongodb 版本4.2

2.期望结果:
 支持副本集事务回滚 spring boot 下测试正常

注： 没安装docker-composer那个

 

 

\3. 副本集搭建3个mongodb server端

副本集 1主2从 至少3个mongodb 

端口:20917 20918 20919

 

3-1.创建共用的keyfile:

 

mkdir -p /data/mongodb0_conf

cd /data/mongodb0_conf

openssl rand -base64 741 > mongodb-keyfile

chmod 600 mongodb-keyfile

chown 999 mongodb-keyfile

 

3-2.创建3个mongo 本地存放文件目录mongo1 mongo2 mongo3 配置db config ，config目录暂无配置

mkdir -p /ddy/mongo1/db /ddy/mongo1/config /ddy/mongo2/db /ddy/mongo2/config  /ddy/mongo3/db /ddy/mongo3/config

 

3-3.第一次启动 3个docker mongo 命名mongoServer1 mongoServer2 mongoServer3：

docker run --name mongoServer1 --restart always -v /ddy/mongo1/db:/data/db -v /ddy/mongo1/config:/data/configdb -v /ddy/mongoconf:/opt/keyfile -p 27917:27017 -d mongo:4.2.0-rc2-bionic --keyFile /opt/keyfile/mongodb-keyfile --replSet exuehui-mongo-set

 

 

docker run --name mongoServer2 --restart always -v /ddy/mongo2/db:/data/db -v /ddy/mongo2/config:/data/configdb -v /ddy/mongoconf:/opt/keyfile -p 27918:27017 -d mongo:4.2.0-rc2-bionic --keyFile /opt/keyfile/mongodb-keyfile --replSet exuehui-mongo-set

 

docker run --name mongoServer3 --restart always -v /ddy/mongo3/db:/data/db -v /ddy/mongo3/config:/data/configdb -v /ddy/mongoconf:/opt/keyfile -p 27919:27017 -d mongo:4.2.0-rc2-bionic --keyFile /opt/keyfile/mongodb-keyfile --replSet exuehui-mongo-set

 

 

3-4.进入第一台mongo 创建副本集 及 配置创建用户 

docker exec -it mongoServer1 /bin/bash

mongo

 

rs.initiate({ _id:"exuehui-mongo-set", members:[ {_id:0,host:"home.lemonsoft.vip:27917"}, {_id:1,host:"home.lemonsoft.vip:27918"}, {_id:2,host:"home.lemonsoft.vip:27919"} ]})

 

查看 副本集是否成功配置

rs.status()

 

 

admin 库创建管理员

use admin;

db.createUser({ user: 'root', pwd: ' xxxx', roles: [ { role: "userAdminAnyDatabase", db: "admin" } ] });

 

退出docker 

exit 

exit

 

3-4. 重新运行容器 带权限验证启动 添加带--auth

docker rm mongServer1

docker rm mongoServer2

docker rm mongoServer3

 

 

重新启动

docker run --name mongoServer1 --restart always -v /ddy/mongo1/db:/data/db -v /ddy/mongo1/config:/data/configdb -v /ddy/mongoconf:/opt/keyfile -p 27917:27017 -d mongo:4.2.0-rc2-bionic --keyFile /opt/keyfile/mongodb-keyfile --auth --replSet exuehui-mongo-set

 

 

docker run --name mongoServer2 --restart always -v /ddy/mongo2/db:/data/db -v /ddy/mongo2/config:/data/configdb -v /ddy/mongoconf:/opt/keyfile -p 27918:27017 -d mongo:4.2.0-rc2-bionic --keyFile /opt/keyfile/mongodb-keyfile --auth --replSet exuehui-mongo-set

 

 

docker run --name mongoServer3 --restart always -v /ddy/mongo3/db:/data/db -v /ddy/mongo3/config:/data/configdb -v /ddy/mongoconf:/opt/keyfile -p 27919:27017 -d mongo:4.2.0-rc2-bionic --keyFile /opt/keyfile/mongodb-keyfile --auth --replSet exuehui-mongo-set

 

3-5.后续 在docker 里面 主的一方 登录admin 再创建新的库 及库的用户

use test1

 

 

db.createUser({ user: 'ddy2019', pwd: ' xxxxx', roles: [ { role: "dbOwner", db: "test1" } ] });

 

客户端连接就都需要用户登录了

 

 

3-6.副本集的连接直接配置在主连接就行 注意配置IP时 用内网的话 只能内网访问 外网就不行了 如果要外网测试需要配置外网地址

 