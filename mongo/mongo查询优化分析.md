在MySQL中，慢查询日志是经常作为我们优化查询的依据，那在MongoDB中是否有类似的功能呢？答案是肯定的，那就是开启Profiling功能。该工具在运行的实例上收集有关MongoDB的写操作，游标，数据库命令等，可以在数据库级别开启该工具，也可以在实例级别开启。该工具会把收集到的所有都写入到[`system.profile`](http://docs.mongodb.org/manual/reference/system-collections/#.system.profile)集合中，该集合是一个[*capped collection*](http://docs.mongodb.org/manual/core/capped-collections/)。更多的信息见：http://docs.mongodb.org/manual/tutorial/manage-the-database-profiler/和http://mp.weixin.qq.com/s?__biz=MzA4Nzg5Nzc5OA==&mid=207007436&idx=1&sn=a63601d81c8d112228c96ad9fffb031c&scene=21#wechat_redirect

**使用说明：**

**1：Profiling级别说明**

```
0：关闭，不收集任何数据。
1：收集慢查询数据，默认是100毫秒。
2：收集所有数据
```

**2：开启Profiling和设置**

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
1：通过mongo shell:
#查看状态：级别和时间
drug:PRIMARY> db.getProfilingStatus()   
{ "was" : 1, "slowms" : 100 }
#查看级别
drug:PRIMARY> db.getProfilingLevel()    
1
#设置级别
drug:PRIMARY> db.setProfilingLevel(2)
{ "was" : 1, "slowms" : 100, "ok" : 1 }
#设置级别和时间
drug:PRIMARY> db.setProfilingLevel(1,200)
{ "was" : 2, "slowms" : 100, "ok" : 1 }

以上要操作要是在test集合下面的话，只对该集合里的操作有效，要是需要对整个实例有效，则需要在所有的集合下设置或则在开启的时候开启参数：
2：不通过mongo shell:
mongod --profile=1 --slowms=15
或则在配置文件里添加2行：
profile = 1
slowms = 300
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

**3：关闭Profiling**

```
# 关闭
drug:PRIMARY> db.setProfilingLevel(0)
{ "was" : 1, "slowms" : 200, "ok" : 1 }
```

**4：修改“慢查询日志”的大小**

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
#关闭Profiling
drug:PRIMARY> db.setProfilingLevel(0)
{ "was" : 0, "slowms" : 200, "ok" : 1 }
#删除system.profile集合
drug:PRIMARY> db.system.profile.drop()
true
#创建一个新的system.profile集合
drug:PRIMARY> db.createCollection( "system.profile", { capped: true, size:4000000 } )
{ "ok" : 1 }
#重新开启Profiling
drug:PRIMARY> db.setProfilingLevel(1)
{ "was" : 0, "slowms" : 200, "ok" : 1 }
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

注意：要改变Secondary的system.profile的大小，你必须停止Secondary，运行它作为一个独立的，然后再执行上述步骤。完成后，重新启动加入副本集。

**慢查询(system.profile)说明：**

通过下面的例子说明，更多信息见：http://docs.mongodb.org/manual/reference/database-profiler/

**1：参数含义**

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
drug:PRIMARY> db.system.profile.find().pretty()
{
    "op" : "query",    #操作类型，有insert、query、update、remove、getmore、command   
    "ns" : "mc.user",  #操作的集合
    "query" : {        #查询语句
        "mp_id" : 5,
        "is_fans" : 1,
        "latestTime" : {
            "$ne" : 0
        },
        "latestMsgId" : {
            "$gt" : 0
        },
        "$where" : "new Date(this.latestNormalTime)>new Date(this.replyTime)"
    },
    "cursorid" : NumberLong("1475423943124458998"),
    "ntoreturn" : 0,   #返回的记录数。例如，profile命令将返回一个文档（一个结果文件），因此ntoreturn值将为1。limit(5)命令将返回五个文件，因此ntoreturn值是5。如果ntoreturn值为0，则该命令没有指定一些文件返回，因为会是这样一个简单的find()命令没有指定的限制。
    "ntoskip" : 0,     #skip()方法指定的跳跃数
    "nscanned" : 304,  #扫描数量
    "keyUpdates" : 0,  #索引更新的数量，改变一个索引键带有一个小的性能开销，因为数据库必须删除旧的key，并插入一个新的key到B-树索引
    "numYield" : 0,    #该查询为其他查询让出锁的次数
    "lockStats" : {    #锁信息，R：全局读锁；W：全局写锁；r：特定数据库的读锁；w：特定数据库的写锁
        "timeLockedMicros" : {     #锁
            "r" : NumberLong(19467),
            "w" : NumberLong(0)
        },
        "timeAcquiringMicros" : {  #锁等待
            "r" : NumberLong(7),
            "w" : NumberLong(9)
        }
    },
    "nreturned" : 101,        #返回的数量
    "responseLength" : 74659, #响应字节长度
    "millis" : 19,            #消耗的时间（毫秒）
    "ts" : ISODate("2014-02-25T02:13:54.899Z"), #语句执行的时间
    "client" : "127.0.0.1",   #链接ip或则主机
    "allUsers" : [ ],     
    "user" : ""               #用户
}
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

 除上面外还有：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
scanAndOrder：
scanAndOrder是一个布尔值，是True当一个查询不能使用的文件的顺序在索引中的排序返回结果：MongoDB中必须将其接收到的文件从一个游标后的文件进行排序。
如果scanAndOrder是False，MongoDB的可使用这些文件的顺序索引返回排序的结果。即：True：文档进行排序，False：使用索引。

moved
更新操作在磁盘上移动一个或多个文件到新的位置。表明本次update是否移动了硬盘上的数据，如果新记录比原记录短，通常不会移动当前记录，如果新记录比原记录长，那么可能会移动记录到其它位置，这时候会导致相关索引的更新.磁盘操作更多，加上索引更新，会使得这样的操作比较慢.
nmoved：
文件在磁盘上操作。

nupdated：
更新文档的数目
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

**getmore**是一个getmore 操作，getmore通常发生在结果集比较大的查询时，第一个query返回了部分结果，后续的结果是通过getmore来获取的。

如果**nscanned**(扫描的记录数)远大于**nreturned**(返回结果的记录数)的话，要考虑通过加索引来优化记录定位了。**responseLength** 如果过大，说明返回的结果集太大了，这时要看是否只需要必要的字段。

**2：日常使用的查询**

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
#返回最近的10条记录
db.system.profile.find().limit(10).sort({ ts : -1 }).pretty()

#返回所有的操作，除command类型的
db.system.profile.find( { op: { $ne : 'command' } } ).pretty()

#返回特定集合
db.system.profile.find( { ns : 'mydb.test' } ).pretty()

#返回大于5毫秒慢的操作
db.system.profile.find( { millis : { $gt : 5 } } ).pretty()

#从一个特定的时间范围内返回信息
db.system.profile.find(
                       {
                        ts : {
                              $gt : new ISODate("2012-12-09T03:00:00Z") ,
                              $lt : new ISODate("2012-12-09T03:40:00Z")
                             }
                       }
                      ).pretty()

#特定时间，限制用户，按照消耗时间排序
db.system.profile.find(
                       {
                         ts : {
                               $gt : new ISODate("2011-07-12T03:00:00Z") ,
                               $lt : new ISODate("2011-07-12T03:40:00Z")
                              }
                       },
                       { user : 0 }
                      ).sort( { millis : -1 } )
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

**总结：**

   Profiling 功能肯定是会影响效率的，但是不太严重，原因是他使用的是system.profile 来记录，而system.profile 是一个capped collection 这种collection 在操作上有一些限制和特点，但是效率更高，所以在使用的时候可以打开该功能，不需要一直打开。

1，查看数据库

```
> db.stats(); ``{ `` ``"db" : "test",    //当前数据库 `` ``"collections" : 3,   //当前数据库多少表 `` ``"objects" : 4,    //当前数据库所有表多少条数据 `` ``"avgObjSize" : 51,   //每条数据的平均大小 `` ``"dataSize" : 204,   //所有数据的总大小 `` ``"storageSize" : 16384,  //所有数据占的磁盘大小 `` ``"numExtents" : 3, `` ``"indexes" : 1,    //索引数 `` ``"indexSize" : 8176,   //索引大小 `` ``"fileSize" : 201326592,  //预分配给数据库的文件大小 `` ``"nsSizeMB" : 16, `` ``"dataFileVersion" : { ``  ``"major" : 4, ``  ``"minor" : 5 `` ``}, `` ``"ok" : 1 ``} 
```

2,查看数据库表 posts为表名

```
> db.posts.stats(); ``{ `` ``"ns"` `: ``"test.posts"``, `` ``"count"` `: 1, `` ``"size"` `: 56, `` ``"avgObjSize"` `: 56, `` ``"storageSize"` `: 8192, `` ``"numExtents"` `: 1, `` ``"nindexes"` `: 1, `` ``"lastExtentSize"` `: 8192, `` ``"paddingFactor"` `: 1, `` ``"systemFlags"` `: 1, `` ``"userFlags"` `: 0, `` ``"totalIndexSize"` `: 8176, `` ``"indexSizes"` `: { ``  ``"_id_"` `: 8176 `` ``}, `` ``"ok"` `: 1 ``} 
```