# 使用 BeanCopier 复制对象

Cglib是一款比较底层的操作java字节码的框架。

BeanCopier是一个工具类，可以用于Bean对象内容的复制。

复制Bean对象内容的方法有很多，比如自己手动get set ，或者使用PropertyUtils或者使用BeanUtils

 

BeanCopier与 PropertyUtils 、BeanUtils的不同在于：

PropertyUtils 和 BeanUtils 使用的是反射机制来完成属性的复制。

而BeanCopier 的原理是通过字节码动态生成一个类，这个里面实现get 和 set方法。(性能和对象直接调用set一样)

BeanCopier 唯一的花销是生成动态类上，但是生成的后的象可以自己保存起来反复使用。

根据网上的资料 BeanCopier的性能是PropertyUtils (apache-common)的500倍。 PropertyUtils的性能是BeanUtils(apache-common)的2倍。

 

以下是使用例子

```java
import net.sf.cglib.beans.BeanCopier;



 



public class BeanCopierTest {



	public static void main(String[] args) {



		Bean1 bean1 = new Bean1();



		Bean2 bean2 = new Bean2();



		bean1.setValue("hello");



		BeanCopier copier = BeanCopier.create(Bean1.class, Bean2.class, false);



		copier.copy(bean1, bean2, null);



 



		System.out.println(bean2.getValue());



	}



 



	static class Bean1 {



		private String value;



		public String getValue() {



			return value;



		}



		public void setValue(String value) {



			this.value = value;



		}



	}



	static class Bean2 {



		private String value;



		public String getValue() {



			return value;



		}



		public void setValue(String value) {



			this.value = value;



		}



	}



}
```

 

注意事项：BeanCopier在复制的时候使用get和set方法了。如果方法定义不严格，会造成复制的对象属性值不正确。

BeanCopier是Cglib包中的一个类，用于对象的复制。


注意：目标对象必须先实例化  而且对象必须要有setter方法

初始化例子：

BeanCopier copier = BeanCopier.create(Source.class, Target.class, false);  
copier.copy(source, target, null);

第三个参数useConverter，是否开启Convert。 默认BeanCopier只会做同名，同类型属性的copier，否则就会报错。如果类型需要转换比如Date转换成String则自定义Convert类实现Convert接口。

下例用到 父类Class.isAssignableFrom(子类Class)方法 这个方法用于判断子类是否属于父类，有人问为什么不用instanceOf， instanceOf用于判断类与类直接的关系，而isAssignableFrom可以用于接口直接的子父关系。

父类Class.isAssignableFrom(子类Class)



 * ```
 import java.util.Date;
 
	import net.sf.cglib.beans.BeanCopier;
	import net.sf.cglib.core.Converter;
	
	import org.junit.Test;
	
	import com.ibm.icu.text.SimpleDateFormat;
	
	/**
 
	 * 
	
	 * @author Test
	
	 * @createDate 2014-5-31上午09:50:18
	
	 * @className BeanCopierTest.java
	
  * */
    public class BeanCopierTest {
    @Test
    public void beanCopierTest() {
    	Address1 a1 = new Address1("LA", new Date());
    	Address1 a2 = new Address1();
    	a2 = a1;
    	// 这种复制修改会造成a1对象的数据进行修改
    	a2.setName("WA");
    	System.out.println(a1);
 
    	Address2 a3 = new Address2();
    	// 这样就只能使用其他拷贝方法 BeanCopier
    	BeanCopier beanCopier = BeanCopier.create(Address1.class,
    			Address2.class, true);
    	beanCopier.copy(a1, a3, new DateStringConverter());
    	System.out.println(a3);
    	System.out.println(a1);
 
    }
    }
 ```
 
 

```
class Address1 {
	private String name;
	private Date date;

	public Address1() {
	}
	 
	public Address1(String name, Date date) {
		super();
		this.name = name;
		this.date = date;
	}
	 
	public String getName() {
		return name;
	}
	 
	public void setName(String name) {
		this.name = name;
	}
	 
	public Date getDate() {
		return date;
	}
	 
	public void setDate(Date date) {
		this.date = date;
	}
	 
	@Override
	public String toString() {
		return "名字：" + this.name + " 时间：" + this.date;
	}

}
```



```
class Address2 {
	private int id;
	private String name;
	private String date;

	public Address2() {
	}
	 
	public Address2(String name, String date) {
		super();
		this.name = name;
		this.date = date;
	}
	 
	public int getId() {
		return id;
	}
	 
	public void setId(int id) {
		this.id = id;
	}
	 
	public String getName() {
		return name;
	}
	 
	public void setName(String name) {
		this.name = name;
	}
	 
	public String getDate() {
		return date;
	}
	 
	public void setDate(String date) {
		this.date = date;
	}
	 
	@Override
	public String toString() {
		return "" + this.id + " 名字：" + this.name + " 时间：" + this.date;
	}

}
```



```
/**

 * 

 * @author Test

 * @createDate 2014-5-31上午10:21:50

 * @className BeanCopierTest.java

 * @useFor 用于转换类型 比如 Address1中的Date类型 转成String
   */
   class DateStringConverter implements Converter {

   @SuppressWarnings("all")
   @Override
   /**

    * @author Test
    * @createDate 2014-5-31
    * @params value是Src对象  target是From对象 context是From类中的方法名
    * @return void
    * @useFor 实现转换方法
      */
      public Object convert(Object value, Class target, Object context) {
      System.out.println(value.getClass() + " " + value); // Src对象
      System.out.println(target); // From对象
      System.out.println(context.getClass() + " " + context); // String对象,具体的方法名
      if (value.getClass().isAssignableFrom(Date.class)) {
      	return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(value);
      } else {
      	return value;
      }
      }

}
```

