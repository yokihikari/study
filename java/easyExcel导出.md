**测试demo:**

```
    @GetMapping("/easy/export/Excel")
    public void easyExcel(HttpServletResponse response) {
        List<TerminalGroupResponse> users = new ArrayList<>();
        for (int i = 0; i < 200; i++) {
            TerminalGroupResponse terminalGroupResponse = new TerminalGroupResponse();
            terminalGroupResponse.setDimension("机");
            terminalGroupResponse.setDepartment("责任部门");
            terminalGroupResponse.setTeNumber("480");
            terminalGroupResponse.setTeName("PBN能力");
            terminalGroupResponse.setTeRunPhaseName("终端运行阶段");
            terminalGroupResponse.setGroupRunPhaseName("群组运行阶段");
            terminalGroupResponse.setGroupName("群组名称");
            terminalGroupResponse.setGroupId("207");
            terminalGroupResponse.setCreateUser("system");
            terminalGroupResponse.setGroupAttribute("群组属性");
            terminalGroupResponse.setFormulaRule("E1:机型[全部]=A320;E2:机场标高[全部]≥8000;E3:机场标高[全部]≤9200");
            terminalGroupResponse.setRemission("/");
            terminalGroupResponse.setFormulaCondition("E=E1且E2");
            terminalGroupResponse.setHomologousCondition("E1,E3,E2同源关系为或");
            terminalGroupResponse.setRemissionMeasures("检查飞机高原适应性、飞行员飞行资质、一发失效程序、着陆及起飞重量检查、航路备降机场分析、严格按照高高原机场运行检查单执行");
            terminalGroupResponse.setFormulaDescription("超过机型限制跑道坡度，禁止在该机场运行");
            terminalGroupResponse.setTimeFilter("");
            if (i%2==0) {
                terminalGroupResponse.setRiskLevel("禁风险");
                terminalGroupResponse.setRiskValue("30.0");
            }else {
                terminalGroupResponse.setRiskValue("10.01");
                terminalGroupResponse.setRiskLevel("高风险");
            }
            terminalGroupResponse.setRiskPush("");
            terminalGroupResponse.setNature("显性");
            terminalGroupResponse.setGroupState("未生效");
            users.add(terminalGroupResponse);
        }
        // 导出模板
        ClassPathResource classPathResource = new ClassPathResource("/template/template-alibaba.xlsx");
        try {
            long time1 = System.currentTimeMillis();
            InputStream templateFileName = classPathResource.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ExcelWriter excelWriter = EasyExcel.write(baos).withTemplate(templateFileName).build();
            WriteSheet writeSheet = EasyExcel.writerSheet().build();
            // 直接写入数据
            excelWriter.fill(users, writeSheet);
            excelWriter.fill(users, writeSheet);
            // 千万别忘记关闭流
            excelWriter.finish();
            long time2 = System.currentTimeMillis() - time1;
            logger.info("生成excel-{}", time2);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date today = new Date();
            //以今天的日期为文件名
            String fileName = "终端群组导出-" + sdf.format(today);
            response.setContentType("application/msexcel;charset=UTF-8");
            response.setHeader("Content-disposition", "attachment;filename=\"" + new String((fileName + ".xlsx").getBytes("GBK"), "ISO8859-1") + "\"");
            //response字符流转换成字节流，template需要字节流作为输出
            OutputStream outputStream = response.getOutputStream();
            outputStream.write(baos.toByteArray());
            outputStream.flush();
            outputStream.close();
        }catch (Exception e){
            logger.error("错误信息{}", e.getMessage());
        }
    }
```

**使用:**

```
    @GetMapping("/exportGroupExcel")
    public void riskTableExcel(HttpServletResponse response) {
        long time1 = System.currentTimeMillis();
        ApiResponse<byte[]> apiResponse = terminalElementGroupInfoApi.exportTerminalGroup(Lists.newArrayList("A"), Lists.newArrayList(0,1), Lists.newArrayList(1));
        long time2 = System.currentTimeMillis() - time1;
        logger.info("调用接口花费总时间{}", time2);
        try {
            if (apiResponse.isSuccess()) {
                long time3 = System.currentTimeMillis();
                byte[] excelFile = apiResponse.getData();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date today = new Date();
                //以今天的日期为文件名
                String fileName = "终端群组导出-" + sdf.format(today);
                response.setContentType("application/msexcel;charset=UTF-8");
                response.setHeader("Content-disposition", "attachment;filename=\"" + new String((fileName + ".xlsx").getBytes("GBK"), "ISO8859-1") + "\"");
                //response字符流转换成字节流，template需要字节流作为输出
                OutputStream outputStream = response.getOutputStream();
                outputStream.write(excelFile);
                outputStream.flush();
                outputStream.close();
                long time4 = System.currentTimeMillis() - time3;
                logger.info("返回数据花费时间{}", time4);
            } else {
                logger.error(JSON.toJSONString(apiResponse));
                response.getWriter().write(JSON.toJSONString(apiResponse));
            }

        } catch (Exception e) {
            logger.warn(JSON.toJSONString(e));
        }
    }
```

```
@Override
    public ApiResponse<byte[]> exportTerminalGroup(List<String> dimension, List<Integer> groupState, List<Integer> externalGroup) {
        try {
            Future<byte[]> objectsFuture = terminalBiz.exportTerminalGroup(dimension, groupState, externalGroup);
//            if (objectsFuture.isDone()) {
                return ApiResponse.success(objectsFuture.get());
//            }
        } catch (Exception e) {
            logger.error("导出终端群组列表excel文件异常，e-{}", JSON.toJSONString(e));
            throw new RuntimeException(e);
        }
    }
```

```
@Async
    public Future<byte[]> exportTerminalGroup(List<String> dimension, List<Integer> groupState, List<Integer> externalGroup) throws IOException {
        // 导出模板
        ClassPathResource classPathResource = new ClassPathResource("/template/terminal-export-template.xlsx");
        // 最终数据
        List<TerminalGroupResponse> responses = new ArrayList<>();
        long time1 = System.currentTimeMillis();
        // 根据群组类型和群组状态查询群组
        List<OcrTerminalElementGroupPO> groupPOS = new ArrayList<>();
        if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(externalGroup)) {
            groupPOS = terminalElementGroupDaoService.list(new QueryWrapper<OcrTerminalElementGroupPO>().in("group_type", externalGroup).in("group_state", groupState));
        }
        List<Integer> teIds = new ArrayList<>();
        if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(groupPOS)) {
            // 得到终端id
            teIds = groupPOS.stream().map(OcrTerminalElementGroupPO::getTeId).distinct().collect(toList());
        }

        // 根据维度和上面筛选的终端id查询终端
        List<OcrTerminalElementPO> terminalElementPOS = terminalElementDaoService.list(new QueryWrapper<OcrTerminalElementPO>().in("te_id", teIds).in("te_dimension", dimension));
        if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isEmpty(terminalElementPOS)) {
            throw new RuntimeException("该条件下暂无终端");
        }
        // key为telId value为终端信息
        Map<Integer, OcrTerminalElementPO> teIdInfoMap = terminalElementPOS.stream().collect(toMap(OcrTerminalElementPO::getTeId, terminalInfo -> terminalInfo));

        // 查询运行阶段
        List<RunPhaseEntity> runPhaseServiceAll = runPhaseService.findAll();
        // key为运行阶段id, value为运行阶段名称
        Map<String, String> runPhaseIdNameMap = runPhaseServiceAll.stream().collect(toMap(RunPhaseEntity::getId, runPhaseEntity -> runPhaseEntity.getRunPhaseName()));
        long time2 = System.currentTimeMillis() - time1;
        logger.info("查询所费时间{}", time2);
        // 如果群组不为空
        if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(groupPOS)) {
            // 拿到终端id
            List<Integer> telIds = terminalElementPOS.stream().map(OcrTerminalElementPO::getTeId).collect(toList());
            // 再将上面所查出的群组过滤一遍
            groupPOS = groupPOS.stream().filter(group -> telIds.contains(group.getTeId())).collect(toList());

            // 拿到群组id
            List<Integer> groupIds = groupPOS.stream().map(OcrTerminalElementGroupPO::getGroupId).collect(toList());

            long time5 = System.currentTimeMillis();
            // 群组属性
            ApiResponse<Map<Integer, List<TerminalElementGroupAttributeResponse>>> attributeResponse = terminalElementGroupAttributeApi.findAttributeByGroupIds(groupIds);
            if (!attributeResponse.isSuccess()) {
                throw new RuntimeException("调用群组属性查询接口失败");
            }
            Map<Integer, List<TerminalElementGroupAttributeResponse>> data = attributeResponse.getData();
            long endTime5 = System.currentTimeMillis() - time5;
            logger.info("查询群组属性时间{}", endTime5);

            // 群组时间过滤大区间
            List<OcrTerminalElementGroupTimeFilterPO> timeFilterPOS = terminalElementGroupTimeFilterDaoService.list(new QueryWrapper<OcrTerminalElementGroupTimeFilterPO>().in("group_id", groupIds));
            // key为groupId  value为timeFilter
            Map<Integer, OcrTerminalElementGroupTimeFilterPO> groupIdTimeFilterMap = Maps.newHashMap();
            if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(timeFilterPOS)) {
                groupIdTimeFilterMap = timeFilterPOS.stream().collect(toMap(OcrTerminalElementGroupTimeFilterPO::getGroupId, timeFilter -> timeFilter));
            }
            // 群组时间过滤小区间
            List<OcrTerminalElementGroupTimeIntervalPO> timeIntervalPOS = terminalElementGroupTimeIntervalDaoService.list(new QueryWrapper<OcrTerminalElementGroupTimeIntervalPO>().in("group_id", groupIds));
            // key为groupId, value为timeInterval
            Map<Integer, List<OcrTerminalElementGroupTimeIntervalPO>> groupIdTimeIntervalMap = Maps.newHashMap();
            if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(timeIntervalPOS)) {
                groupIdTimeIntervalMap = timeIntervalPOS.stream().collect(groupingBy(OcrTerminalElementGroupTimeIntervalPO::getGroupId));
            }

            // 根据群组id查询岗位推送
            List<OcrTerminalElementGroupRiskPushPO> groupRiskPushPOS = terminalElementGroupRiskPushDaoService.list(new QueryWrapper<OcrTerminalElementGroupRiskPushPO>().in("group_id", groupIds));
            // groupId为key, roleId集合为value
            Map<Integer, List<OcrTerminalElementGroupRiskPushPO>> groupIdRoleInfoMap = groupRiskPushPOS.stream().collect(groupingBy(OcrTerminalElementGroupRiskPushPO::getGroupId));
            long time6 = System.currentTimeMillis();
            ApiResponse<List<FocUserRoleBaseInfoResponse>> focUserRoleApiFocUserRoles = focUserRoleApiServiceImpl.getFocUserRoles();
            if (!focUserRoleApiFocUserRoles.isSuccess()) {
                throw new RuntimeException("调用岗位查询接口失败");
            }
            List<FocUserRoleBaseInfoResponse> focUserRolesData = focUserRoleApiFocUserRoles.getData();
            // roleId为key, roleName为value
            Map<String, String> roleIdInfoMap = focUserRolesData.stream().collect(toMap(FocUserRoleBaseInfoResponse::getRoleId, focUserRoleBaseInfoResponse -> focUserRoleBaseInfoResponse.getRoleName()));
            logger.info("查询岗位时间{}", System.currentTimeMillis()-time6);
            // 根据群组id查询主公式
            List<OcrTerminalElementGroupFormulaPO> formulaPOS = terminalElementGroupFormulaDaoService.list(new QueryWrapper<OcrTerminalElementGroupFormulaPO>().in("group_id", groupIds));
            // key为群组id value为主公式集合
            Map<Integer, List<OcrTerminalElementGroupFormulaPO>> groupIdFormulaMap = Maps.newHashMap();
            // key为主公式id value为子公式集合
            Map<Long, List<OcrTerminalElementGroupFormulaSubPO>> formulaIdSubMap = Maps.newHashMap();
            if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(formulaPOS)) {
                groupIdFormulaMap = formulaPOS.stream().collect(groupingBy(OcrTerminalElementGroupFormulaPO::getGroupId));
                List<Long> mainFormulaIds = formulaPOS.stream().map(OcrTerminalElementGroupFormulaPO::getFormulaId).collect(toList());
                // 根据主公式id查询子公式
                List<OcrTerminalElementGroupFormulaSubPO> subPOS = terminalElementGroupFormulaSubDaoService.list(new QueryWrapper<OcrTerminalElementGroupFormulaSubPO>().in("main_formula_id", mainFormulaIds));
                formulaIdSubMap = subPOS.stream().collect(groupingBy(OcrTerminalElementGroupFormulaSubPO::getMainFormulaId));
            }

            Map<Integer, List<OcrTerminalElementGroupFormulaPO>> finalGroupIdFormulaMap = groupIdFormulaMap;
            Map<Long, List<OcrTerminalElementGroupFormulaSubPO>> finalFormulaIdSubMap = formulaIdSubMap;
            Map<Integer, OcrTerminalElementGroupTimeFilterPO> finalGroupIdTimeFilterMap = groupIdTimeFilterMap;
            Map<Integer, List<OcrTerminalElementGroupTimeIntervalPO>> finalGroupIdTimeIntervalMap = groupIdTimeIntervalMap;
            // 循环群组
            long groupTime = System.currentTimeMillis();
            groupPOS.forEach(groupPO -> {
                List<OcrTerminalElementGroupRiskPushPO> terminalElementGroupRiskPushPOS = groupIdRoleInfoMap.get(groupPO.getGroupId());
                if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(finalGroupIdFormulaMap)) {
                    List<OcrTerminalElementGroupFormulaPO> formulas = finalGroupIdFormulaMap.get(groupPO.getGroupId());
                    // 循环主公式并组装返回数据
                    List<TerminalGroupResponse> groupResponses = formulaPOS.stream().map(formula -> {
                        // 组装群组信息
                        TerminalGroupResponse terminalGroupResponse = setGroupResponse(groupPO, runPhaseIdNameMap, data);
                        // 组装终端信息
                        terminalGroupResponse = setTelInfo(terminalGroupResponse, groupPO.getTeId(), teIdInfoMap, runPhaseIdNameMap);
                        // 风险描述
                        terminalGroupResponse.setFormulaDescription(StringUtils.isEmpty(formula.getFormulaDescription()) ? "/" : formula.getFormulaDescription());
                        // 风险值
                        terminalGroupResponse.setRiskValue(Objects.isNull(formula.getRiskValue()) ? "/" : String.valueOf(formula.getRiskValue()));
                        // 风险等级
                        String riskLevel = formula.getRiskLevel();
                        if (Objects.equals(riskLevel, "F")) {
                            terminalGroupResponse.setRiskLevel("禁风险");
                        } else if (Objects.equals(riskLevel, "H")) {
                            terminalGroupResponse.setRiskLevel("高风险");
                        } else if (Objects.equals(riskLevel, "M")) {
                            terminalGroupResponse.setRiskLevel("中风险");
                        } else {
                            terminalGroupResponse.setRiskLevel("低风险");
                        }
                        if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(terminalElementGroupRiskPushPOS)) {
                            // 群组岗位推送(一个等级只有一条数据)
                            OcrTerminalElementGroupRiskPushPO ocrTerminalElementGroupRiskPushPO = terminalElementGroupRiskPushPOS.stream().filter(groupRiskPushPO -> Objects.equals(groupRiskPushPO.getPushRiskLevel(), riskLevel)).collect(toList()).get(0);
                            // 拿到岗位id集合
                            String pushFocPost = ocrTerminalElementGroupRiskPushPO.getPushFocPost();
                            List<String> postIds = Stream.of(pushFocPost.split(",")).collect(toList());
                            // 拿到岗位名字
                            List<String> postNames = postIds.stream().map(postId -> {
                                String postName = roleIdInfoMap.get(postId);
                                return postName;
                            }).collect(toList());
                            String name = postNames.stream().collect(joining(","));
                            // 群组岗位推送
                            terminalGroupResponse.setRiskPush(name);
                        }
                        // 缓解措施
                        terminalGroupResponse.setRemissionMeasures(StringUtils.isEmpty(formula.getRemissionMeasures()) ? "/" : formula.getRemissionMeasures());
                        // 缓解公式
                        terminalGroupResponse.setRemission(formula.getFormulaRemission() == 0 ? "/" : "是");
                        // 群组性质
                        terminalGroupResponse.setNature(formula.getNature() == 0 ? "显性" : "隐性");
                        // 时间过滤大区间
                        if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(finalGroupIdTimeFilterMap)) {
                            OcrTerminalElementGroupTimeFilterPO ocrTerminalElementGroupTimeFilterPO = finalGroupIdTimeFilterMap.get(groupPO.getGroupId());
                            if (Objects.nonNull(ocrTerminalElementGroupTimeFilterPO)) {
                                LocalDateTime startTime = ocrTerminalElementGroupTimeFilterPO.getStartTime();
                                String start = LocalDateTimeUtils.formatTime(startTime, null);
                                Integer endTimeState = ocrTerminalElementGroupTimeFilterPO.getEndTimeState();
                                String end;
                                start = "开始时间:" + start;
                                if (Objects.equals(endTimeState, 1)) {
                                    end = "PERM-结束时间为永久";
                                } else if (Objects.equals(endTimeState, 0)) {
                                    LocalDateTime endTime = ocrTerminalElementGroupTimeFilterPO.getEndTime();
                                    end = LocalDateTimeUtils.formatTime(endTime, null);
                                    end = "EST-预计结束时间:" + end;
                                } else {
                                    LocalDateTime endTime = ocrTerminalElementGroupTimeFilterPO.getEndTime();
                                    end = LocalDateTimeUtils.formatTime(endTime, null);
                                    end = "结束时间:" + end;
                                }
                                terminalGroupResponse.setTimeFilter(start + "\r\n" + end);
                            }
                        } else {
                            terminalGroupResponse.setTimeFilter("/");
                        }
                        // 时间过滤小区间
                        if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(finalGroupIdTimeIntervalMap)) {
                            List<OcrTerminalElementGroupTimeIntervalPO> terminalElementGroupTimeIntervalPOS = finalGroupIdTimeIntervalMap.get(groupPO.getGroupId());
                            if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(terminalElementGroupTimeIntervalPOS)) {
                                List<String> intervalContent = new ArrayList<>();
                                terminalElementGroupTimeIntervalPOS.forEach(timeIntervalPO -> {
                                    String timeIntervalContent = timeIntervalPO.getTimeIntervalContent();
                                    Stream.of(timeIntervalContent.split("\\|")).collect(toList()).forEach(interval -> {
                                        intervalContent.add(interval);
                                    });
                                });
                                String timeFilter = terminalGroupResponse.getTimeFilter();
                                if (Objects.equals(timeFilter, "/")) {
                                    timeFilter = intervalContent.stream().collect(joining("\r\n"));
                                } else {
                                    timeFilter = timeFilter + "\r\n" + intervalContent.stream().collect(joining("\r\n"));
                                }
                                terminalGroupResponse.setTimeFilter(timeFilter);
                            }
                        }
                        // 同源关系公式
                        String homologyFormula = formula.getHomologyFormula();
                        List<String> homologs = new ArrayList<>();
                        String homologStr = null;
                        if (StringUtils.isNotEmpty(homologyFormula)) {
                            homologs = JSON.parseArray(homologyFormula, String.class);
                            // 去除掉空元素
                            homologs = homologs.stream().filter(homolog -> StringUtils.isNotEmpty(homolog)).collect(toList());
                            if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(homologs)) {
                                homologs.forEach(homo -> {
                                    homo.replace(",", "/");
                                });
                            }
                            homologStr = homologs.stream().collect(joining(","));
                        }
                        Integer homologyRelation = formula.getHomologyRelation();
                        String relation = new StringBuffer("同源关系为").append(Objects.equals(homologyRelation, 0) ? "且" : "或").toString();
                        terminalGroupResponse.setHomologousCondition(com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isEmpty(homologs) ? "无" : homologStr + relation);
                        // 公式关系
                        String riskFormula = formula.getRiskFormula();
                        riskFormula = riskFormula.replace("||", "或");
                        riskFormula = riskFormula.replace("&&", "且");
                        terminalGroupResponse.setFormulaCondition(StringUtils.isEmpty(riskFormula) ? "无" : riskFormula);
                        // 公式规则
                        if (com.baomidou.mybatisplus.core.toolkit.CollectionUtils.isNotEmpty(finalFormulaIdSubMap)) {
                            List<OcrTerminalElementGroupFormulaSubPO> terminalElementGroupFormulaSubPOS = finalFormulaIdSubMap.get(formula.getFormulaId());
                            List<String> subRule = new ArrayList<>();
                            terminalElementGroupFormulaSubPOS.forEach(sub -> {
                                String seqName = "E" + sub.getFormulaSeq();
                                String formulaContent = sub.getFormulaContent();
                                subRule.add(seqName + ":" + formulaContent);
                            });
                            terminalGroupResponse.setFormulaRule(subRule.stream().collect(joining("\r\n")));
                        }
                        return terminalGroupResponse;
                    }).collect(toList());
                    responses.addAll(groupResponses);
                } else {
                    TerminalGroupResponse terminalGroupResponse = setGroupResponse(groupPO, runPhaseIdNameMap, data);
                    terminalGroupResponse = setTelInfo(terminalGroupResponse, groupPO.getTeId(), teIdInfoMap, runPhaseIdNameMap);
                    List<String> names = new ArrayList<>();
                    terminalElementGroupRiskPushPOS.forEach(riskPush -> {
                        String pushRiskLevel = riskPush.getPushRiskLevel();
                        String pushFocPost = riskPush.getPushFocPost();
                        List<String> postIds = Stream.of(pushFocPost.split(",")).collect(toList());
                        String levelName;
                        if (Objects.equals(pushRiskLevel, "F")) {
                            levelName = "禁风险";
                        } else if (Objects.equals(pushRiskLevel, "H")) {
                            levelName = "高风险";
                        } else if (Objects.equals(pushRiskLevel, "M")) {
                            levelName = "中风险";
                        } else {
                            levelName = "低风险";
                        }
                        // 拿到岗位名字
                        List<String> postNames = postIds.stream().map(postId -> {
                            String postName = roleIdInfoMap.get(postId);
                            return postName;
                        }).collect(toList());
                        names.add(levelName + ":" + postNames.stream().collect(joining(",")));
                    });
                    terminalGroupResponse.setRiskPush(names.stream().collect(Collectors.joining("\r\n")));
                    responses.add(terminalGroupResponse);
                }
            });
            long endGroup = System.currentTimeMillis() - groupTime;
            logger.info("组装群组的时间{}", endGroup);
        } else {
            long telTime = System.currentTimeMillis();
            // 如果群组为空就只组装终端
            terminalElementPOS.forEach(terminal -> {
                TerminalGroupResponse terminalGroupResponse = new TerminalGroupResponse();
                terminalGroupResponse.setTeName(terminal.getTeName());
                String teDimension = terminal.getTeDimension();
                switch (teDimension){
                    case "A":
                        teDimension = "机";
                        break;
                    case "C":
                        teDimension = "人";
                        break;
                    case "E":
                        teDimension = "环";
                        break;
                    default:
                        teDimension = "/";
                        break;
                }
                terminalGroupResponse.setDimension(teDimension);
                terminalGroupResponse.setTeNumber(String.valueOf(terminal.getTeId()));
                terminalGroupResponse.setTeRunPhaseName(runPhaseIdNameMap.get(terminal.getRunPhaseId()));
                responses.add(terminalGroupResponse);
            });
            long endTelTime = System.currentTimeMillis() - telTime;
            logger.info("组装终端的时间{}", endTelTime);
        }
        long start = System.currentTimeMillis();
        byte[] excelFile = ExportExcelUtils.exportByTemplate(responses, classPathResource.getInputStream());
        long exportTotal = System.currentTimeMillis() - start;
        logger.info("工具类生成excel的时间耗费{}", exportTotal);
        return new AsyncResult<byte[]>(excelFile);
    }

```

```
 /**
     * @param data
     * @param templateFile
     * @return
     */
    public static byte[] exportByTemplate(List<TerminalGroupResponse> data, InputStream templateFile){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ExcelWriter excelWriter = EasyExcel.write(baos).withTemplate(templateFile).build();
        WriteSheet writeSheet = EasyExcel.writerSheet().build();
        // 直接写入数据
        excelWriter.fill(data, writeSheet);
        excelWriter.fill(data, writeSheet);
        // 千万别忘记关闭流
        excelWriter.finish();
        return baos.toByteArray();
    }
```

参考资料:[填充Excel · 语雀 (yuque.com)](https://www.yuque.com/easyexcel/doc/fill)

