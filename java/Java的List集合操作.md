**Java List去掉重复对象Java8**

**一、去除List中重复的String**

1.    Set<String> set = new LinkedHashSet<>();
2.    set.addAll(list);
3.    list.clear();
4.    list.addAll(set);

或使用Java8的写法：

List<String> list = list.stream().distinct().collect(Collectors.toList());

**二、List中对象去重**

1. public class Student
2.    private Long id;
3.    private String name;
4.  

重写Student对象的equals()方法和hashCode()方法:

1. @Override
2.    public boolean equals(Object o) {
3. ​       if (this == o) return true;
4. ​       if (o == null || getClass() != o.getClass()) return false;
5. ​       Student Student = (Student) o;
6. ​       if (!id.equals(Student.id)) return false;
7. ​       return name.equals(Student.name);
8.    }
9.    @Override
10.    public int hashCode() {
11. ​       int result = id.hashCode();
12. ​       result = 19 * result + name.hashCode();
13. ​       return result;
14.    }
15.  
16.  
17. ​       List<Student> students = Arrays.asList(s1, s2, s3);
18. ​       List<Student> studentsNew = new ArrayList<>();
19. ​       *// 去重*
20. ​       students.stream().forEach(
21. ​               s -> {
22. ​                   if (!studentsNew.contains(s)) {
23. ​                       studentsNew.add(s);
24. ​                   }
25. ​               }
26. ​           );
27.  
28. List的contains()方法实际上是用对象的equals方法去比较

 

**三、根据对象的属性去重**

1. ​       Set<Student> studentSet = new TreeSet<>((o1, o2) -> o1.getId().compareTo(o2.getId()));
2. ​       studentSet.addAll(students);

Java8写法

List<Student> list = Students.stream().collect(collectingAndThen(toCollection(() -> new TreeSet<>(comparingLong(Student::getId))), ArrayList::new));

出于项目需要,有一个list,实体包含多个字段,当其中两个字段相同均相同时,就认为list中的两条记录是一样的,本来想通过分组实现,java8的分组只提供根据一个字段分组,只好另寻他路,java8有一个collectingAndThen可以根据多个字段去重,因为我们的需求是可以去重之后操作,因此采用这种方式。

**分组**

classEntities.stream().collect(Collectors.groupingBy(ClassEntity::getGrade)); **java8去重(根据年级和专业,当年级和专业都相同的情况下看做是重复数据)**

**List<ClassEntity> distinctClass = classEntities.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getProfessionId() + ";" + o.getGrade()))), ArrayList::new));**

**通过hashSet去重(如将classNames去重):该种去重是bean完全相同的时候算重复数据**

List<String> classNameList = new ArrayList(new HashSet(classNames));

**使用lambda表达式的感受**

lambda表达式极大的简化了代码，让整个代码的排班更加精简突显风格，但是却也增加了阅读的难度，特别是对原理不熟悉或者阅读他人代码的情况下进行调式的难度会增加。

list按字段排序

List<PromotionForHomeDto> list = promotionBasicDao.

list(new WherePrams().orderBy("create_time desc"));

**list = list.stream().sorted(Comparator.comparing(PromotionForHomeDto::getCreateTime)) .collect(Collectors.toList());**



根据某个字段排序

List<User> newList = list.stream().sorted(Comparator.comparing(User::getAge))

​        .collect(Collectors.toList());

for(User u :newList){

System.out.println(u);

}

}

}



List<User> newList = list.stream().sorted(Comparator.comparing(User::getAge))**

​        .collect(Collectors.toList());

for(User u :newList){

System.out.println(u);

}}}



**initialCapacity = (需要存储的元素个数 / 负载因子) + 1。 注意负载因子（即 loader factor） 默认**

**为 0.75，如果暂时无法确定初始值大小，请设置为 16（即默认值） 。**

| 集合类            | Key           | Value         | Super       | 说明                    |
| ----------------- | ------------- | ------------- | ----------- | ----------------------- |
| Hashtable         | 不允许为 null | 不允许为 null | Dictionary  | 线程安全                |
| ConcurrentHashMap | 不允许为 null | 不允许为 null | AbstractMap | 锁分段技术（ JDK8:CAS） |
| TreeMap           | 不允许为 null | 允许为 null   | AbstractMap | 线程不安全              |
| HashMap           | 允许为 null   | 允许为 null   | AbstractMap | 线程不安全              |

反例： 由于 HashMap 的干扰，很多人认为 ConcurrentHashMap 是可以置入 null 值，而事实上， 存储

null 值时会抛出 NPE 异常。

合理利用好集合的有序性(sort)和稳定性(order)，避免集合的无序性(unsort)和不稳

定性(unorder)带来的负面影响。

说明： 有序性是指遍历的结果是按某种比较规则依次排列的。 稳定性指集合每次遍历的元素次序是一定

的。 如： **ArrayList 是 order/unsort； HashMap 是 unorder/unsort； TreeSet 是 order/sort**。