这是第N次在项目中遇到需要将数据分批处理的情况了。以前在用jdbcTemplate插入数据时，遇到过，当时花了好几分钟进行了分批处理，用LIST的SUBLIST方法进行了分批。此算法比较简单，但想起来，也得花个分把钟，在时间紧急的情况下，还是"拿来主义"比较方便点。现在把它贴出来，存在这，供以后用到时方便查询

代码如下：

import java.util.ArrayList;
import java.util.List;

public class Test {


    public static void main(String[] args) {
        doTest();
    }
    
    private static void doTest() {
        List<String> dataList = new ArrayList<>();
        dataList.add("11");
        dataList.add("22");
        dataList.add("33");
        dataList.add("44");
        dataList.add("55");
        dataList.add("66");
        dataList.add("77");
        dataList.add("88");
        dataList.add("99");
    
        listExcute(dataList);
    
    }
    
    private static void listExcute(List<String> dataList) {
        if (null != dataList && dataList.size() > 0) {
            int pointsDataLimit = 2;//限制条数,【修改这里】
            Integer size = dataList.size();
            if (pointsDataLimit < size) {//判断是否有必要分批
                int part = size / pointsDataLimit;//分批数
                System.out.println("共有 ： " + size + "条，！" + " 分为 ：" + part + "批");
                for (int i = 0; i < part; i++) {
                    List<String> listPage = dataList.subList(0, pointsDataLimit);
                    System.out.println("第" + (i + 1) + "次,执行处理:" + listPage);
                    dataList.subList(0, pointsDataLimit).clear();
                }
                if (!dataList.isEmpty()) {
                    System.out.println("最后一批数据,执行处理:" + dataList);//表示最后剩下的数据
                }
            } else {
                System.out.println("不需要分批,执行处理:" + dataList);
            }
        } else {
            System.out.println("没有数据!!!");
        }
    }
}

主要函数：

```
if (null != dataList && dataList.size() > 0) {
    int pointsDataLimit = 2;//限制条数,【修改这里】
    Integer size = dataList.size();
    if (pointsDataLimit < size) {//判断是否有必要分批
        int part = size / pointsDataLimit;//分批数
        System.out.println("共有 ： " + size + "条，！" + " 分为 ：" + part + "批");
        for (int i = 0; i < part; i++) {
            List<String> listPage = dataList.subList(0, pointsDataLimit);
            System.out.println("第" + (i + 1) + "次,执行处理:" + listPage);
            dataList.subList(0, pointsDataLimit).clear();
        }
        if (!dataList.isEmpty()) {
            System.out.println("最后一批数据,执行处理:" + dataList);//表示最后剩下的数据
        }
    } else {
        System.out.println("不需要分批,执行处理:" + dataList);
    }
} else {
    System.out.println("没有数据!!!");
}
```

测试结果为：

共有 ： 9条，！ 分为 ：4批
第1次,执行处理:[11, 22]
第2次,执行处理:[33, 44]
第3次,执行处理:[55, 66]
第4次,执行处理:[77, 88]
最后一批数据,执行处理:[99]