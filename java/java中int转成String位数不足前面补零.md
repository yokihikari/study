```
String.format("%010d", 25); //25为int型 
```

0代表前面要补的字符
10代表字符串长度
d表示参数为整数类型

今天想将int 转String 位数不够前面补零，在本来想看看有没有现成的API的，结果搜出来的大多数下面这个

```
public static String addZeroForNum(String str,int strLength) {  
  int strLen =str.length();  
  if (strLen <strLength) {  
   while (strLen< strLength) {  
    StringBuffersb = new StringBuffer();  
    sb.append("0").append(str);//左补0  
//    sb.append(str).append("0");//右补0  
    str= sb.toString();  
    strLen= str.length();  
   }  
  }  

  return str;  
 }  1234567891011121314
```

不过我觉得有点麻烦，自己想了想想到一个稍微简单点的方法，如下一行即可

```
String str = String.format("%5d", num).replace(" ", "0");  1
```

其中num是int， str 是转换后的结果。很简单吧

最近我又搜了关于String.format的东西，其实有自带的补零方法，

```
String.format("%06",12);//其中0表示补零而不是补空格，6表示至少6位  
```