代码如下:


mport java.text.DecimalFormat; 



DecimalFormat  df  = new DecimalFormat("######0.00"); 

double d1 = 3.23456 
double d2 = 0.0;
double d3 = 2.0;
df.format(d1);
df.format(d2);
df.format(d3);



3个结果分别为:

复制代码代码如下:


3.23
0.00
2.00



java保留两位小数问题：

方式一：

四舍五入 

复制代码代码如下:


double  f  =  111231.5585; 
BigDecimal  b  =  new  BigDecimal(f); 
double  f1  =  b.setScale(2,  BigDecimal.ROUND_HALF_UP).doubleValue(); 



保留两位小数 

方式二：

复制代码代码如下:


java.text.DecimalFormat  df  =new  java.text.DecimalFormat("#.00"); 
df.format(你要格式化的数字);



例：

复制代码代码如下:


new java.text.DecimalFormat("#.00").format(3.1415926)



\#.00 表示两位小数 #.0000四位小数 以此类推...

方式三：

复制代码代码如下:


double d = 3.1415926;
String result = String .format("%.2f");



%.2f %. 表示 小数点前任意位数  2 表示两位小数 格式后的结果为f 表示浮点型

方式四：

复制代码代码如下:


NumberFormat ddf1=NumberFormat.getNumberInstance() ;
void setMaximumFractionDigits(int digits)



digits 显示的数字位数
为格式化对象设定小数点后的显示的最多位,显示的最后位是舍入的



复制代码代码如下:


import java.text.* ;
import java.math.* ;
class TT
{
public static void main(String args[])
{ double x=23.5455;
NumberFormat ddf1=NumberFormat.getNumberInstance() ;



ddf1.setMaximumFractionDigits(2);

```
/ 如果不需要四舍五入，可以使用RoundingMode.DOWN
        nf.setRoundingMode(RoundingMode.HALF_UP);
```

String s= ddf1.format(x) ;
System.out.print(s);
}
}





复制代码代码如下:


import java.text.*;



DecimalFormat df=new DecimalFormat(".##");
double d=1252.2563;
String st=df.format(d);
System.out.println(st);