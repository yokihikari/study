很多小伙伴在刚开始用Springboot整合jsp开发时都会遇到这个问题, 按照别人的教程一步一步搭建, 但就是会报404, 在网上找遍了方法都解决不了。笔者以前学springboot的时候因为这个问题忙活了整个下午才把问题解决,。最近看到有小伙伴在群里问， 所以打算把我已知的解决方法整理起来, 希望对大家有所帮助。不足之处,欢迎大家指正, 若还有其他方法, 欢迎补充。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719142102196.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)


首先这个问题很诡异, Springboot版本的不同, IDEA的版本不同, 都有可能造成404, 笔者有2017,2018,2019三个版本的IDEA, 按照相同的步骤搭建项目, 有的直接就能访问, 有的则需要配置一下才能正常访问。

一般404分两种情况, 一种是请求未找到Controller里RequestMapping配置的路径, 还有一种情况是请求找到了对应路径, 但是在跳转页面时找不到对应的资源。你可以用debug的方式来查看你是属于哪一种情况。

先说第一种情况:
首先查看页面请求地址是否与@RequestMapping里的地址一样, 然后再看项目启动日志里的项目路径, 是否与application文件里配置的路径一致, Springboot 2.x 配置项目路径用的是server.servlet.context-path, 而1.x用的是server.context-path, 当2.x版本的Springboot用1.x的配置方式时会报红, 但1.x版本的Springboot用2.x的配置方式不会, 这是一个大坑。
若配置错误，图中红色方框内的路径就是[/] 。

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719134739572.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719134756443.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)

第二种情况, 以下的3、4、5、6方法不是必须, 只是某些版本的IDEA创建的某个版本的Springboot项目才需要其中的一个或多个步骤, 你可以每个都试一试, 总有一个方法能解决你的问题:

方法1、 查看pom.xml文件里是否加入了正确的依赖, 记住provided需要删除或者注释掉

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719134812981.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)

方法2、 查看application文件配置信息， Springboot 2.0以下版本 用spring.mvc.view.prefix 和spring.mvc.view.suffix, 2.0以上版本用spring.view.prefix 和 spring.view.suffix

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719140311755.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)


方法3、 查看编译文件夹target -> classes里是否有你存放的jsp文件, 若没有, 则右键webapp -> Mark Directory as -> Resources Root, 然后在clean一下项目, 重新启动, classes里面就有了对应的jsp文件

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719140436963.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719140452120.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719140459504.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)

方法4、 ctrl + alt + shift + s打开Project Structure -> Modules -> 你的项目模块 -> web 添加web.xml 应用

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719152131127.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)

设置完成后页面如下

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719152151266.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)

方法5、 ctrl + alt + shift + s打开Project Structure -> Artifacts -> xxx exploded -> + -> Directory Content 添加webapp 应用

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719140607958.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719140620508.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719140706166.png)

下图表示添加成功
方法6、 若项目是多个模块, 如图, 则需要更改工作路径. 打开Run/Debug Configurations -> springboot -> 你项目的运行主类, 设置Working Directory 为 MODULE_DIR或者 MODULE_WORKING_DIR 应用

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190719140653202.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zNzUwNTAxNA==,size_16,color_FFFFFF,t_70)


若以上方法都试过一遍还不行, 那就放最后的大招吧。。。。

把appliction配置文件里配置的spring.view.prefix=/WEB-INF/jsp/ spring.view.suffix=.jsp 都注释掉, 新建一个配置类来代替它们。


@Configuration
public class JspWebConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/");
        viewResolver.setSuffix(".jsp");
        viewResolver.setViewClass(JstlView.class);
        return viewResolver;
    }
}

然后再重启项目， 就可以访问了。
代码示例： https://github.com/jiangjc3/springboot-jsp
希望能够解决你的问题， 以上！