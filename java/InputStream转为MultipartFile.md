# [         ](https://www.cnblogs.com/yadongliang/p/13578889.html)             



**目录导航**

- 方式一 CommonsMultipartFile
  - [pom](https://www.cnblogs.com/yadongliang/p/13578889.html#_label0_0)
  - [test](https://www.cnblogs.com/yadongliang/p/13578889.html#_label0_1)
- [方式二 mockFile](https://www.cnblogs.com/yadongliang/p/13578889.html#_label1)
- 通过restTemplate调用远程接口,实现文件上传
  - [入参为multipartFile](https://www.cnblogs.com/yadongliang/p/13578889.html#_label2_0)
  - [入参为inpurtStream](https://www.cnblogs.com/yadongliang/p/13578889.html#_label2_1)
- 感谢
  - [通过`RestTemplate`上传文件(InputStreamResource详解)](https://www.cnblogs.com/yadongliang/p/13578889.html#_label3_0)

 

------

[目录导航](https://www.cnblogs.com/yadongliang/p/13578889.html#_labelTop)

## 方式一 CommonsMultipartFile



### pom

```
<!-- https://mvnrepository.com/artifact/commons-fileupload/commons-fileupload -->
<dependency>
    <groupId>commons-fileupload</groupId>
    <artifactId>commons-fileupload</artifactId>
    <version>1.3.3</version>
</dependency>
```



### test

```
@Test
public void testOSSServiceImport(){
    File file = new File("test.png");
    DiskFileItem fileItem = (DiskFileItem) new DiskFileItemFactory().createItem("file",
            MediaType.ALL_VALUE, true, file.getName());

    try (InputStream input = new FileInputStream(file); OutputStream os = fileItem.getOutputStream()) {
        IOUtils.copy(input, os);
    } catch (Exception e) {
        throw new IllegalArgumentException("Invalid file: " + e, e);
    }

    MultipartFile multi = new CommonsMultipartFile(fileItem);

}
```

[目录导航](https://www.cnblogs.com/yadongliang/p/13578889.html#_labelTop)

## 方式二 mockFile

pom引入spring-test

```
import org.springframework.mock.web.MockMultipartFile;

MultipartFile file = new MockMultipartFile(name,name, MediaType.MULTIPART_FORM_DATA_VALUE, inputStream);
```

 

[目录导航](https://www.cnblogs.com/yadongliang/p/13578889.html#_labelTop)

## 通过restTemplate调用远程接口,实现文件上传

 

```
    private static final String OSS_IMPORT = "/oss/import";
    private static final String OBJECT = "/oss/object";
    private static final String OBJECT_CONTENT = "/oss/object/content";

    @Autowired
    private SysConfigDao sysConfigDao;
    @Autowired
    private OssDao ossDao;
    @Autowired
    private RestTemplate restTemplate;

    private static String ossEndpoint;

    @PostConstruct
    private void init() {
        SysConfigPo sysConfigPo = sysConfigDao.findByTypeAndSubTypeAndKey(TYPE, SUB_TYPE, CONFIG_KEY);
        ossEndpoint = Objects.isNull(sysConfigPo) ? null : sysConfigPo.getValue();
    }
```

 

 

```
    private MultiValueMap<String, Object> toRequestParams(OssServiceQo ossServiceQo, MultipartFile file) {

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("file", file.getResource());

        Map<String, Object> requestParams = JSONUtils.parseObject(JSONUtils.toJSONString(ossServiceQo));
        if (!CollectionUtils.isEmpty(requestParams)) {
            Set<Map.Entry<String, Object>> entries = requestParams.entrySet();

            for (Map.Entry<String, Object> entry : entries) {
                String key = entry.getKey();
                if (org.springframework.util.StringUtils.isEmpty(entry)) {
                    continue;
                }
                parts.add(key, entry.getValue());
            }
        }
        return parts;
    }
```

 



### 入参为multipartFile

```
    @Override
    public Response<Album> save(String namespace, String prefix, String name, MultipartFile file) {
        OssServiceQo ossServiceQo = getOssServiceQo(namespace, prefix, name);
        MultiValueMap<String, Object> body = toRequestParams(ossServiceQo, file);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(body, httpHeaders);
        return restTemplate.exchange(ossEndpoint + OSS_IMPORT, HttpMethod.POST, httpEntity, new ParameterizedTypeReference<Response<Album>>() {
        }).getBody();
    }
```

 

```
    private MultiValueMap<String, Object> toRequestParams(OssServiceQo ossServiceQo, MultipartFile file) {

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("file", file.getResource());

        Map<String, Object> requestParams = JSONUtils.parseObject(JSONUtils.toJSONString(ossServiceQo));
        if (!CollectionUtils.isEmpty(requestParams)) {
            Set<Map.Entry<String, Object>> entries = requestParams.entrySet();

            for (Map.Entry<String, Object> entry : entries) {
                String key = entry.getKey();
                if (org.springframework.util.StringUtils.isEmpty(entry)) {
                    continue;
                }
                parts.add(key, entry.getValue());
            }
        }
        return parts;
    }
```

 



### 入参为inpurtStream

```
    @Override
    public Response<Album> save(String namespace, String prefix, String name, InputStream inputStream) {
        OssServiceQo ossServiceQo = getOssServiceQo(namespace, prefix, name);
        MultiValueMap<String, Object> body = toRequestParams(ossServiceQo, inputStream);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(body, httpHeaders);
        return restTemplate.exchange(ossEndpoint + OSS_IMPORT, HttpMethod.POST, httpEntity, new ParameterizedTypeReference<Response<Album>>() {
        }).getBody();
    }
```

 

```
    private MultiValueMap<String, Object> toRequestParams(OssServiceQo ossServiceQo, InputStream inputStream) {
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        Resource resource = new CommonInputStreamResource(inputStream);
        parts.add("file", resource);
        Map<String, Object> requestParams = JSONUtils.parseObject(JSONUtils.toJSONString(ossServiceQo));
        if (!CollectionUtils.isEmpty(requestParams)) {
            Set<Map.Entry<String, Object>> entries = requestParams.entrySet();

            for (Map.Entry<String, Object> entry : entries) {
                String key = entry.getKey();
                if (org.springframework.util.StringUtils.isEmpty(entry)) {
                    continue;
                }
                parts.add(key, entry.getValue());
            }
        }
        return parts;
    }
```