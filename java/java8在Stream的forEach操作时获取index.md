 

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
import java.util.Objects;
import java.util.function.BiConsumer;

/**
 * 
 * @author yangzhilong
 * @date 7/15/2019
 */
public class ForEachUtils {
    
    /**
     * 
     * @param <T>
     * @param startIndex 开始遍历的索引
     * @param elements 集合
     * @param action 
     */
    public static <T> void forEach(int startIndex,Iterable<? extends T> elements, BiConsumer<Integer, ? super T> action) {
        Objects.requireNonNull(elements);
        Objects.requireNonNull(action);
        if(startIndex < 0) {
            startIndex = 0;
        }
        int index = 0;
        for (T element : elements) {
            index++;
            if(index <= startIndex) {
                continue;
            }
            
            action.accept(index-1, element);
        }
    }
}
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

使用：

```
ForEachUtils.forEach(0, list, (index, item) -> {
            
});
```

说明：第一个参数为起始索引，第二个是要遍历的集合，第三个参数为BiConsumer类型的处理器。

单元测试：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * @author yangzhilong
 * @date 7/15/2019
 */
@Slf4j
public class ForEachUtilsTest {
    @Test
    public void test() {
        List<String> list = Arrays.asList("1","2", "3");
        ForEachUtils.forEach(0, list, (index, item) -> {
            log.info(index + " - " + item);
        });
    }
    @Test
    public void test1() {
        List<String> list = Arrays.asList("x","y", "z");
        ForEachUtils.forEach(1, list, (index, item) -> {
            log.info(index + " - " + item);
        });
    }
}
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

输出：

```
0 - 1
1 - 2
2 - 3
```

 

```
1 - y
2 - z
```