在使用springboot时发现idea启动时日志报的三个error,虽然不影响运行,但是也不想看到，强迫症，连警告都不想有的，更何况是ERROR，还飘红

![img](https://img-blog.csdnimg.cn/20200619101329154.png)

An incompatible version [1.2.12] of the Apache Tomcat Native library is installed, while Tomcat requires version [1.2.14]
incompatible：就是说不匹配，不兼容的意思

解决办法：

到tomcat的链接地址，找对应的版本

http://archive.apache.org/dist/tomcat/tomcat-connectors/native/

找到版本，提示要1.2.14

![img](https://img-blog.csdnimg.cn/20200619101428442.png)

地址：

http://archive.apache.org/dist/tomcat/tomcat-connectors/native/1.2.14/binaries/

下载

![img](https://img-blog.csdnimg.cn/2020061910155717.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2plYW5zbXkxMTE=,size_16,color_FFFFFF,t_70)

然后解压文件从里面找到"x64",找到里面的tcnative-1.dll

![img](https://img-blog.csdnimg.cn/20200619102306328.png)

将其复制到路径下:C:\Windows\System32下，如果有就覆盖，我这里原本是没有的

![img](https://img-blog.csdnimg.cn/20200619102141126.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2plYW5zbXkxMTE=,size_16,color_FFFFFF,t_70)

然后重新启动springboot项目即可

![img](https://img-blog.csdnimg.cn/20200619102351224.png)

.没有ERROR, 但是看这一条信息，还说是older