1、执行jar包的命令和在windows操作系统上是一样:
        java -jar penn-0.0.1-SNAPSHOT.jar;
            注:  关闭服务器连接时会关闭此程序进程,（推荐测试可用）

2、将jar程序设置成后台运行，并且将标准输出的日志重定向至文件msg.log;
        nohup java -jar penn-0.0.1-SNAPSHOT.jar >msg.log 2>&1 &;
            注:  nohup命令的作用就是让程序在后台运行，不用担心关闭连接进程断掉的问题了（推荐使用)

3、如果想杀掉运行中的jar程序，查看进程命令为: ps -aux | grep java
   找到此进程: root      9836  0.1  4.6 10903144 1522292 ?    Sl   Nov08   1:31 java -jar penn-0.0.1-SNAPSHOT.jar
   执行:  kill -9 9836 杀死进程;
            注:  9836 为进程标识号
            (Linux下还提供了一个killall命令，可以直接使用进程的名字而不是进程标识号，例如：#killall -9 NAME)