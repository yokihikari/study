Sentinel 是面向分布式服务架构的高可用流量防护组件，主要以流量为切入点，从限流、流量整形、熔断降级、系统负载保护、热点防护等多个维度来帮助开发者保障微服务的稳定性。Sentinel 安装部署请参考：https://www.jianshu.com/p/9626b74aec1e
 Sentinel 具有以下特性:

- 丰富的应用场景：Sentinel 承接了阿里巴巴近 10 年的双十一大促流量的核心场景，例如秒杀（即突发流量控制在系统容量可以承受的范围）、消息削峰填谷、集群流量控制、实时熔断下游不可用应用等。
- 完备的实时监控：Sentinel 同时提供实时的监控功能。您可以在控制台中看到接入应用的单台机器秒级数据，甚至 500 台以下规模的集群的汇总运行情况。
- 广泛的开源生态：Sentinel 提供开箱即用的与其它开源框架/库的整合模块，例如与 Spring Cloud、Dubbo、gRPC 的整合。您只需要引入相应的依赖并进行简单的配置即可快速地接入 Sentinel。
- 完善的 SPI 扩展点：Sentinel 提供简单易用、完善的 SPI 扩展接口。您可以通过实现扩展接口来快速地定制逻辑。例如定制规则管理、适配动态数据源等。
   1、在gitegg-platform-cloud中引入依赖



```xml
        <!-- Sentinel 高可用流量防护组件 -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
        </dependency>
```

2、在gitegg-platform-cloud的application.yml文件中加入暴露/actuator/sentinel端点的配置



```php
management:
  endpoints:
    web:
      exposure:
        include: '*'
```

3、GitEgg-Platform重新install，GitEgg-Cloud更新导入的依赖，启动gitegg-service-system服务,在浏览器中打开[http://127.0.0.1:8001/actuator/sentinel](https://links.jianshu.com/go?to=http%3A%2F%2F127.0.0.1%3A8001%2Factuator%2Fsentinel)地址，可以看到返回的Json信息，说明项目已经整合好了Sentinel。

![img](https:////upload-images.jianshu.io/upload_images/19669137-a5515782103702b9.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png





```json
{
    "blockPage": null,
    "appName": "gitegg-service-system",
    "consoleServer": [],
    "coldFactor": "3",
    "rules": {
        "systemRules": [],
        "authorityRule": [],
        "paramFlowRule": [],
        "flowRules": [],
        "degradeRules": []
    },
    "metricsFileCharset": "UTF-8",
    "filter": {
        "order": -2147483648,
        "urlPatterns": [
            "/**"
        ],
        "enabled": true
    },
    "totalMetricsFileCount": 6,
    "datasource": {},
    "clientIp": "172.16.10.3",
    "clientPort": "8719",
    "logUsePid": false,
    "metricsFileSize": 52428800,
    "logDir": "",
    "heartbeatIntervalMs": 10000
}
```

4、在配置文件中添加Sentinel服务地址，默认情况下 Sentinel 会在客户端首次调用的时候进行初始化，开始向控制台发送心跳包。也可以配置sentinel.eager=true ,取消Sentinel控制台懒加载。



```css
spring:
    cloud:
        sentinel:
        filter:
            enabled: true
        transport:
            port: 8719
            #指定sentinel控制台的地址
            dashboard: 127.0.0.1:8086
        eager: true
```

5、SystemController.java中添加限流的测试方法



```kotlin
    @ApiOperation(value = "限流测试")
    @GetMapping(value = "sentinel/protected")
    public Result<String> sentinelProtected() {
        return Result.data("访问的是限流测试接口");
    }
```

6、启动服务，通过浏览器访问刚刚新增的测试接口地址，[http://127.0.0.1:8011/system/sentinel/protected](https://links.jianshu.com/go?to=http%3A%2F%2F127.0.0.1%3A8011%2Fsystem%2Fsentinel%2Fprotected)，刷新几次，然后打开Sentinel控制台地址，可以看到当前服务的访问情况

![img](https:////upload-images.jianshu.io/upload_images/19669137-b24f069601ff01cb.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png


 7、以上是没有对接口进行限流的情况，现在我们设置规则，对接口进行限流，打开Sentinel控制台，点击左侧限流规则菜单，然后点击右上角“新增流控规则”按钮，在弹出的输入框中，资源名输入需要限流的接口，我们这里设置为：/system/sentinel/protected，阈值类型：QPS, 单机阈值：20，确定添加。

![img](https:////upload-images.jianshu.io/upload_images/19669137-da046f57f3fc5698.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png


 8、为了测试并发请求，我们这里借助压力测试工具Jmeter，具体使用方法[https://jmeter.apache.org/](https://links.jianshu.com/go?to=https%3A%2F%2Fjmeter.apache.org%2F)，下载好Jmeter之后，点击新建->测试计划->线程组->HTTP请求-查看结果树。我们限流设置的单机阈值为20，我们这里线程组先设置为20，查看请求是否会被限流，然后再将线程组设置为100查看是否被限流。

![img](https:////upload-images.jianshu.io/upload_images/19669137-d69a1f808fa4dbae.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png



![img](https:////upload-images.jianshu.io/upload_images/19669137-eaa65409a7b49c12.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png



![img](https:////upload-images.jianshu.io/upload_images/19669137-fd51724dbf3637ab.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png



![img](https:////upload-images.jianshu.io/upload_images/19669137-69a770fa77f5c91b.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png



![img](https:////upload-images.jianshu.io/upload_images/19669137-913db7a07b73675e.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png


 从以上测试结果可以看到当设置为100时，出现访问失败，返回Blocked by Sentinel (flow limiting)，说明限流已生效。
 9、Sentinel同时也支持热点参数限流和系统自适应限流，这里只需要在Sentinel控制台配置即可，所以这里不介绍具体操作及代码：
 热点参数限流：何为热点？热点即经常访问的数据。很多时候我们希望统计某个热点数据中访问频次最高的 Top K 数据，并对其访问进行限制。比如：



- 商品 ID 为参数，统计一段时间内最常购买的商品 ID 并进行限制
- 用户 ID 为参数，针对一段时间内频繁访问的用户 ID 进行限制
   热点参数限流会统计传入参数中的热点参数，并根据配置的限流阈值与模式，对包含热点参数的资源调用进行限流。热点参数限流可以看做是一种特殊的流量控制，仅对包含热点参数的资源调用生效。
   Sentinel 利用 LRU 策略统计最近最常访问的热点参数，结合令牌桶算法来进行参数级别的流控。热点参数限流支持集群模式,详细使用指南：[https://github.com/alibaba/Sentinel/wiki/%E7%83%AD%E7%82%B9%E5%8F%82%E6%95%B0%E9%99%90%E6%B5%81](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Falibaba%2FSentinel%2Fwiki%2F%E7%83%AD%E7%82%B9%E5%8F%82%E6%95%B0%E9%99%90%E6%B5%81)

系统自适应限流：Sentinel 系统自适应限流从整体维度对应用入口流量进行控制，结合应用的 Load、CPU 使用率、总体平均 RT、入口 QPS 和并发线程数等几个维度的监控指标，通过自适应的流控策略，让系统的入口流量和系统的负载达到一个平衡，让系统尽可能跑在最大吞吐量的同时保证系统整体的稳定性。,详细使用指南：[https://github.com/alibaba/Sentinel/wiki/%E7%B3%BB%E7%BB%9F%E8%87%AA%E9%80%82%E5%BA%94%E9%99%90%E6%B5%81](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Falibaba%2FSentinel%2Fwiki%2F%E7%B3%BB%E7%BB%9F%E8%87%AA%E9%80%82%E5%BA%94%E9%99%90%E6%B5%81)

