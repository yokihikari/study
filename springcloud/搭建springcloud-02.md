1、在GitEgg工程的根目录，最上级父pom.xml文件中引入需要依赖的库及Maven插件，设置编码方式：



```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.gitegg.cloud</groupId>
    <artifactId>GitEgg-Cloud</artifactId>
    <name>gitegg-cloud</name>
    <packaging>pom</packaging>
    <version>1.0-SNAPSHOT</version>

    <modules>
        <module>gitegg-common</module>
        <module>gitegg-oauth</module>
        <module>gitegg-gateway</module>
        <module>gitegg-plugin</module>
        <module>gitegg-service</module>
        <module>gitegg-service-api</module>
    </modules>

    <properties>
        <!-- jdk版本1.8 -->
        <java.version>1.8</java.version>
        <!-- maven-compiler-plugin插件版本，Java代码编译 -->
        <maven.plugin.version>3.8.1</maven.plugin.version>
        <!-- maven编译时指定编码UTF-8 -->
        <maven.compiler.encoding>UTF-8</maven.compiler.encoding>
        <!-- 项目统一字符集编码UTF-8 -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <!-- 项目统一字符集编码UTF-8 -->
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <!-- GitEgg项目统一设置版本号 -->
        <gitegg.project.version>1.0-SNAPSHOT</gitegg.project.version>
        <!-- SpringBoot版本号 -->
        <spring.boot.version>2.3.3.RELEASE</spring.boot.version>
        <!-- SpringCloud版本号 -->
        <spring.cloud.version>Hoxton.SR8</spring.cloud.version>
        <!-- Spring Cloud Alibaba -->
        <spring.cloud.alibaba>2.2.3.RELEASE</spring.cloud.alibaba>
        <!-- SpringPlatform版本号 -->
        <!--<spring.platform.version>Cairo-SR8</spring.platform.version>-->
    </properties>

    <dependencies>
        <!-- Lombok 通过简单注解消除冗长代码  -->
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <!-- Spring IO Platform主要是解决依赖版本的冲突问题，spring-boot-dependencies已引入此库，需要自定义版本时，自己选择版本引入。 -->
            <!--<dependency>
                <groupId>io.spring.platform</groupId>
                <artifactId>platform-bom</artifactId>
                <version>${spring.platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>-->
            <!-- spring boot工程的依赖库,无需继承spring-boot-starter-parent -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring.boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!-- spring cloud工程的依赖库 -->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring.cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
             <!-- Spring Cloud Alibaba -->
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring.cloud.alibaba}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <finalName>${project.name}</finalName>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
            </resource>
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.xml</include>
                </includes>
            </resource>
        </resources>
        <pluginManagement>
            <plugins>
                <!-- 能够将Spring Boot应用打包为可执行的jar或war文件，然后以通常的方式运行Spring Boot应用 -->
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                    <version>${spring.boot.version}</version>
                    <configuration>
                        <fork>true</fork>
                        <finalName>${project.build.finalName}</finalName>
                    </configuration>
                    <executions>
                        <execution>
                            <goals>
                                <goal>repackage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
                <!-- 复制jar包到指定目录,复制文件到服务器,连接服务器执行命令 -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-antrun-plugin</artifactId>
                    <executions>
                        <execution>
                            <phase>package</phase>
                            <goals>
                                <goal>run</goal>
                            </goals>
                            <configuration>
                                <tasks>
                                    <!--suppress UnresolvedMavenProperty -->
                                    <copy overwrite="true" tofile="${session.executionRootDirectory}/target/${project.artifactId}.jar" file="${project.build.directory}/${project.artifactId}.jar"/>
                                </tasks>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.plugin.version}</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <encoding>UTF-8</encoding>
                    <compilerArgs>
                        <arg>-parameters</arg>
                    </compilerArgs>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>aliyun-repos</id>
            <url>https://maven.aliyun.com/nexus/content/groups/public/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>gitegg-release</id>
            <name>Release Repository</name>
            <url>https://packages.aliyun.com/maven/repository/2020515-release-dpxo1j/</url>
        </repository>
    </repositories>

    <pluginRepositories>
        <pluginRepository>
            <id>aliyun-plugin</id>
            <url>https://maven.aliyun.com/nexus/content/groups/public/</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

</project>
```

2、修改gitegg-service的pom.xml文件，引入需要的库：



```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>GitEgg-Cloud</artifactId>
        <groupId>com.gitegg.cloud</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>gitegg-service</artifactId>
    <packaging>pom</packaging>
    <modules>
        <module>gitegg-service-base</module>
        <module>gitegg-service-bigdata</module>
        <module>gitegg-service-system</module>
    </modules>

    <dependencies>
        <!-- spring boot web核心包 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- spring boot 健康监控 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
    </dependencies>
    
</project>
```

3、在gitegg-service-system工程下新建GitEggSystemApplication主启动类：



```java
package com.gitegg.service.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * gitegg-system 启动类
 */
@SpringBootApplication
public class GitEggSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitEggSystemApplication.class,args);
    }

}
```

4、在gitegg-service-system工程的resources目录下新建application.yml文件：



```undefined
server:
  port: 8001
spring:
  application:
    name: gitegg-service-system
```

5、在GitEggSystemApplication类上右键，Run 'GitEggSystemApplicat...' ，运行项目，可以看到最基础的SpringBoot项目启动成功：



![img](https:////upload-images.jianshu.io/upload_images/19669137-10bce47e737f8457.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png