Ribbon是Netflix下的负载均衡项目，它主要实现中间层应用程序的负载均衡。为Ribbon配置服务提供者地址列表后，Ribbon就会基于某种负载均衡算法，自动帮助服务调用者去请求。Ribbon默认提供的负载均衡算法有多种，例如轮询、随即、加权轮训等，也可以为Ribbon实现自定义的负载均衡算法。Ribbon有以下特性：

- 负载均衡器，可支持插拔式的负载均衡规则
- 对多种协议提供支持，如HTTP、TCP、UDP
- 集成了负载均衡功能的客户端

  OpenFeign 中使用 Ribbon 进行负载均衡，所以 OpenFeign 直接内置了 Ribbon。在导入OpenFeign 依赖后，无需再专门导入 Ribbon 依赖。接下来，我们把gitegg-service-base作为服务的调用方，启动两个不同端口的gitegg-service-system作为服务的被调用方，测试Ribbon的负载均衡。
 1、首先在gitegg-service-system工程中，新建被调用的controller方法，返回系统配置的端口号以区分是哪个服务被调用了。



```kotlin
package com.gitegg.service.system.controller;

import com.gitegg.platform.boot.common.base.Result;
import com.gitegg.service.system.dto.SystemDTO;
import com.gitegg.service.system.service.ISystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "system")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Api(tags = "gitegg-system")
@RefreshScope
public class SystemController {

    private final ISystemService systemService;

    @Value("${spring.datasource.maxActive}")
    private String nacosMaxActiveType;

    @Value("${server.port}")
    private Integer serverPort;

    @GetMapping(value = "list")
    @ApiOperation(value = "system list接口")
    public Object list() {
        return systemService.list();
    }


    @GetMapping(value = "page")
    @ApiOperation(value = "system page接口")
    public Object page() {
        return systemService.page();
    }

    @GetMapping(value = "exception")
    @ApiOperation(value = "自定义异常及返回测试接口")
    public Result<String> exception() {
        return Result.data(systemService.exception());
    }

    @PostMapping(value = "valid")
    @ApiOperation(value = "参数校验测试接口")
    public Result<SystemDTO> valid(@Valid @RequestBody SystemDTO systemDTO) {
        return Result.data(systemDTO);
    }

    @PostMapping(value = "nacos")
    @ApiOperation(value = "Nacos读取配置文件测试接口")
    public Result<String> nacos() {
        return Result.data(nacosMaxActiveType);
    }

    @GetMapping(value = "api/by/id")
    @ApiOperation(value = "Fegin Get调用测试接口")
    public Result<Object> feginById(@RequestParam("id") String id) {
        return Result.data(systemService.list());
    }

    @PostMapping(value = "api/by/dto")
    @ApiOperation(value = "Fegin Post调用测试接口")
    public Result<Object> feginByDto(@Valid @RequestBody SystemDTO systemDTO) {
        return Result.data(systemDTO);
    }

    @GetMapping("/api/ribbon")
    @ApiOperation(value = "Ribbon调用测试接口")
    public Result<String> testRibbon() {
        return Result.data("现在访问的服务端口是:" + serverPort);
    }
}
```

2、在gitegg-service-system-api工程中，编写使用OpenFeign调用testRibbon的公共方法



```css
package com.gitegg.service.system.api.feign;

import com.gitegg.platform.boot.common.base.Result;
import com.gitegg.service.system.api.dto.ApiSystemDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "gitegg-service-system")
public interface ISystemFeign {

    /**
     * OpenFeign测试Get
     *
     * @param id
     * @return
     */
    @GetMapping("/system/api/by/id")
    Result<Object> querySystemById(@RequestParam("id") Long id);

    /**
     * OpenFeign测试Post
     *
     * @param apiSystemDTO
     * @return ApiSystemDTO
     */
    @PostMapping("/system/api/by/dto")
    Result<ApiSystemDTO> querySystemByDto(@RequestBody ApiSystemDTO apiSystemDTO);

    /**
     * OpenFeign测试Ribbon负载均衡功能
     * @return
     */
    @GetMapping("/system/api/ribbon")
    Result<String> testRibbon();

}
```

3、在gitegg-service-base中添加测试Ribbon负载均衡的Feign调用方法



```css
package com.gitegg.service.base.controller;

import com.gitegg.platform.boot.common.base.Result;
import com.gitegg.service.system.api.dto.ApiSystemDTO;
import com.gitegg.service.system.api.feign.ISystemFeign;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "base")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Api(tags = "gitegg-base")
@RefreshScope
public class BaseController {

    private final ISystemFeign systemFeign;

    @GetMapping(value = "api/by/id")
    @ApiOperation(value = "Fegin Get调用测试接口")
    public Result<Object> feginById(@RequestParam("id") Long id) {
        return Result.data(systemFeign.querySystemById(id));
    }

    @PostMapping(value = "api/by/dto")
    @ApiOperation(value = "Fegin Post调用测试接口")
    public Result<Object> feginByDto(@Valid @RequestBody ApiSystemDTO systemDTO) {
        return Result.data(systemFeign.querySystemByDto(systemDTO));
    }

    @PostMapping(value = "api/ribbon")
    @ApiOperation(value = "Ribbon调用测试接口")
    public Result<Object> testRibbon() {
        return Result.data(systemFeign.testRibbon());
    }
}
```

4、先启动gitegg-service-base服务，再启动gitegg-service-system服务，服务启动成功之后，将gitegg-service-system下bootstrap.yml里面server.port改为8011，然后再点击启动，这样就启动了两个gitegg-service-system服务（如果运行两个服务时提示：gitegg-service-system is not allowed to run in parallel. Would you like to stop the running one?，这时，在IDEA中点击Run-Edit configurations-勾选Allow parallel run即可），服务全部启动完毕之后，可以在Console窗口里面看到三个服务的Console

![img](https:////upload-images.jianshu.io/upload_images/19669137-b515ccce2b0afc97.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png


 三个服务：

![img](https:////upload-images.jianshu.io/upload_images/19669137-d3065a28cbbd83b7.png?imageMogr2/auto-orient/strip|imageView2/2/w/653/format/webp)

image.png


 5、打开浏览器访问：[http://127.0.0.1:8001/doc.html](https://links.jianshu.com/go?to=http%3A%2F%2F127.0.0.1%3A8001%2Fdoc.html)，点击Ribbon调用测试接口
 菜单，进行测试，点击请求，我们可以看到每次返回的端口都是变化的，一会儿是8001一会儿是8011，因为Ribbon负载均衡默认是使用的轮询策略

![img](https:////upload-images.jianshu.io/upload_images/19669137-021a62cb28bfb506.png?imageMogr2/auto-orient/strip|imageView2/2/w/869/format/webp)

image.png



![img](https:////upload-images.jianshu.io/upload_images/19669137-be03965c508c2767.png?imageMogr2/auto-orient/strip|imageView2/2/w/819/format/webp)

image.png


 6、如果我们需要修改负载均衡策略或者自定义负载均衡策略，根据我们的架构设计，我们在GitEgg-Platform的子工程gitegg-platform-cloud中设置公共的负载均衡策略，然后每个微服务需要不同的策略的话，可以在自己的工程中添加配置文件。接下来，在gitegg-platform-cloud中新建Ribbon配置类





```kotlin
package com.gitegg.platform.cloud.ribbon.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description Ribbon公共负载均衡策略配置
 */
@Configuration
public class RibbonConfig {

    /**
     * 负载均衡策略配置
     * @return
     */
    @Bean
    public IRule rule(){
        //随机策略  从所有可用的提供者中随机选择一个
        return new RandomRule();
    }

}
```

7、修改完成之后，GitEgg_Platform工程重新执行install，GitEgg_Cloud刷新导入的包，参照步骤5再执行测试，这时我们发现微服务返回的端口，不再是有规律的切换，而是随机不确定的出现。