Spring Cloud OpenFeign 默认是使用Ribbon实现负载均衡和重试机制的，虽然Feign有自己的重试机制，但该功能在Spring Cloud OpenFeign基本用不上，除非有特定的业务需求，则可以实现自己的Retryer，然后在全局注入或者针对特定的客户端使用特定的Retryer。
  在SpringCloud体系项目中，引入的重试机制保证了高可用的同时，也会带来一些其它的问题，如幂等操作或一些没必要的重试，下面我们实际操作来测试Spring Cloud架构中的重试机制。
 1、因为Ribbon默认是开启重试机制的，使用上一章节的代码可以测试重试机制，这里为了分辨是否执行了重试，我们把gitegg-service-cloud下面配置的Ribbon负载均衡策略改为轮询。按照上一章节方式启动三个服务，然后页面快速点击测试，发现服务端口一直有规律的切换。然后，快速关闭其中一个gitegg-service-system服务，此时继续在页面快速点击测试，我们发现接口并没有报错，而是切换到其中一个服务的端口，这说明重试机制有效。
 2、接下来，我们修改配置文件使重试机制失效，就可以看到服务关闭后因没有重试机制系统报错的结果。修改GitEgg-Platform工程下子工程gitegg-service-cloud的代码，添加Ribbon相关配置文件，因为Ribbon默认是开启重试机制的，这里选择关闭



```bash
ribbon:
  #请求连接的超时时间
  ConnectTimeout: 5000
  #请求处理/响应的超时时间
  ReadTimeout: 5000
  #对所有操作请求都进行重试
  OkToRetryOnAllOperations: false
  #切换实例的重试次数
  MaxAutoRetriesNextServer: 0
  #当前实例的重试次数
  MaxAutoRetries: 0
```

3、GitEgg-Platform重新install，GitEgg-Cloud项目重新导入依赖，然后重启三个服务，这时，快速点击测试接口的时候再关闭其中一个服务，发现接口在访问服务的时候因为没有重试机制，导致访问接口报错



![img](https:////upload-images.jianshu.io/upload_images/19669137-38cf5018d90b1942.png?imageMogr2/auto-orient/strip|imageView2/2/w/1160/format/webp)

image.png