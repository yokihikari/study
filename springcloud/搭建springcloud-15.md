Sentinel Dashboard中添加的规则是存储在内存中的，我们的微服务或者Sentinel一重启规则就丢失了，现在我们将Sentinel规则持久化配置到Nacos中，在Nacos中添加规则，然后同步到Sentinel Dashboard服务中。Sentinel 支持以下几种规则：流量控制规则、熔断降级规则、系统保护规则、来源访问控制规则 和 热点参数规则。具体可查看官网 [Sentinel 规则](https://links.jianshu.com/go?to=https%3A%2F%2Fgithub.com%2Falibaba%2FSentinel%2Fwiki%2F%E5%A6%82%E4%BD%95%E4%BD%BF%E7%94%A8%23%E6%9F%A5%E8%AF%A2%E6%9B%B4%E6%94%B9%E8%A7%84%E5%88%99)
 我们以流控规则为例进行配置，其他规则可自行配置测试。
 流量规则的定义
 重要属性：

|      Field      | 说明                                                         |           默认值            |
| :-------------: | :----------------------------------------------------------- | :-------------------------: |
|    resource     | 资源名，即规则的作用对象                                     |                             |
|      count      | 限流阈值                                                     |                             |
|      grade      | 限流阈值类型，QPS 模式（1）或并发线程数模式（0）             |          QPS 模式           |
|    limitApp     | 流控针对的调用来源                                           | default，代表不区分调用来源 |
|    strategy     | 调用关系限流策略：直接、链路、关联                           |    根据资源本身（直接）     |
| controlBehavior | 流控效果（直接拒绝/WarmUp/匀速+排队等待），不支持按调用关系限流 |          直接拒绝           |
|   clusterMode   | 是否集群限流                                                 |             否              |

1、gitegg-platform-cloud的pom.xml中引入sentinel-datasource-nacos依赖



```xml
        <!-- Sentinel 使用Nacos配置 -->
        <dependency>
            <groupId>com.alibaba.csp</groupId>
            <artifactId>sentinel-datasource-nacos</artifactId>
        </dependency>
```

2、gitegg-platform-cloud的配置文件application.yml中添加数据源配置Nacos的路径（这里面的配置，在实际应用过程中是配置在GitEgg-Cloud的Nacos配置中，会自动覆盖这些配置）



```bash
spring:
  cloud:
    sentinel:
      filter:
        enabled: true
      transport:
        port: 8719
        dashboard: 127.0.0.1:8086
      eager: true
      datasource:
        ds2:
          nacos:
            data-type: json # 默认提供两种内置的值，分别是 json 和 xml (不填默认是json)
            server-addr: 127.0.0.1:8848
            dataId: ${spring.application.name}-sentinel
            groupId: DEFAULT_GROUP
            rule-type: flow #rule-type 配置表示该数据源中的规则属于哪种类型的规则(flow流控，degrade熔断降级，authority，system系统保护, param-flow热点参数限流, gw-flow, gw-api-group)
#Ribbon配置
ribbon:
  #请求连接的超时时间
  ConnectTimeout: 5000
  #请求处理/响应的超时时间
  ReadTimeout: 5000
  #对所有操作请求都进行重试
  OkToRetryOnAllOperations: true
  #切换实例的重试次数
  MaxAutoRetriesNextServer: 1
  #当前实例的重试次数
  MaxAutoRetries: 1
#Sentinel端点配置
management:
  endpoints:
    web:
      exposure:
        include: '*'
```

3、打开Nacos控制台，新增gitegg-service-system-sentinel配置项



```json
[
    {
        "resource": "/system/sentinel/protected",
        "count": 5,
        "grade": 1,
        "limitApp": "default",
        "strategy": 0,
        "controlBehavior": 0,
        "clusterMode": false
    }
]
```

![img](https:////upload-images.jianshu.io/upload_images/19669137-f5dbea7bd04322d6.png?imageMogr2/auto-orient/strip|imageView2/2/w/1086/format/webp)

image.png



4、打开Sentinel控制台管理界面，点击流控规则菜单可以看到我们在Nacos中配置的限流信息，使用上一章节中使用的Jmater进行测试，可以看到限流生效。



![img](https:////upload-images.jianshu.io/upload_images/19669137-1ae0ed19c5f3fbae.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png