Nacos是一个更易于构建云原生应用的动态服务发现、配置管理和服务管理平台，Nacos 致力于帮助您发现、配置和管理微服务。Nacos 提供了一组简单易用的特性集，帮助您快速实现动态服务发现、服务配置、服务元数据及流量管理。Nacos部署请参考Nacos安装指南：https://www.jianshu.com/p/2e065c15d730
 1、跟之前新建SpringBoot自定义扩展一样，我们在GitEgg_Platform中新建gitegg-platform-cloud子工程，此工程主要用于Spring Cloud相关功能的自定义及扩展。
 2、在GitEgg_Platform中的gitegg-platform-bom子工程添加SpringCloud Alibaba的依赖



```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.3.RELEASE</version>
        <relativePath />
    </parent>

    <modelVersion>4.0.0</modelVersion>

    <groupId>com.gitegg.platform</groupId>
    <artifactId>gitegg-platform-bom</artifactId>
    <name>${project.artifactId}</name>
    <version>${gitegg.project.version}</version>
    <packaging>pom</packaging>

    <properties>
        <!-- jdk版本1.8 -->
        <java.version>1.8</java.version>
        <!-- maven-compiler-plugin插件版本，Java代码编译 -->
        <maven.plugin.version>3.8.1</maven.plugin.version>
        <!-- maven编译时指定编码UTF-8 -->
        <maven.compiler.encoding>UTF-8</maven.compiler.encoding>
        <!-- 项目统一字符集编码UTF-8 -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <!-- 项目统一字符集编码UTF-8 -->
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <!-- GitEgg项目统一设置版本号 -->
        <gitegg.project.version>1.0-SNAPSHOT</gitegg.project.version>

        <!-- mysql数据库驱动 -->
        <mysql.connector.version>8.0.17</mysql.connector.version>
        <!-- postgresql数据库驱动 -->
        <postgresql.connector.version>9.1-901.jdbc4</postgresql.connector.version>
        <!-- 数据库连接池Druid -->
        <druid.version>1.1.23</druid.version>
        <!-- Mybatis Plus增强工具 -->
        <mybatis.plus.version>3.4.0</mybatis.plus.version>
        <!-- Knife4j Swagger2文档 -->
        <knife4j.version>3.0.1</knife4j.version>
        <!-- Spring Cloud Alibaba -->
        <spring.cloud.alibaba>2.2.3.RELEASE</spring.cloud.alibaba>

    </properties>

    <dependencyManagement>
        <dependencies>
            <!-- gitegg数据库驱动及连接池 -->
            <dependency>
                <groupId>com.gitegg.platform</groupId>
                <artifactId>gitegg-platform-db</artifactId>
                <version>${gitegg.project.version}</version>
            </dependency>
            <!-- gitegg mybatis-plus -->
            <dependency>
                <groupId>com.gitegg.platform</groupId>
                <artifactId>gitegg-platform-mybatis</artifactId>
                <version>${gitegg.project.version}</version>
            </dependency>
            <!-- gitegg swagger2-knife4j -->
            <dependency>
                <groupId>com.gitegg.platform</groupId>
                <artifactId>gitegg-platform-swagger</artifactId>
                <version>${gitegg.project.version}</version>
            </dependency>
            <!-- gitegg boot自定义扩展 -->
            <dependency>
                <groupId>com.gitegg.platform</groupId>
                <artifactId>gitegg-platform-boot</artifactId>
                <version>${gitegg.project.version}</version>
            </dependency>
            <!-- gitegg cloud自定义扩展 -->
            <dependency>
                <groupId>com.gitegg.platform</groupId>
                <artifactId>gitegg-platform-cloud</artifactId>
                <version>${gitegg.project.version}</version>
            </dependency>
            <!-- mysql数据库驱动 -->
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>${mysql.connector.version}</version>
            </dependency>
            <!-- postgresql数据库驱动 -->
            <dependency>
                <groupId>postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version>${postgresql.connector.version}</version>
            </dependency>
            <!-- 数据库连接池 -->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid-spring-boot-starter</artifactId>
                <version>${druid.version}</version>
            </dependency>
            <!-- Mybatis Plus增强工具 -->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-boot-starter</artifactId>
                <version>${mybatis.plus.version}</version>
            </dependency>
            <!-- Swagger2 knife4j bom方式引入 -->
            <dependency>
                <groupId>com.github.xiaoymin</groupId>
                <artifactId>knife4j-dependencies</artifactId>
                <version>${knife4j.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <!-- Spring Cloud Alibaba -->
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-alibaba-dependencies</artifactId>
                <version>${spring.cloud.alibaba}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>


</project>
```

3、在gitegg-platform-cloud工程中引入spring-cloud-starter-alibaba-nacos-discovery



```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>GitEgg-Platform</artifactId>
        <groupId>com.gitegg.platform</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>gitegg-platform-cloud</artifactId>
    <name>${project.artifactId}</name>
    <version>${project.parent.version}</version>
    <packaging>jar</packaging>

    <dependencies>
        <!-- Nacos 服务注册发现-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
    </dependencies>
</project>
```

4、GitEgg_Platform工程重新执行install，在GitEgg_Cloud的子工程gitegg-service中引入gitegg-platform-cloud



```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>GitEgg-Cloud</artifactId>
        <groupId>com.gitegg.cloud</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>gitegg-service</artifactId>
    <packaging>pom</packaging>
    <modules>
        <module>gitegg-service-base</module>
        <module>gitegg-service-bigdata</module>
        <module>gitegg-service-system</module>
    </modules>

    <dependencies>
        <!-- gitegg Spring Boot自定义及扩展 -->
        <dependency>
            <groupId>com.gitegg.platform</groupId>
            <artifactId>gitegg-platform-boot</artifactId>
        </dependency>
        <!-- gitegg Spring Cloud自定义及扩展 -->
        <dependency>
            <groupId>com.gitegg.platform</groupId>
            <artifactId>gitegg-platform-cloud</artifactId>
        </dependency>
        <!-- gitegg数据库驱动及连接池 -->
        <dependency>
            <groupId>com.gitegg.platform</groupId>
            <artifactId>gitegg-platform-db</artifactId>
        </dependency>
        <!-- gitegg mybatis-plus -->
        <dependency>
            <groupId>com.gitegg.platform</groupId>
            <artifactId>gitegg-platform-mybatis</artifactId>
        </dependency>
        <!-- gitegg swagger2-knife4j -->
        <dependency>
            <groupId>com.gitegg.platform</groupId>
            <artifactId>gitegg-platform-swagger</artifactId>
        </dependency>
        <!-- spring boot web核心包 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <!-- spring boot 健康监控 -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
    </dependencies>

</project>
```

5、修改application.yml文件，添加nacos配置：



```ruby
server:
  port: 8001
spring:
  application:
    name: gitegg-service-system
  cloud:
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    url: jdbc:mysql://127.0.0.1/gitegg_cloud?zeroDateTimeBehavior=convertToNull&useUnicode=true&characterEncoding=utf8&allowMultiQueries=true
    username: root
    password: root
    initialSize: 1
    minIdle: 3
    maxActive: 20
    # 配置获取连接等待超时的时间
    maxWait: 60000
    # 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
    timeBetweenEvictionRunsMillis: 60000
    # 配置一个连接在池中最小生存的时间，单位是毫秒
    minEvictableIdleTimeMillis: 30000
    validationQuery: select 'x'
    testWhileIdle: true
    testOnBorrow: false
    testOnReturn: false
    # 打开PSCache，并且指定每个连接上PSCache的大小
    poolPreparedStatements: true
    maxPoolPreparedStatementPerConnectionSize: 20
    # 配置监控统计拦截的filters，去掉后监控界面sql无法统计，'wall'用于防火墙
    filters: config,stat,slf4j
    # 通过connectProperties属性来打开mergeSql功能；慢SQL记录
    connectionProperties: druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000;
    # 合并多个DruidDataSource的监控数据
    useGlobalDataSourceStat: true
mybatis-plus:
      mapper-locations: classpath*:/com/gitegg/*/*/mapper/*Mapper.xml
      typeAliasesPackage: com.gitegg.*.*.entity
      global-config:
        #主键类型  0:"数据库ID自增", 1:"用户输入ID",2:"全局唯一ID (数字类型唯一ID)", 3:"全局唯一ID UUID";
        id-type: 2
        #字段策略 0:"忽略判断",1:"非 NULL 判断"),2:"非空判断"
        field-strategy: 2
        #驼峰下划线转换
        db-column-underline: true
        #刷新mapper 调试神器
        refresh-mapper: true
        #数据库大写下划线转换
        #capital-mode: true
        #逻辑删除配置
        logic-delete-value: 1
        logic-not-delete-value: 0
      configuration:
        map-underscore-to-camel-case: true
        cache-enabled: false
```

6、修改GitEggSystemApplication.java添加注解@EnableDiscoveryClient,然后运行GitEggSystemApplication：



```kotlin
package com.gitegg.service.system;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * gitegg-system 启动类
 */
@EnableDiscoveryClient
@ComponentScan(basePackages = "com.gitegg")
@MapperScan("com.gitegg.*.*.mapper")
@SpringBootApplication
public class GitEggSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitEggSystemApplication.class,args);
    }

}
```

7、在浏览器中打开nacos的地址，点击左侧菜单的服务列表，可以查看到服务已经注册到nacos

![img](https:////upload-images.jianshu.io/upload_images/19669137-f5ed5dee39f1cf0e.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png