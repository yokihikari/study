1、创建父工程：File > New > Project...，选择Maven，Create from archetype不要勾选，点击Next进入下一步，填写工程信息。



![img](https:////upload-images.jianshu.io/upload_images/19669137-a7459e83e8d87d36.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png



2、填写项目名称，选择工程代码存放路径，GroupId 、ArtifactId、Version，然后点击Finish完成创建。



![img](https:////upload-images.jianshu.io/upload_images/19669137-829f7a30250a20dd.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)


 3、因为是父工程，所以删除工程下用不到的src目录。此时，pom.xml文件中没有节点<packaging>pom</packaging>，这里不需要处理，当创建子工程之后，这里会自动添加此节点。

![img](https:////upload-images.jianshu.io/upload_images/19669137-c2f75d84968f0ae5.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

4、创建第一个子工程，在GitEgg-Cloud工程上点击右键，然后选择：New > Module... > Maven，然后直接Next下一步，在Name处填入子工程名称：gitegg-common，然后点击Finish，子工程创建完成，gitegg-common主要提供系统公共方法及工具。



![img](https:////upload-images.jianshu.io/upload_images/19669137-29d63043c1ced75e.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)



5、然后使用上一步同样的操作步骤，分别创建gitegg-oauth(鉴权),gitegg-gateway(网关)，gitegg-plugin(第三方功能扩展)工程模块。使用第1步创建父工程的步骤创建gitegg-service(业务逻辑处理模块)和gitegg-service-api(业务逻辑处理统一封装对外提供服务的api接口)。创建时，在Maven窗口点击右键选择org.apache.tapestry:quickstart，在信息填写页的Parent中选择gitegg-service/gitegg-service-api建好的完整工程目录如下：



![img](https:////upload-images.jianshu.io/upload_images/19669137-fff771dc65d35691.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png



也可以将业务模块分为微服务模块,如user-server,api-server,其中

每个模块提供 api,service,dao三层