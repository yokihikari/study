平时开发过程中，经常要用到参数校验，如果直接在代码逻辑里面写参数校验，代码有点冗余且用起来不是非常方便，显得代码逻辑复杂且重复代码太多，这里我们使用注解的方式进行参数校验,SpringBoot中常用的用于参数校验的注解如下：



```tsx
@AssertFalse 所注解的元素必须是Boolean类型，且值为false
@AssertTrue 所注解的元素必须是Boolean类型，且值为true
@DecimalMax 所注解的元素必须是数字，且值小于等于给定的值
@DecimalMin 所注解的元素必须是数字，且值大于等于给定的值
@Digits 所注解的元素必须是数字，且值必须是指定的位数
@Future 所注解的元素必须是将来某个日期
@Max 所注解的元素必须是数字，且值小于等于给定的值
@Min 所注解的元素必须是数字，且值小于等于给定的值
@Range 所注解的元素需在指定范围区间内
@NotNull 所注解的元素值不能为null
@NotBlank 所注解的元素值有内容
@Null 所注解的元素值为null
@Past 所注解的元素必须是某个过去的日期
@PastOrPresent 所注解的元素必须是过去某个或现在日期
@Pattern 所注解的元素必须满足给定的正则表达式
@Size 所注解的元素必须是String、集合或数组，且长度大小需保证在给定范围之内
@Email 所注解的元素需满足Email格式
```

1、在GitEgg-Platform工程的子工程gitegg-platform-boot里添加spring-boot-starter-validation依赖，因为自SpringBoot2.3.X开始spring-boot-starter-web默认不再引入校验框架，这里需要手动引入,pom.xml如下



```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>GitEgg-Platform</artifactId>
        <groupId>com.gitegg.platform</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>gitegg-platform-boot</artifactId>
    <name>${project.artifactId}</name>
    <version>${project.parent.version}</version>
    <packaging>jar</packaging>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
        </dependency>
        <dependency>
            <groupId>com.gitegg.platform</groupId>
            <artifactId>gitegg-platform-swagger</artifactId>
            <optional>true</optional>
        </dependency>
    </dependencies>

</project>
```

2、GitEgg-Platform工程重新install，在GitEgg-Cloud工程的子工程gitegg-service-system里面新建类SystemDTO.java



```kotlin
package com.gitegg.service.system.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class SystemDTO {

    @NotNull
    @Min(value = 10, message = "id必须大于10")
    @Max(value = 150, message = "id必须小于150")
    private Long id;

    @NotNull(message = "名称不能为空")
    @Size(min = 3, max = 20, message = "名称长度必须在3-20之间")
    private String name;
}
```

3、SystemController.java类里面添加参数校验测试接口



```css
package com.gitegg.service.system.controller;

import com.gitegg.platform.boot.common.base.Result;
import com.gitegg.platform.boot.common.exception.BusinessException;
import com.gitegg.service.system.dto.SystemDTO;
import com.gitegg.service.system.service.ISystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "system")
@AllArgsConstructor
@Api(tags = "gitegg-system")
public class SystemController {

    private final ISystemService systemService;

    @GetMapping(value = "list")
    @ApiOperation(value = "system list接口")
    public Object list() {
        return systemService.list();
    }


    @GetMapping(value = "page")
    @ApiOperation(value = "system page接口")
    public Object page() {
        return systemService.page();
    }

    @GetMapping(value = "exception")
    @ApiOperation(value = "自定义异常及返回测试接口")
    public Result<String> exception() {
        return Result.data(systemService.exception());
    }

    @PostMapping(value = "valid")
    @ApiOperation(value = "参数校验测试接口")
    public Result<SystemDTO> valid(@Valid @RequestBody SystemDTO systemDTO) {
        return Result.data(systemDTO);
    }
}
```

4、运行GitEggSystemApplication.java，打开浏览器访问：[http://127.0.0.1:8001/doc.html](https://links.jianshu.com/go?to=http%3A%2F%2F127.0.0.1%3A8001%2Fdoc.html)，然后点击左侧的参数校验测试接口，使用Swagger2进行测试，即可查看校验结果

![img](https:////upload-images.jianshu.io/upload_images/19669137-8cb3cee84531c7cd.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png


 5、这里的提示信息用到了上一章节讲到的统一异常处理逻辑：





```dart
    /**
     * 非法请求-参数校验
     */
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public Result handlerMethodArgumentNotValidException(MethodArgumentNotValidException methodArgumentNotValidException) {
        //获取异常字段及对应的异常信息
        StringBuffer stringBuffer = new StringBuffer();
        methodArgumentNotValidException.getBindingResult().getFieldErrors().stream()
                .map(t -> t.getField() + t.getDefaultMessage() + ";")
                .forEach(e -> stringBuffer.append(e));
        String errorMessage = stringBuffer.toString();
        Result result = Result.error(ResultCodeEnum.PARAM_VALID_ERROR, errorSystem + errorMessage);
        return result;
    }
```

