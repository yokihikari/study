Feign是Netflix公司开源的轻量级rest客户端，使用Feign可以非常方便的实现Http 客户端。Spring Cloud引入
Feign并且集成了Ribbon实现客户端负载均衡调用(Ribbon是Netflix公司开源的一个负载均衡的项目（https://github.com/Netflix/ribbon），它是一个基于 HTTP、
TCP的客户端负载均衡器)

### Feign 工作原理如下 

### 1、 启动类添加@EnableFeignClients注解，Spring会扫描标记了@FeignClient注解的接口，并生成此接口的代理 对象 

### 2、 @FeignClient(value = Eureka的服务名称），Feign会从注册中 心获取cms服务列表，并通过负载均衡算法进行服务调用。

### 3、在接口方法 中使用注解@GetMapping("/服务的方法路径")，指定调用的url，Feign将根据url进行远程调

### Feign注意点 :

### SpringCloud对Feign进行了增强兼容了SpringMVC的注解 ，我们在使用SpringMVC的注解时需要注意： 

### 1、feignClient接口 有参数在参数必须加@PathVariable("XXX")和@RequestParam("XXX") 

### 2、feignClient返回值为复杂对象时其类型必须有无参构造函数。