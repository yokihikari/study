Sentinel除了流量控制以外，对调用链路中不稳定的资源进行熔断降级也是保障高可用的重要措施之一。由于调用关系的复杂性，如果调用链路中的某个资源不稳定，最终会导致请求发生堆积。Sentinel 熔断降级会在调用链路中某个资源出现不稳定状态时（例如调用超时或异常比例升高），对这个资源的调用进行限制，让请求快速失败，避免影响到其它的资源而导致级联错误。当资源被降级后，在接下来的降级时间窗口之内，对该资源的调用都自动熔断。

   Sentinel 提供以下几种熔断策略：

- 慢调用比例 (SLOW_REQUEST_RATIO)：选择以慢调用比例作为阈值，需要设置允许的慢调用 RT（即最大的响应时间），请求的响应时间大于该值则统计为慢调用。当单位统计时长（statIntervalMs）内请求数目大于设置的最小请求数目，并且慢调用的比例大于阈值，则接下来的熔断时长内请求会自动被熔断。经过熔断时长后熔断器会进入探测恢复状态（HALF-OPEN 状态），若接下来的一个请求响应时间小于设置的慢调用 RT 则结束熔断，若大于设置的慢调用 RT 则会再次被熔断。
- 异常比例 (ERROR_RATIO)：当单位统计时长（statIntervalMs）内请求数目大于设置的最小请求数目，并且异常的比例大于阈值，则接下来的熔断时长内请求会自动被熔断。经过熔断时长后熔断器会进入探测恢复状态（HALF-OPEN 状态），若接下来的一个请求成功完成（没有错误）则结束熔断，否则会再次被熔断。异常比率的阈值范围是 [0.0, 1.0]，代表 0% - 100%。
- 异常数 (ERROR_COUNT)：当单位统计时长内的异常数目超过阈值之后会自动进行熔断。经过熔断时长后熔断器会进入探测恢复状态（HALF-OPEN 状态），若接下来的一个请求成功完成（没有错误）则结束熔断，否则会再次被熔断。
    熔断降级规则说明:
   熔断降级规则（DegradeRule）包含下面几个重要的属性：

|       Field        | 说明                                                         |   默认值   |
| :----------------: | :----------------------------------------------------------- | :--------: |
|      resource      | 资源名，即规则的作用对象                                     |            |
|       grade        | 熔断策略，支持慢调用比例/异常比例/异常数策略                 | 慢调用比例 |
|       count        | 慢调用比例模式下为慢调用临界 RT（超出该值计为慢调用）；异常比例/异常数模式下为对应的阈值 |            |
|     timeWindow     | 熔断时长，单位为 s                                           |            |
|  minRequestAmount  | 熔断触发的最小请求数，请求数小于该值时即使异常比率超出阈值也不会熔断 |     5      |
|   statIntervalMs   | 统计时长（单位为 ms），如 60*1000 代表分钟级                 |  1000 ms   |
| slowRatioThreshold | 慢调用比例阈值，仅慢调用比例模式有效                         |            |

   接下来我们对这三种熔断策略分别进行配置测试：
 1、寿险在SystemController.java里面添加需要熔断测试的接口



```kotlin
    @ApiOperation(value = "慢调用比例熔断策略")
    @GetMapping(value = "sentinel/slow/request/ratio")
    public Result<String> sentinelRR() {
        try {
            double randomNumber;
            randomNumber = Math.random();
            if (randomNumber >= 0 && randomNumber <= 0.80) {
                Thread.sleep(300L);
            } else if (randomNumber >= 0.80 && randomNumber <= 0.80 + 0.10) {
                Thread.sleep(10L);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Result.success("慢调用比例熔断策略");
    }

    @ApiOperation(value = "异常比例熔断策略")
    @GetMapping(value = "sentinel/error/ratio")
    public Result sentinelRatio() {
        int i = 1/0;
        return Result.success("异常比例熔断策略");
    }

    @ApiOperation(value = "异常数熔断策略")
    @GetMapping(value = "sentinel/error/count")
    public Result sentinelCount() {
        int i = 1/0;
        return Result.success("异常数熔断策略");
    }
```

2、浏览器打开Sentinel管理控制台，打开降级规则菜单，新增降级规则，首先测试“慢调用比例”，根据官方介绍，最大RT是指最大允许的响应时间，我们这里设置成200ms，比例阈值设置成0.8，熔断时长为10s，最小请求数为5，意思是指：在1ms内请求数目大于5，并且慢调用的比例大于80%，则接下来的熔断时长内请求会自动被熔断，熔断时长是10秒，10秒之后会进入探测恢复状态（HALF-OPEN 状态），若接下来的一个请求响应时间小于200ms， 则结束熔断，若大于200ms 则会再次被熔断。



![img](https:////upload-images.jianshu.io/upload_images/19669137-983f6ae74cf7ad24.png?imageMogr2/auto-orient/strip|imageView2/2/w/687/format/webp)

image.png



3、打开Jmeter，点击新建->测试计划->线程组->HTTP请求-聚合报告。线程组设置为15，循环次数1000



![img](https:////upload-images.jianshu.io/upload_images/19669137-823a562a3e6c4c16.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png



![img](https:////upload-images.jianshu.io/upload_images/19669137-230eb418ee5a3d69.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png


 4、测试结果

![img](https:////upload-images.jianshu.io/upload_images/19669137-5a48eb137ea56127.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)

image.png



5、异常比例和异常数参考上面的测试方法进行测试，这里不再赘述，只是测试之前需要把GitEgg-Platform里面GitEggControllerAdvice.java统一异常处理的代码注释掉，否则测试代码抛出的异常会被捕获，达不到预想的效果。