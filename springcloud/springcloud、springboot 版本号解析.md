**1、Spring cloud的版本说明**

第一代版本: **Angle**，第二代版本: **Brixton**；

第三代版本: **Camden**，第四代版本: **Dalston**；

第五代版本: **Edgware**，第六代版本: **Finchley**；

第七代版本: **GreenWich**，第八代版本: **Hoxton**；

 

这种发布的版本是以**伦敦地铁站**作为版本命名，并按地铁站名称的首字母**A-Z**依次命名。

为什么我们的SpringCloud会以这种方式来发布版本，因为假如我们传统的 2.1.5.release这种发布的话，SpringCloud会包含很多子项目的版本就会给人造成混淆。

我们可以这么理解，使用英文版本的命名方式，就是为了统一管理在Spring Cloud下的子项目版本集合，否则会造成版本混乱。

（下图数据采集于：2020年11月20日星期五）

![img](https://img2020.cnblogs.com/blog/1416754/202011/1416754-20201120105234068-1215629789.png)

 

 

 **HoxtonSR9** **CURRENT** **GA**

Hoxton：第八代Spring Cloud版本；

SR9：修正版，修正了9次，说明release之后，进行了9次的问题修复。

CURRENT：这是官网当前推荐的版本；

GA：稳定版本，可用于生产了；

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
<dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-dependencies</artifactId>
        <version>Hoxton.SR2</version>
        <type>pom</type>
        <scope>import</scope>
</dependency>
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

**Hoxton SNAPSHOT**

Hoxton：第八代Spring Cloud版本；

SNAPSHOT：开发版本，快照版本；

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
<dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-dependencies</artifactId>
        <version>Hoxton.BUILD-SNAPSHOT </version>
        <type>pom</type>
        <scope>import</scope>
</dependency>
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

**注意：当然这里也需要添加spring.io的repositories，这里省略…**

**Spring Cloud****和Spring Boot的对应关系**

**![img](https://img2020.cnblogs.com/blog/1416754/202011/1416754-20201120111200941-1876982906.png)**

 

 目前的Hoxton对应的Spring Boot是2.2.x

**生产版本选择 – 很重要、很重要**

（1）: 打死不用 非稳定版本/ end-of-life(不维护)版本

（2）：release版本先等等(等别人去探雷)；

（3）: 推荐  SR2以后的可以放心使用；

------

 

**2、springboot 版本号**

**2.1、例如：Spring Boot的版本号是2.1.5**

（1）其中2: 表示的主版本号，表示是我们的SpringBoot第二代产品。

（2）其中1: 表示的是次版本号，增加了一些新的功能但是主体的架构是没有变化的，是兼容的。

（3）其中5: 表示的是bug修复版。

（4）所以2.1.5合起来就是springboot的第二代版本的第1个小版本的第5次bug修复版本。

***结论：版本格式 -> 主版本号.子版本号.修正版本号。\***

主版本号：当你做了不兼容的 API 修改或者进行了大调整；

子版本号：当你做了向下兼容的功能性新增；

修订号：当你做了向下兼容的问题修正；

所以，我们就可以知道，SpringBoot 1.x的版本和Spring Boot 2.x是无法直接通过修改版本号就可以升级的，但是如果要从2.1.5升级到截止到2020年3月6日的2.2.5版本的话，简单修改下版本就可以成功升级了。

**2.2、我们会发现在使用版本的时候，还有一个点后缀.RELEASE、.SR1、.M1之类的，这又是什么意思呢？**

snapshot 快照

alpha 内测

beta 公测

release 稳定版本

GA 最稳定版本

Final 正式版

Pro(professional) 专业版

Plus 加强版

Retail 零售版

DEMO 演示版

Build 内部标号

Delux 豪华版 （deluxe：豪华的，华丽的）

Corporation或Enterpraise 企业版

M1 M2 M3 M是milestone的简写 里程碑的意思

RC 版本RC:(Release Candidate)，几乎就不会加入新的功能了，而主要着重于除错

SR 修正版

Trial 试用版

Shareware 共享版

Full 完全版

 

这个名词是多的不得了，这里我们讲几个常见的。

**build-snapshot**：开发版本，也叫快照版本。当前版本处于开发中，开发完成之后，自己进行测试，另外让团队其它人也进行测试使用下; 

**M1...M2（Milestone）**：里程碑版本，在版发布之前 会出几个里程碑的版本。使用snapshot版本开发了一个时间，觉得最近写代码杠杠的，那么就整几个里程碑版本记录下吧，记录我们这个重大的时刻，是你我未来的回忆。

**RC1…RC2（Release Candidates）**：发布候选。内部开发到一定阶段了，各个模块集成后，经过细心的测试整个开发团队觉得软件已经稳定没有问题了，可以对外发行了。

release：正式版本。发布候选差不多之后，那么说明整个框架到了一定的阶段了，可投入市场大面积使用了，那么发布出去，让广大用户来吃吃香吧。

**SR1…SR2（Service Release）**：修正版。这是啥意思呐，这不release版本发布之后，让广大群体使用了嘛，再牛逼的架构师，也无法写出零bug的代码，那么这时候，就优先对于release版本的问题进行修复，这时候每次迭代的版本就是SR1,SR2,SR3。

那么上面的一个顺序是这样子的：

snapshot –>M1…MX –> RC1…RCX –> release –> SR1…SRX

对应的文字理解：

开发版本(BS) --(开发到一个小阶段，就要标记下)--> 里程碑版本(MX) --(版本到了一个相对稳定的阶段，可以对外发行了，但是可能还存在修复的问题，此时只做修复，不做新功能的增加)--> 发布候选(RC1) --(BUG修复完成，发布)-->正式版本(release)  --(外界反馈存在一些问题，进行内部在修复)--> 修正版本(SRX)

***结论：版本格式-> 主版本号\******.\******子版本号\******.\******修正版本号\******.\******软件版本阶段\***

**2.3、我们会发现在网站上可以看到会标着CURRENT、PRE等等，这个又是什么意思呢？**

**CURRENT：**当前推荐的版本

**GA：**稳定版，可用于生产

**PRE  ：**里程碑版/预览版本

**SNAPSHOT :** 快照

这里只是一个标识，方便大家在使用的时候，进行选择版本，我们一般选择是 **CURRENT** 和 **GA** 版本，**SNAPSHOT** 打死不选。

举例说明：（图片采集于20201120）

![img](https://img2020.cnblogs.com/blog/1416754/202011/1416754-20201120102206953-562501172.png)

 

 

 2.4.0 CURRENT GA

2.4.0 ：第二代Spring Boot的第2个小版本的第0次bug修改；

CURRENT ：这是官网当前推荐的使用版本；

GA：这是稳定版本，可用于生产环境。

**![img](https://img2020.cnblogs.com/blog/1416754/202011/1416754-20201120103227587-1408510022.png)**

 

 

 

引入使用：

```
 <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.4.0</version>
    </parent>
```

 

2.3.0 M2 PRE 版本（当前举例版本不存在 M2 PRE版本）

2.3.0：第二代Spring Boot的第3个小版本的第0次bug修复；

M2：这是2.3.0版本的第2次里程碑版本啦；

PRE：程碑版本/预览版本，告知我们这个是里程碑版本，先偷腥的人可以来；

```
<parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.0.M2</version>
    </parent>
```

里程碑版本是记录内部开发的一个版本，所以，如果是上面这么引入的，是无法使用的，因为还未发布到官网上，那么在使用的时候，需要添加仓库：

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```
<repositories>
        <repository>
            <id>spring-snapshots</id>
            <url>https://repo.spring.io/snapshot</url>
            <snapshots><enabled>true</enabled></snapshots>
        </repository>
        <repository>
            <id>spring-milestones</id>
            <url>https://repo.spring.io/milestone</url>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>spring-snapshots</id>
            <url>https://repo.spring.io/snapshot</url>
        </pluginRepository>
        <pluginRepository>
            <id>spring-milestones</id>
            <url>https://repo.spring.io/milestone</url>
        </pluginRepository>
    </pluginRepositories>
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

 

2.4.1-SNAPSHOT

2.4.1：第二代Spring Boot的第4个小版本的第1次bug修复；

SNAPSHOT：这是我们内部在开发的一个版本啦，也是对于2.4.0版本的bug迭代版本；

```
<parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.4.1-SNAPSHOT</version>
    </parent>
```

**注意：当然这里也需要添加spring.io的repositories，这里省略…**

 

**2.1.13** **GA （此版本是2.2.5版本中的）**

**![img](https://img2020.cnblogs.com/blog/1416754/202011/1416754-20201120104629015-2052360898.png)**

 

 

 

2.1.13：第一代Spring Boot的第1个小版本的第13次bug修复；

GA：这是稳定版本，可用于生产；

2.1.13和2.2.5版本差别就在于2.2.5多了一个CURRENT标识，说明官方更推荐2.2.5版本。如果你当前在使用2.1.x版本的话，也是稳定的版本，官网也在修复此版本存在的问题。

```
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.1.13.RELEASE</version>
</parent>
```

 

总结：

（1）Spring Boot版本格式：主版本号.子版本号.修正版本号.软件版本阶段。

（2）Spring Cloud版本格式：(英文)主版本号.软件阶段版本。

（3）Spring Boot 2.3.5 ：表示的是Spring Boot的第二代的第3次小版本的第5次bug修复；

（4）build-snapshot：开发版本/快照版本；

（5）MX：里程碑版本；

（6）release：正式版本；

（7）SRX：修正版本；

（8）版本阶段：snapshot –> M1…MX –>RC1…RCX –> release –> SR1…SRX

（9）Spring Cloud是以伦敦地铁站作为版本名称，并按地铁站名称的首字母A-Z依次命名。

（10）Spring Boot的版本升级，发现并没有SR1...SR2的标识，所以Spring Boot这样有数字为版本的是通过最后一位数字来说明在迭代修复bug；对于Spring Cloud是英文版本号，没有数字的概念，那么就是通过.SRX的方式。

 

重中之重总结：

（1）版本主要有三部分组成：版本号 +版本阶段+ 版本标识；

（2）版本号一般由3位数字构成：主版本号.次版本号.版本修正号（比如：Spring Boot）；当框架是n个子框架的集合的时候，那么为了对子项目统一管理，这时候版本号就是：英文名称（Spring Cloud）。

（3）版本阶段：为了记录版本的各个阶段，**开发阶段（snapshot） - 里程碑阶段（MX） - 发布候选（RCX）- 正式版本（release） - 修正版本（SRX）**;对于Spring Boot版本阶段就会比较简单，对于Spring Cloud的版本阶段基本是遵照上面的阶段。

（4）版本标识：为了方便开发使用者群体选择合适的版本，也说明了当前版本号处于一个什么情况了，常见的就是**SNAPSHOT**/**PRE** /**GA**/**CURRENT**。

 

如有错误，还请大佬多多指教。

参考文档：https://blog.51cto.com/11142439/2484022?source=dra