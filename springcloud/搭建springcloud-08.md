随着业务的发展、微服务架构的升级，服务的数量、程序的配置日益增多（各种微服务、各种服务器地址、各种参数），传统的配置文件方式和数据库的方式已无法满足开发人员对配置管理的要求：

- 安全性：配置跟随源代码保存在代码库中，容易造成配置泄漏。
- 时效性：修改配置，需要重启服务才能生效。
- 局限性：无法支持动态调整：例如日志开关、功能开关。
   因此，分布式配置中心应运而生！
   使用Nacos之前首先了解一下SpringBoot配置文件bootstrap与application的加载顺序：
- bootstrap.yml（bootstrap.properties）先加载
- application.yml（application.properties）后加载
- bootstrap.yml 用于应用程序上下文的引导阶段
- bootstrap.yml 由父Spring ApplicationContext加载
   Nacos的Config默认读取的是bootstrap.yml配置文件，如果将Nacos Config的配置写到application.yml里面，工程启动时就会一直报错。

1、在GitEgg-Platform工程的子工程gitegg-platform-cloud中引入spring-cloud-starter-alibaba-nacos-config依赖,重新install GitEgg-Platform工程，然后GitEgg-Cloud项目需要重新在IDEA中执行Reload All Maven Projects。



```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>GitEgg-Platform</artifactId>
        <groupId>com.gitegg.platform</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>gitegg-platform-cloud</artifactId>
    <name>${project.artifactId}</name>
    <version>${project.parent.version}</version>
    <packaging>jar</packaging>

    <dependencies>
        <!-- Nacos 服务注册发现-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        </dependency>
        <!-- Nacos 分布式配置-->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
        </dependency>
    </dependencies>

</project>
```

2、因为Nacos默认读取服务配置要写在 bootstrap.yml 中，所以我们在gitegg-service-system工程下新建 bootstrap.yml文件，同时在 bootstrap.yml 做好Nacos Config的配置



```css
server:
  port: 8001
spring:
  application:
    name: gitegg-service-system
  cloud:
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848
      config:
        server-addr: 127.0.0.1:8848
        file-extension: yml
        group: DEFAULT_GROUP
        enabled: true
```

3、在Nacos服务器上新建gitegg-service-system.yaml配置，将application.yml里面的配置信息复制到Nacos服务器上的配置信息里，然后删除application.yml，在 Nacos Spring Cloud 中，`dataId` 的完整格式如下：



```bash
${prefix}-${spring.profiles.active}.${file-extension}
```

- `prefix` 默认为 `spring.application.name` 的值，也可以通过配置项 `spring.cloud.nacos.config.prefix`来配置。

- `spring.profiles.active` 即为当前环境对应的 profile，详情可以参考 [Spring Boot文档](https://links.jianshu.com/go?to=https%3A%2F%2Fdocs.spring.io%2Fspring-boot%2Fdocs%2Fcurrent%2Freference%2Fhtml%2Fboot-features-profiles.html%23boot-features-profiles)。 **注意：当 `spring.profiles.active` 为空时，对应的连接符 `-` 也将不存在，dataId 的拼接格式变成 `${prefix}.${file-extension}`**

- ```
  file-exetension
  ```

   为配置内容的数据格式，可以通过配置项 

  ```
  spring.cloud.nacos.config.file-extension
  ```

   来配置。目前只支持 

  ```
  properties
  ```

   和 

  ```
  yaml
  ```

   类型。

  详细配置信息可以参考 

  Spring Boot文档

  ![img](https:////upload-images.jianshu.io/upload_images/19669137-7ca5b9a3d32dbaa4.png?imageMogr2/auto-orient/strip|imageView2/2/w/1086/format/webp)

  image.png



```ruby
spring:
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    url: jdbc:mysql://127.0.0.1/gitegg_cloud?zeroDateTimeBehavior=convertToNull&useUnicode=true&characterEncoding=utf8&allowMultiQueries=true
    username: root
    password: root
    initialSize: 1
    minIdle: 3
    maxActive: 20
    # 配置获取连接等待超时的时间
    maxWait: 60000
    # 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
    timeBetweenEvictionRunsMillis: 60000
    # 配置一个连接在池中最小生存的时间，单位是毫秒
    minEvictableIdleTimeMillis: 30000
    validationQuery: select 'x'
    testWhileIdle: true
    testOnBorrow: false
    testOnReturn: false
    # 打开PSCache，并且指定每个连接上PSCache的大小
    poolPreparedStatements: true
    maxPoolPreparedStatementPerConnectionSize: 20
    # 配置监控统计拦截的filters，去掉后监控界面sql无法统计，'wall'用于防火墙
    filters: config,stat,slf4j
    # 通过connectProperties属性来打开mergeSql功能；慢SQL记录
    connectionProperties: druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000;
    # 合并多个DruidDataSource的监控数据
    useGlobalDataSourceStat: true
mybatis-plus:
      mapper-locations: classpath*:/com/gitegg/*/*/mapper/*Mapper.xml
      typeAliasesPackage: com.gitegg.*.*.entity
      global-config:
        #主键类型  0:"数据库ID自增", 1:"用户输入ID",2:"全局唯一ID (数字类型唯一ID)", 3:"全局唯一ID UUID";
        id-type: 2
        #字段策略 0:"忽略判断",1:"非 NULL 判断"),2:"非空判断"
        field-strategy: 2
        #驼峰下划线转换
        db-column-underline: true
        #刷新mapper 调试神器
        refresh-mapper: true
        #数据库大写下划线转换
        #capital-mode: true
        #逻辑删除配置
        logic-delete-value: 1
        logic-not-delete-value: 0
      configuration:
        map-underscore-to-camel-case: true
        cache-enabled: false
```

4、以上就可以读取配置文件了，我们在SystemController.java里面添加读取配置的测试代码，读取配置的某一个属性，如果需要读取实时刷新数据，可以添加@RefreshScope注解



```css
package com.gitegg.service.system.controller;

import com.gitegg.platform.boot.common.base.Result;
import com.gitegg.platform.boot.common.exception.BusinessException;
import com.gitegg.service.system.dto.SystemDTO;
import com.gitegg.service.system.service.ISystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "system")
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Api(tags = "gitegg-system")
@RefreshScope
public class SystemController {

    private final ISystemService systemService;

    @Value("${spring.datasource.maxActive}")
    private String nacosMaxActiveType;

    @GetMapping(value = "list")
    @ApiOperation(value = "system list接口")
    public Object list() {
        return systemService.list();
    }


    @GetMapping(value = "page")
    @ApiOperation(value = "system page接口")
    public Object page() {
        return systemService.page();
    }

    @GetMapping(value = "exception")
    @ApiOperation(value = "自定义异常及返回测试接口")
    public Result<String> exception() {
        return Result.data(systemService.exception());
    }

    @PostMapping(value = "valid")
    @ApiOperation(value = "参数校验测试接口")
    public Result<SystemDTO> valid(@Valid @RequestBody SystemDTO systemDTO) {
        return Result.data(systemDTO);
    }

    @PostMapping(value = "nacos")
    @ApiOperation(value = "Nacos读取配置文件测试接口")
    public Result<String> nacos() {
        return Result.data(nacosMaxActiveType);
    }
}
```

5、启动项目，打开浏览器访问：[http://127.0.0.1:8001/doc.html](https://links.jianshu.com/go?to=http%3A%2F%2F127.0.0.1%3A8001%2Fdoc.html)，点击Nacos读取配置文件测试接口菜单，进行测试，可以查看读取到的配置信息，因为添加了@RefreshScope，我们测试实时刷新功能，手动修改Nacos里面的spring.datasource.maxActive配置，再次执行测试接口，可以看到读取到的配置信息已刷新

![img](https:////upload-images.jianshu.io/upload_images/19669137-1742e2e5861e9039.png?imageMogr2/auto-orient/strip|imageView2/2/w/1181/format/webp)

image.png