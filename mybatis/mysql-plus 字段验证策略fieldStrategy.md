ignored  不管有没有有设置属性，所有的字段都会设置到insert语句中，如果没设置值，全为null，这种在update 操作中会有风险，把有值的更新为null 

not_null,也是默认策略，也就是忽略null的字段，不忽略""

not-empty  为null，为空串的忽略，就是如果设置值为null，“”，不会插入数据库

 

实际业务中，如果非要把有值变为空，可以在对象属性字段上面加上：

@TableField(strategy = FieldStrategy.IGNORED)
 FieldStrategy.not_null达不到效果，有点奇怪，

![img](https://img-blog.csdnimg.cn/20190923165607747.png)



 

 

策略也会影响查询

![img](https://img-blog.csdnimg.cn/20190728114805233.png)



全局设置

![img](https://img-blog.csdnimg.cn/20190728114459745.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xpdW1pbmc2OTA0NTIwNzQ=,size_16,color_FFFFFF,t_70)



局部设置

![img](https://img-blog.csdnimg.cn/20190728114909872.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xpdW1pbmc2OTA0NTIwNzQ=,size_16,color_FFFFFF,t_70)



 

策略影响查询结果 设置字段验证策略为ignored时，表示没设置值，以null为值，这样查询条件就带上了null的查询条件，导致查询结果不是想要的，

-----------------------------------------------------------

 

![img](https://img-blog.csdnimg.cn/20190728115046121.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2xpdW1pbmc2OTA0NTIwNzQ=,size_16,color_FFFFFF,t_70)

 


