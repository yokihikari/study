## 前言

分别讲解 mybatis-plus 3.2.0 和 mybatis-plus 3.3.0的区别：

mybatis-plus 3.2.0 **不支持**全局逻辑删除的实体字段名，**支持**设置逻辑删除的值

mybatis-plus 3.3.0 **支持**全局逻辑删除的实体字段名，**支持**设置逻辑删除的值

## mybatis-plus 3.2.0

springboot 加 mybatis-plus-boot-starter 来测试

```xml
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.0.RELEASE</version>
    </parent>
   
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.2.0</version>
        </dependency>
```

**application.yaml**

```java
mybatis-plus:
  global-config:
    db-config:
      # logic-delete-field: isDelete  # 全局逻辑删除的实体字段名(since 3.3.0,配置后可以忽略不配置步骤2)
      logic-delete-value: 1 # 逻辑已删除值(默认为 1)
      logic-not-delete-value: 0 # 逻辑未删除值(默认为 0)
```

在这里 logic-delete-field 已经被我注释掉了 ，mybatis-plus 3.2.0 **不支持**全局删除的实体字段名。

如果想要实现逻辑删除，需要在**实体类字段上加上@TableLogic注解**

```java
    @TableLogic
    private Boolean isDelete;
```

## mybatis-plus 3.3.0

springboot 加 mybatis-plus-boot-starter 来测试



```xml
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.2.0.RELEASE</version>
    </parent>
   
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.3.0</version>
        </dependency>
```

**application.yaml**

```yml
mybatis-plus:
  global-config:
    db-config:
      logic-delete-field: isDelete  # 全局逻辑删除的实体字段名(since 3.3.0,配置后可以忽略不配置步骤2)
      logic-delete-value: 1 # 逻辑已删除值(默认为 1)
      logic-not-delete-value: 0 # 逻辑未删除值(默认为 0)
```

mybatis-plus 3.3.0 支持全局删除的实体字段名，实体字段名为isDelete就可以实现逻辑删除，同时**不需要**在实体类字段上加上@TableLogic注解