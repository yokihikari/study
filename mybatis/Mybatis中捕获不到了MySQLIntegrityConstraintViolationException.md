Mybatis不鼓励捕捉异常，但在某些特定场景下需要捕捉，这个时候通过try catch是捕捉不到的，mybatis有自己的处理方式，它把异常映射成了DataAccessException，那么我们需要抛出异常并捕捉。

service中抛出异常

```java
@Transactional(readOnly=false)



	public int save(BaseDepartment department) throws DataAccessException{



		return departmentDao.insert(department);



	}
```

controller中捕获异常并进行处理

```java
try {



			if (departmentService.save(department) > 0) {



				body.setReason("成功");



				body.setResultFlag(1);



			} else {



				body.setReason("失败");



				body.setResultFlag(801);



			}



		} catch (DataAccessException e) {



			body.setReason("保存失败：部门名称已存在！");



			body.setResultFlag(8012);



		}
```

 