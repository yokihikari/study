一、问题描述
使用mybatis + mybatisPlus进行修改某字段，想要将其设为null， 但执行时没有成功。

二、原因
mybatis-plus会将所有为空的字段在修改时进行过滤，不进行设为空的修改操作。

三、解决办法
在相关字段上加上注解 @TableField(strategy = FieldStrategy.IGNORED)

```
@TableField(strategy = FieldStrategy.IGNORED)
private Long userId;
```

或者

```java
updateAllColumnById(entity) // 全部字段更新
insertAllColumn(entity) // 全部字段插入
```