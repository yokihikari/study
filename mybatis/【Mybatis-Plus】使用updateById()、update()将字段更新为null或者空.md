## **问题背景：**

最近测试同学给我提了个bug，字段不能置空，我查看了下项目配置发现是字段级别被设置为NOT_EMPTY导致的。

## **mybatis-plus FieldStrategy 有三种策略：**

1.IGNORED：0 忽略

2.NOT_NULL：1 非 NULL，默认策略

3.NOT_EMPTY：2 非空

而默认更新策略是NOT_NULL：非 NULL；即通过接口更新数据时数据为NULL值时将不更新进数据库。

### **解决方案**

**1. 设置全局的field-strategy**

```
mybatis-plus:`` ``mapper-locations: classpath:mapper/*Mapper.xml`` ``global-config:``  ``# 数据库相关配置``  ``db-config:``   ``#字段策略 IGNORED:``"忽略判断"``,NOT_NULL:``"非 NULL 判断"``),NOT_EMPTY:``"非空判断"``   ``field-strategy: not_null
```

2.对所需字段设置单独的field-strategy（较为麻烦，不推荐）

```
@TableField``(strategy = FieldStrategy.NOT_NULL)``private` `String cabinetNumber;
```

3.使用UpdateWrapper方式更新

 

在mybatis-plus中，除了updateById方法，还提供了一个update方法，直接使用update方法也可以将字段设置为null，代码如下：

```
LambdaUpdateWrapper<City> updateWrapper = ``new` `LambdaUpdateWrapper<>();``updateWrapper.eq(City::getId,city.getId());``updateWrapper.set(City::getProvince,``null``);``cityMapper.update(city,updateWrapper);
```

这种方式不影响其他方法，不需要修改全局配置，也不需要在字段上单独加注解，所以推荐使用该方式。