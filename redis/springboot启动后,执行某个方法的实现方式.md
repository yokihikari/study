## 1 注解@PostConstruct

直接在方式上面加注解，但是会影响服务提供，比如这个方法要执行五分钟 这五分钟之内是无法提供服务的，这个方法是在服务初始化后之前运行， 所以 此方法运行不结束，服务就无法初始化，

- 构造函数，Autowired(Value)，PostConstruct的执行顺序为：
  ***Constructor > Autowired > PostConstruct***

```java
@PostConstruct
     public  void pingStart(){
        System.out.println(" ping start:");
        getPingip();
        System.out.println(" ping end: ");
    }
123456
```

## 2 实现ApplicationListener

通过实现接口ApplicationListener方式启动，服务已经初始化过，不影响 服务启动，并且启动之后可以正常提供服务

```java
@Component
public class ApplicationStartQuartzJobListener implements ApplicationListener<ContextRefreshedEvent>{
 
	@Autowired
    private QuartzManager quartzManager;
 
    /**
     * 初始启动quartz
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        try {
        	quartzManager.start();
            System.out.println("任务已经启动...");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
123456789101112131415161718
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200515093356521.png)

## 3 实现 CommandLineRunner

监听接口方式，启动服务，执行方式时仍然提供服务，服务初始化之后，执行方法

```java
@Component
public class StartPingService implements CommandLineRunner{
	@Autowired
	Ping ping;
	
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		ping.pingStart();
	}
12345678910
```

## 4 实现ApplicationRunner接口

```java
@Component
public class ApplicationRunnerImpl implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("通过实现ApplicationRunner接口，在spring boot项目启动后打印参数");
        String[] sourceArgs = args.getSourceArgs();
        for (String arg : sourceArgs) {
            System.out.print(arg + " ");
        }
        System.out.println();
    }
}
123456789101112
```

PS：当项目中同时实现了ApplicationRunner和CommondLineRunner接口时，可使用Order注解或实现Ordered接口来指定执行顺序，值越小越先执行