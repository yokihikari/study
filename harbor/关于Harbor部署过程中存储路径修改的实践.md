安装
Harbor安装的环境要求，具体步骤不再细说，过程很简单，详情请见官网文档：

https://github.com/goharbor/harbor/blob/master/docs/installation_guide.md

 

在默认的安装部署下，harbor的存储路径是/data/。在一般的实际部署中大概率有修改的需求，且官方文档在此并没有详细地说明。

如下图所示，根据宿主机根目录磁盘不同，默认的容量可能不满足需求：



且仅仅挂载/data/registry(镜像存储路径)并不能修改上图中存储容量的显示，而是需要迁移整个/data/路径下harbor的内容。

因此针对如何修改harbor的默认存储路径这一问题做下简要阐述。

 

如何修改存储路径
实践环境：
操作系统：CentOS 7.2

Docker：1.11 和 17.03.2

Docker-Compose：1.20.1

Harbor：1.6.1

使用离线安装包

 

修改内容：

```
1.harbor.cfg
修改"secretkey"的路径

#The path of secretkey storage
secretkey_path = /data/harbor-data # 默认是 /data


2.docker-compose.yml
修改原先所有默认为"/data"的volume的挂载路径

version: '2'
services:
  log:
    image: goharbor/harbor-log:v1.6.1
    container_name: harbor-log 
    restart: always
    volumes:
      - /var/log/harbor/:/var/log/docker/:z
      - ./common/config/log/:/etc/logrotate.d/:z
    ports:
      - 127.0.0.1:1514:10514
    networks:
      - harbor
  registry:
    image: goharbor/registry-photon:v2.6.2-v1.6.1
    container_name: registry
    restart: always
    volumes:
      - /data/harbor-data/registry:/storage:z
      - ./common/config/registry/:/etc/registry/:z
    networks:
      - harbor
    environment:
      - GODEBUG=netdns=cgo
    depends_on:
      - log
    logging:
      driver: "syslog"
      options:  
        syslog-address: "tcp://127.0.0.1:1514"
        tag: "registry"
  postgresql:
    image: goharbor/harbor-db:v1.6.1
    container_name: harbor-db
    restart: always
    volumes:
      - /data/harbor-data//database:/var/lib/postgresql/data:z
    networks:
      - harbor
    env_file:
      - ./common/config/db/env
    depends_on:
      - log
    logging:
      driver: "syslog"
      options:  
        syslog-address: "tcp://127.0.0.1:1514"
        tag: "postgresql"
  adminserver:
    image: goharbor/harbor-adminserver:v1.6.1
    container_name: harbor-adminserver
    env_file:
      - ./common/config/adminserver/env
    restart: always
    volumes:
      - /data/harbor-data/config/:/etc/adminserver/config/:z
      - /data/harbor-data/secretkey:/etc/adminserver/key:z
      - /data/harbor-data/:/data/:z
    networks:
      - harbor
    depends_on:
      - log
    logging:
      driver: "syslog"
      options:  
        syslog-address: "tcp://127.0.0.1:1514"
        tag: "adminserver"
  ui:
    image: goharbor/harbor-ui:v1.6.1
    container_name: harbor-ui
    env_file:
      - ./common/config/ui/env
    restart: always
    volumes:
      - ./common/config/ui/app.conf:/etc/ui/app.conf:z
      - ./common/config/ui/private_key.pem:/etc/ui/private_key.pem:z
      - ./common/config/ui/certificates/:/etc/ui/certificates/:z
      - /data/harbor-data/secretkey:/etc/ui/key:z
      - /data/harbor-data/ca_download/:/etc/ui/ca/:z
      - /data/harbor-data/psc/:/etc/ui/token/:z
    networks:
      - harbor
    depends_on:
      - log
      - adminserver
      - registry
    logging:
      driver: "syslog"
      options:  
        syslog-address: "tcp://127.0.0.1:1514"
        tag: "ui"
  jobservice:
    image: goharbor/harbor-jobservice:v1.6.1
    container_name: harbor-jobservice
    env_file:
      - ./common/config/jobservice/env
    restart: always
    volumes:
      - /data/harbor-data/job_logs:/var/log/jobs:z
      - ./common/config/jobservice/config.yml:/etc/jobservice/config.yml:z
    networks:
      - harbor
    depends_on:
      - redis
      - ui
      - adminserver
    logging:
      driver: "syslog"
      options:  
        syslog-address: "tcp://127.0.0.1:1514"
        tag: "jobservice"
  redis:
    image: goharbor/redis-photon:v1.6.1
    container_name: redis
    restart: always
    volumes:
      - /data/harbor-data/redis:/var/lib/redis
    networks:
      - harbor
    depends_on:
      - log
    logging:
      driver: "syslog"
      options:  
        syslog-address: "tcp://127.0.0.1:1514"
        tag: "redis"
  proxy:
    image: goharbor/nginx-photon:v1.6.1
    container_name: nginx
    restart: always
    volumes:
      - ./common/config/nginx:/etc/nginx:z
    networks:
      - harbor
    ports:
      - 80:80
      - 443:443
      - 4443:4443
    depends_on:
      - postgresql
      - registry
      - ui
      - log
    logging:
      driver: "syslog"
      options:  
        syslog-address: "tcp://127.0.0.1:1514"
        tag: "proxy"
networks:
  harbor:
    external: false
```


完成上述修改后执行下述命令重新部署容器即可：

```
./prepare
docker-compose up -d
```

注意：
在整个部署过程中，不要手动修改上述关联挂载路径下的内容。若要修改相关内容，一定要保证在容器完全移除（docker-compose down）的前提下进行。

