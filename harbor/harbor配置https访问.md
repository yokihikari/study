3.2 制作证书
切换到放置证书的目录

cd harbor/cert/

制作证书命令

openssl req \
-newkey rsa:4096 -nodes -sha256 -keyout ca.key \
-x509 -days 365 -out ca.crt \
-subj "/C=CN/ST=Guangdong/L=Shenzhen/O=WB/OU=IT/CN=192.168.255.128/emailAddress=v_wbgzlan@webank.com"

说明：C=CN(国家)，ST=Guangdong(省份)，L=Shenzhen(城市)，O=WB(公司)，OU=IT(部门)，CN=192.168.255.128(IP-关键点)，emailAddress=XXX@XXX.com(邮箱)

证书包含ca.key和ca.key两个文件

[root@localhost cert]# ls
ca.crt  ca.key



下载阿里云证书

![img](https://img-blog.csdnimg.cn/20200316154250169.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDIwODA0Mg==,size_16,color_FFFFFF,t_70)



四、安装Harbor
4.1 修改配置
cd harbor/
vim harbor.cfg

修改内容如下：

hostname = 192.168.255.128
ui_url_protocol = https
ssl_cert = harbor/cert/ca.crt
ssl_cert_key = harbor/cert/ca.key

4.2 安装
运行安装脚本

cd harbor/
./install.sh

4.3 检查安装
安装完成后docker中会跑有8个harbor相关容器



用浏览器打开https://192.168.255.128/会出现登录页面，默认的账户：admin:Harbor12345
