2、安装Harbor

（1）从这里下载：https://github.com/goharbor/harbor/releases。PS：不×××无法下载

![img](https://s1.51cto.com/images/blog/201905/03/2c0715ca9d78cd18d6c3a383e1af07d3.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

一定要下载offline离线安装包：https://storage.googleapis.com/harbor-releases/harbor-offline-installer-v1.6.1.tgz

（2）解压

tar xvf harbor-offline-installer-v1.6.1.tgz

移动解压后的harbor目录到/data/app/，因为/data分区是划分出来的

mv harbor /data/app/

（3）编辑vi harbor.cfg文件

cd /data/app/harbor

cp harbor.cfg harbor.cfg.bak

vi harbor.cfg

内容：

hostname = 192.168.1.79

ui_url_protocol = http

max_job_workers = 10

customize_crt = off

ssl_cert = /data/cert/server.crt

ssl_cert_key = /data/cert/server.key

secretkey_path = /data

admiral_url = NA

log_rotate_count = 50

log_rotate_size = 200M

http_proxy =

https_proxy =

no_proxy = 127.0.0.1,localhost,ui,registry

email_identity =

email_server = smtp.mydomain.com

email_server_port = 25

email_username = sample_admin@mydomain.com

email_password = abc

email_from = admin <sample_admin@mydomain.com>

email_ssl = false

email_insecure = false

harbor_admin_password = *******

auth_mode = ldap_auth

self_registration = off

token_expiration = 60

project_creation_restriction = everyone

db_host = postgresql

db_password = root123

db_port = 5432

db_user = postgres

redis_host = redis

redis_port = 6379

redis_password =

redis_db_index = 1,2,3

clair_db_host = postgresql

clair_db_password = root123

clair_db_port = 5432

clair_db_username = postgres

clair_db = postgres

clair_updaters_interval = 12

uaa_endpoint = uaa.mydomain.org

uaa_clientid = id

uaa_clientsecret = secret

uaa_verify_cert = true

uaa_ca_cert = /path/to/ca.pem

registry_storage_provider_name = filesystem

registry_storage_provider_config =

registry_custom_ca_bundle =

解释：

原来没注释的参数不要注释掉，否则下边一步不通过。留默认即可

hostname：配置主机名称，不可以设置127.0.0.1，localhost这样的主机名，可以是IP或者域名

ui_url_protocol：指定使用HTTP协议还是HTTPS协议

Email settings：邮箱设置，option配置，只在首次启动生效，可以登陆UI后修改

harbor_admin_password：设置管理员的初始密码，只在第一次登录时使用

auth_mode：用户认证模式，默认是db_auth,也可以使用ldap_auth验证。

db_password：使用db需要指定连接数据库的密码

self_registration：是否允许自行注册用户，默认是on,新版本可以在图形界面中修改。

max_job_workers：最大工作数，默认是10个

customize_crt：是否为token生成证书，默认为on

ssl_cert：nginx cert与key文件的路径, 只有采用https协议是才有意义

ssl_cert：nginx cert与key文件的路径, 只有采用https协议是才有意义

secretkey_path：The path of secretkey storage

admiral_url：Admiral's url, comment this attribute, or set its value to NA when Harbor is standalone

clair_db_password：未启用calir服务，但解压目录下的"./prepare"文件中要检查以下相关参数配置，不能注释，否则环境准备检查不能通过，报"ConfigParser.NoOptionError: No option u'clair_db_password' in section: u'configuration' "相关错误；或者在"./prepare"中注释相关检查与定义，但需要注意，文件中的关联太多，推荐修改"harbor.cfg"文件即可

ldap_url：ladp相关设置，如未采用ldap认证，但解压目录下的"./prepare"文件中要检查以下相关参数配置，不能注释，否则环境准备检查不能通过，报"ConfigParser.NoOptionError: No option u'ldap_timeout' in section: u'configuration' "相关错误；或者在"./prepare"中注释相关检查与定义，但需要注意，文件中的关联太多，推荐修改"harbor.cfg"文件即可

ldap_scope：

![img](https://s1.51cto.com/images/blog/201905/03/eaaada5eb7150fce79478c14a995b168.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

self_registration：默认开启自注册，off为关闭

token_expiration：token有效时间，默认30minutes

project_creation_restriction：创建项目权限控制，默认是"everyone"(所有人)，可设置为"adminonly"(管理员)

verify_remote_cert：与远程registry通信时是否采用验证ssl

其他使用默认值

（4）确认无误后，使用./install.sh命令开始安装

![img](https://s1.51cto.com/images/blog/201905/03/9b9e3fd2d756ce72d2edd77673eb2f82.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

![img](https://s1.51cto.com/images/blog/201905/03/9deed898dff38f8d8f9e04161ba4bb47.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

4、启动和停止（必须在docker-compose.yml目录下运行命令，如/data/app/Harbor目录）

停止Harbor

docker-compose stop

![img](https://s1.51cto.com/images/blog/201905/03/c32c686ec881e8ec60f07f97c0079c54.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

启动Harbor

docker-compose start