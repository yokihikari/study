## 一.harbor启动端口被占用

如何修改端口(建议不要改，修改之后好像推镜像有一些坑爹问题出现)
1.vim docker-compose.yml
修改80成1180

修改443同理

![在这里插入图片描述](https://img-blog.csdnimg.cn/2018122414593077.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2dob3N0eXVzaGVuZw==,size_16,color_FFFFFF,t_70)

2.vim common/templates/registry/config.yml

修改完配置重启
docker-compose down -v
docker-compose up -d

搭建好后访问下自己的 ip:1180 试试看(如：http://50.xxx.xxx.xxx:1180)

## 二.启动时文件夹不存在

自己手动去文件夹下创建