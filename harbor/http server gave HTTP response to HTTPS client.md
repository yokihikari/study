出现这问题的原因是：Docker自从1.3.X之后docker registry交互默认使用的是HTTPS，但是搭建私有镜像默认使用的是HTTP服务，所以与私有镜像交时出现以上错误。

这个报错是在本地上传私有镜像的时候遇到的报错：

![img](https://images2017.cnblogs.com/blog/1205699/201712/1205699-20171206005341972-493924765.png)

解决办法是：在docker server启动的时候，增加启动参数，默认使用HTTP访问：

 vim /usr/lib/systemd/system/docker.service

![img](https://images2017.cnblogs.com/blog/1205699/201712/1205699-20171206005808566-147554548.png)

在12行后面增加 --insecure-registry ip：5000

 

修改好后重启docker 服务

systemctl daemon-reload 

systemctl restart docker

![img](https://images2017.cnblogs.com/blog/1205699/201712/1205699-20171206005949269-388068348.png)

 

 

重启docker服务后，将容器重启

docker start $(docker ps -aq)

 

 

**如果上述方法还是不能解决，还可以通过以下办法解决：**

**1.vim  /etc/docker/daemon.json   增加一个daemon.json文件**

```
{ "insecure-registries":["192.168.1.100:5000"] }
保存退出

2.重启docker服务
systemctl daemon-reload
systemctl restart docker

3.重启容器
4.上传镜像
docker push 。。。。
```

 

 

 

 

**第二个问题是：Get https://192.168.2.119/v2/: dial tcp 192.168.2.119:443: getsockopt: connection refused**

![img](https://images2017.cnblogs.com/blog/1205699/201712/1205699-20171208113428421-1620386195.png)

**原因：没有指定镜像要上传的地址，站点。默认的是docker.io**

**解决方法：docker tag <imagesname> <ip:port/image>**

​         **docker push ip:port/image** 

**![img](https://images2017.cnblogs.com/blog/1205699/201712/1205699-20171208114553687-1462584987.png)**

![img](https://images2017.cnblogs.com/blog/1205699/201712/1205699-20171208115426421-1402473440.png)