 **配置镜像加速器**

Docker 使用 /etc/docker/daemon.json（Linux）配置 Daemon。

请在该配置文件中加入（没有该文件的话，请先建一个）：

{  "registry-mirrors": ["http://hub-mirror.c.163.com"] }

1、检查内核版本，必须是3.10及以上

[root@localhost ~]# uname -r

3.10.0-327.el7.x86_64

2、安装docker

yum install docker

下载过程中需要输入y确认安装

3、启动docker

[root@localhost ~]# systemctl start docker

[root@localhost ~]# docker -v     --查看版本

Docker version 1.13.1, build 94f4240/1.13.1

4、开机启动docker

[root@localhost ~]# systemctl enable docker

Created symlink from /etc/systemd/system/multiuser.target.wants/docker.service to /usr/lib/systemd/system/docker.service.

5、停止docker

systemctl stop docker

6、重启docker服务

systemctl restart  docker

二、docker的卸载

1.查看docker安装包的命令

 \- rpm -qa|grep docker

 \- yum list installed|grep docker

方式一：

[root@localhost ~]# rpm -qa|grep docker

docker-1.13.1-63.git94f4240.el7.centos.x86_64

docker-client-1.13.1-63.git94f4240.el7.centos.x86_64

docker-common-1.13.1-63.git94f4240.el7.centos.x86_64

方式二：

[root@localhost ~]# yum list installed|grep docker

docker.x86_64                        2:1.13.1-63.git94f4240.el7.centos @extras  

docker-client.x86_64                 2:1.13.1-63.git94f4240.el7.centos @extras  

docker-common.x86_64                 2:1.13.1-63.git94f4240.el7.centos @extras  

2.docker 卸载

yum –y remove deleteName

deleteName:需要删除的安装包名称

三、docker常用命令

 \1. docker search mysql   这条命令表示查询mysql的所有镜像信息

 \2. docker pull mysql  表示从官方下载默认版本的mysql，latest

​    docker pull mysql:5.5  表示下载mysql版本5.5的

 \3. docker images 查看当前本地的所有镜像

 \4. docker rmi image-id   删除制定镜像，image-id是每个镜像独有的id

 \5. docker rum ......    根据镜像启动容器

 \6. docker ps            查看运行中的容器

 \7. docker ps -a         查看所有容器

 \8. docker start 容器id   启动容器

 \9. docker stop  容器id   停止容器

\10. docker rm    容器id   删除容器

\11. service firewalld status   查看防火墙状态

\12. service firewalld stop     关闭防火墙