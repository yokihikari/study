df -h 查看硬盘使用

复制/拷贝：

cp  文件名  路径      cp  hello.csv  ./python/ml：把当前目录的hello.csv拷贝到当前目的python文件夹里的ml文件夹里

cp 源文件名  新文件名   cp  hello.txt   world.txt：复制并改名,并存放在当前目录下  

cp file1 file2 复制一个文件 
cp dir/* . 复制一个目录下的所有文件到当前工作目录 
cp -a /tmp/dir1 . 复制一个目录到当前工作目录 
cp -a dir1 dir2 复制一个目录 

剪切/移动：

mv 文件名 路径

mv hello.csv ./python：把当前目录的hello.csv剪切到当前目的python文件夹里

mv  hello.txt  ../java/   把当前目录下的文件hello.txt剪切到上一级目录的子目录java目录里

mv  hello.txt  ..     把文件hello.txt移动到上一级目录

创建文件 touch a.txt
