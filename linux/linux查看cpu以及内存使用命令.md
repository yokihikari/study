## Linux服务器如何查看CPU使用率、内存占用情况

在 Linux 上查看资源使用情况有很多命令可以参考，CPU、内存、IO、NETWORK等资源使用情况都可以通过某些命令查询到，下面小编将详细的讲解CPU使用和内存使用情况的查看方式。

作为Linux运维工程师系统维护过程中，需要我们经常查看CPU使用率、内存使用率、带宽占用，从资源使用的程度分析系统整体的运行情况。

[![Linux](https://s5.51cto.com/oss/201907/31/fd39e7d7ef9b4a02c98f1d14ffa99117.jpg-wh_651x-s_822057894.jpg)](https://s5.51cto.com/oss/201907/31/fd39e7d7ef9b4a02c98f1d14ffa99117.jpg-wh_651x-s_822057894.jpg)

在 Linux 上查看资源使用情况有很多命令可以参考，CPU、内存、IO、NETWORK等资源使用情况都可以通过某些命令查询到，下面小编将详细的讲解CPU使用和内存使用情况的查看方式。

**Top命令**

Top命令很常用，在第三行有显示CPU当前的使用情况。

![image-20210402170349568](C:\Users\18381\AppData\Roaming\Typora\typora-user-images\image-20210402170349568.png)

字段说明：

- PID：进程标示号
- USER：进程所有者
- PR：进程优先级
- NI：进程优先级别数值
- VIRT：进程占用的虚拟内存值
- RES：进程占用的物理内存值
- SHR ：进程使用的共享内存值
- S ：进程的状态，其中S表示休眠，R表示正在运行，Z表示僵死
- %CPU ：进程占用的CPU使用率
- %MEM ：进程占用的物理内存百分比
- TIME+：进程启动后占用的总的CPU时间
- Command：进程启动的启动命令名称

**Free命令**

f查看总内存、使用、空闲等情况。

[![img](https://s4.51cto.com/oss/201907/31/8d025e363100ddbe4e3a24a6862105d3.jpg-wh_600x-s_2352656602.jpg)](https://s4.51cto.com/oss/201907/31/8d025e363100ddbe4e3a24a6862105d3.jpg-wh_600x-s_2352656602.jpg)

字段说明：

- total：总计物理内存的大小
- used：已使用多大
- free：可用有多少
- Shared：多个进程共享的内存总额
- Buffers/cached：磁盘缓存的大小

**Vmstat命令**

查看CPU使用率、内存使用、IO读写情况，输入命令 vmstat：

[![image-20210402170412466](C:\Users\18381\AppData\Roaming\Typora\typora-user-images\image-20210402170412466.png)](https://s1.51cto.com/oss/201907/31/1fb080b95f636ecbbbdff3652b773f3c.jpg-wh_600x-s_631219572.jpg)

字段说明：

Procs(进程)：

- r: 运行队列中进程数量，这个值也可以判断是否需要增加CPU。(长期大于1)
- b: 等待IO的进程数量

Memory(内存)：

- swpd: 使用虚拟内存大小
- free: 空闲物理内存大小
- buff: 用作缓冲的内存大小
- cache: 用作缓存的内存大小

Swap：

- si: 每秒从交换区写到内存的大小，由磁盘调入内存
- so: 每秒写入交换区的内存大小，由内存调入磁盘

IO：

- bi: 每秒读取的块数
- bo: 每秒写入的块数

系统：

- in: 每秒中断数，包括时钟中断。
- cs: 每秒上下文切换数。

CPU(以百分比表示)：

- us: 用户进程执行时间百分比(user time)
- sy: 内核系统进程执行时间百分比(system time)
- wa: IO等待时间百分比
- id: 空闲时间百分比

对于Linux运维工程师来说，查看资源使用情况非常重要，是监控确认业务的正常运行的数据基础。与此同时，对于突发的业务崩溃运维事故，我们需要的则是能在事故发生的第一时间得到告警和通知