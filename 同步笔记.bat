@echo off
rem 切换至脚本所在目录
set work_path=%~dp0
cd %work_path%
echo swatch to work ...

rem 更新远端笔记到本地
git pull
rem 添加本地笔记到本地git库
git add  .
rem 提交修改/删除/新增记录到本地库
git commit -m 'auto'
rem 推送改动记录到远端git
git push

