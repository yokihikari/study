今天在使用IDEA启动一个开源项目的时候，报了一个错误。具体错误内容如下：

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201222163032380.png#pic_center)

Error running ‘Application’:
Command line is too long. Shorten command line for SpringBootMainApplication or also for Application

查阅了下原因，但是一般的博客都仅仅是介绍了如何解决，问题的原因和为什么这样子解决都完全没有提及，下面我就查到的资料做下简单的分析。

问题分析
错误原因：

看报错的那段英文，可知是命令行太长的原因导致SpringBoot和整个应用无法成功启动，那为什么会报这样一个错误呢？

错误分析：

其实IDEA底层是通过命令行或者文件的方式将classpath传递到Java的JVM虚拟机上的，而大多数的操作系统都会有命令行的最大长度限制，超过这个限定值时就会导致IDEA无法启动当前程序。

错误解决：

当命令行长度大于32768个字符时，将IDEA切换到动态类路径。长类路径被写入文件，然后由应用程序启动器读取并通过系统类加载器加载。

具体解决步骤
Step1：找到项目目录下的.idea\workspace.xml文件

具体位置如下图所示：

![在这里插入图片描述](https://img-blog.csdnimg.cn/2020122216304867.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTc1MTQwOQ==,size_16,color_FFFFFF,t_70#pic_center)

Step2：找到workspace.xml文件下的PropertiesComponent标签栏

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201222163107602.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTc1MTQwOQ==,size_16,color_FFFFFF,t_70#pic_center)

Step3：在PropertiesComponent标签栏内添加如下代码

<property name="dynamic.classpath" value="true" />
![在这里插入图片描述](https://img-blog.csdnimg.cn/20201222163107602.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTc1MTQwOQ==,size_16,color_FFFFFF,t_70#pic_center)


该配置的主要功能就是开启IDEA的动态类路径，将太长的类路径写入文件，然后由应用程序启动器读取并通过系统类加载器加载。

具体实现可以查看IDEA社区版源代码，JdkUtil.java文件，setupJVMCommandLine方法。
链接如下： IDEA源代码