**1、准备的工具与软件：**

一个大于4G的U盘（建议闪迪小豆子系列），软件为Notepad++、Imagewriter、Diskgenius、Chipgenius(U盘查询工具）



这里将工具包链接放出来，大家也可以去百度搜索，度盘地址为：





链接：https://pan.baidu.com/s/19a4UQpRl1uEUWNS6ddvJuQ 

提取码：r8kz 

复制这段内容后打开百度网盘手机App，操作更方便哦



# 2、清理U盘 

用DISKGENIUS打开U盘并删除所有分区（保存更改），然后点击电脑属性，硬件管理，选择U盘并重新新建分区，默认下一步，如图：



[![黑群晖安装教程8：U盘引导制作【保姆级】 基础服务 第1张](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962571273037.png)](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962571273037.png)

[![黑群晖安装教程8：U盘引导制作【保姆级】 基础服务 第2张](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962571680933.png)](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962571680933.png)

[![黑群晖安装教程8：U盘引导制作【保姆级】 基础服务 第3张](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962571444791.png)](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962571444791.png)



# 3、用写盘工具写入引导文件

点开写盘工具，并选择以IMG结尾的引导文件，磁盘选择U盘，然后点击写入，开始写盘，成功后有提示，如图：

[![黑群晖安装教程8：U盘引导制作【保姆级】 基础服务 第4张](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962691515934.png)](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962691515934.png)

[![黑群晖安装教程8：U盘引导制作【保姆级】 基础服务 第5张](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962692437389.png)](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962692437389.png)

# 4、写盘后重新编缉GRUB文件 

写盘成功后用DISKGENIU工具打开U盘，找到GRUB文件复制到桌面然后进行编缉，用U盘查询工具查询U盘的VID与PID，然后去GRUB文件进行更改数据：



[![黑群晖安装教程8：U盘引导制作【保姆级】 基础服务 第6张](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962882555900.png)](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962882555900.png)

[![黑群晖安装教程8：U盘引导制作【保姆级】 基础服务 第7张](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962883877117.png)](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962883877117.png)

[![黑群晖安装教程8：U盘引导制作【保姆级】 基础服务 第8张](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962883554624.png)](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962883554624.png)

[![黑群晖安装教程8：U盘引导制作【保姆级】 基础服务 第9张](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962883692957.png)](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962883692957.png)

# 5、将更改后的GRUB文件复制到U盘并覆盖原来的文件，制作完成：

# [![黑群晖安装教程8：U盘引导制作【保姆级】 基础服务 第10张](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962951600543.png)](https://www.huakings.cn/zb_users/upload/2019/06/201906201560962951600543.png) 