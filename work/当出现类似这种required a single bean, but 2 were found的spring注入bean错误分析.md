当出现类似这种required a single bean, but 2 were found的spring注入bean错误分析


一般在开发中为了方便都是直接使用@Autowired注解注入bean,但是这样可能会带来一个问题,就是当有一个B类继承了A类,并且两者都写上了@Service注解让spring可以扫描到.

那么在原先使用@Autowired注入A类的地方,spring就会发现有两个bean可以注入,分别是A和B,这个时候就会报required a single bean, but 2 were found的错误了

那么要怎么办呢

方案一:

如果生产环境中已经大量的使用了@Autowired,这个时候再去一个个改可能头都要大了.这个时候直接在B类,也就是子类上加上@Primary注解就可以让spring知道原先使用@Autowired注入A类(父类)的地方要使注入B类的类型了

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200221124156952.png)

方案二:

如果是刚刚开始开发,或者使用A类(父类)的地方不多.那么在原先使用@Autowired注入A类的地方下加上@Qualifier(“小写开头的B类的名字”),让spring知道,在这里扫描到了A和B两个类型的bean的时候使用B类的类型去注入

下面只是举个例子,展示@Qualifier的使用

![在这里插入图片描述](https://img-blog.csdnimg.cn/20200221124230821.png)