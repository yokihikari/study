有研究过drools6.4之前版本的同学，是不是感觉很费劲，光搭建workbench环境就够你折腾的了。drools由于缺乏对tomcat的支持，少了很多依赖jar包。部署workbench时一顿好找。现在福利来了，教你分分钟搭建workbench平台，基于drools最新版本6.5.0.Final。

        趁着docker热潮吹遍大江南北的福利，drools官方也制作了相应的docker镜像。所以首先你需要点docker常识。
    
        步骤如下：
    
        1，准备服务器环境。本人是在电脑上用virtualbox安装了一个centos7的虚拟机。
    
        2，安装docker，由于centos7自带了docker。只需要一条简单的命令即可安装docker。
    
              安装docker：yum install docker
              启动docker：service docker start
    
        3，下载并启动workbench镜像。
    
              docker run -p 8080:8080 -p 8001:8001 -d --name drools-workbench jboss/drools-workbench-showcase:latest
    
        4，查看启动workbench情况,先找出容器container_id。
    
              docker ps -a  //会打印container_id
              docker logs container_id       
    
        5，浏览器输入如下url，即可访问workbench，如下图所示。用户名/密码：admin/admin
    
               http://服务器ip:8080/drools-wb
## 新的改变

启动的访问地址有调整，很多文章里面的后缀是 drools-wb ---->>> business-central:

```
   http:localhost:8080/business-central
```

#### 4. Java获取规则文件

规则文件既然已经发布了，那么接下来就是Java出场的时候了。

1. 创建一个实体Dog，这里注意的是包名、类名还有属性都要与上方在Workbench创建的保持一致。

2. 创建DroolsApplicationWorkBenchTests测试类

3. ```
   @RunWith(SpringRunner.class)
   public class DroolsApplicationWorkBenchTests {
    @Test
    public void testWorkbench() {
    String url = "http://ip:8080/drools-wb/maven2/cn/org/zhixiang/drools-test/0.0.1/drools-test-0.0.1.jar";
    KieServices kieServices = KieServices.Factory.get();
    KieRepository kieRepository = kieServices.getRepository();
    UrlResource resource = (UrlResource) kieServices.getResources().newUrlResource(url);
    resource.setBasicAuthentication("enabled");
    resource.setPassword("admin");
    resource.setUsername("admin");
    InputStream is = null;
    try {
    is = resource.getInputStream();
    } catch (IOException e) {
    e.printStackTrace();
    }
    KieModule kieModule = kieRepository.addKieModule(kieServices.getResources().newInputStreamResource(is));
    KieContainer kieContainer = kieServices.newKieContainer(kieModule.getReleaseId());
    KieSession kieSession = kieContainer.newKieSession();
    Dog dog = new Dog();
    dog.setName("大黄");
    kieSession.insert(dog);
    kieSession.fireAllRules();
    }
   }
   ```

   

执行完测试方法以后你的控制台打印出了大黄出场四个字么？

本文所有源码：[https://github.com/shiyujun/drools](https://www.oschina.net/action/GoToLink?url=https%3A%2F%2Fgithub.com%2Fshiyujun%2Fdrools)