查询数据库中的存储过程

方法一：

​    select `name` from mysql.proc where db = 'your_db_name' and `type` = 'PROCEDURE'

方法二：

​     show procedure status;

查看存储过程或函数的创建代码

show create procedure proc_name;

show create function func_name;