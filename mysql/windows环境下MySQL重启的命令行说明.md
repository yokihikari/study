windows+R 弹出运行框  在运行框中输入cmd + 回车 进入系统的dos窗口

.启动mysql：输入 net start mysql;

.停止mysql：输入 net stop mysql;

windows下不能直接重启(restart)，只能先停止，再启动。

这个只是说本地装了mysql的情况下。或者远程连接到mysql服务器，然后在远程机器上操作此命令。

可以编辑my.cnf来修改（windows下my.ini）,在[mysqld]段或者mysql的server配置段进行修改。

max_allowed_packet = 20M

如果找不到my.cnf可以通过

mysql --help | grep my.cnf

去寻找my.cnf文件。

mysql：insert插入数据过慢如何解决，设置innodb_flush_log_at_trx_commit为0就能解决