mysql5.7以上版本在常会报关于only_full_group_by的错误，可以在sql_mode中关闭他，网上查找的解决办法通过实践后发现有些不详细，关键地方说的不清楚，有的有些错误，自己解决之后在这里总结一下。

操作系统：Linux

mysql版本：5.7.18-1

查看

进入mysql 查看mysql版本：select version();

运行SELECT @@GLOBAL.sql_mode;和SELECT @@SESSION.sql_mode;查看sql_model参数，可以看到参数中有ONLY_FULL_GROUP_BY，

ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION

2

临时去除ONLY_FULL_GROUP_BY

因为这种方式从参考资料上来看只是临时去除，所以，我并没有尝试，这里列出解决办法：

set @@GLOBAL.sql_mode='';

set sql_mode ='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';

1

2

修改配置文件去除ONLY_FULL_GROUP_BY

这种方式是我实践的方式，我详细说一下：

打开配置文件mysql.cnf

sudo gedit /etc/mysql/mysql.cnf

1

在[mysqld]中添加代码

sql_mode ='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'

1

网上有很多资料写到这段代码在[mysql]中也同时添加，另外有些写着添加内容为 “set sql_mode XXXX”经过我在自己机器上验证，发现都是不行的，只能在[mysqld]添加，否则会造成mysql无法连接

验证是否生效

重启mysql

sudo service mysql restart

1

查看参数是否存在

mysql> SELECT @@sql_mode;

+------------------------------------------------------------------------------------------------------------------------+

| @@sql_mode                                                                                                             |

+------------------------------------------------------------------------------------------------------------------------+

| STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION |

+------------------------------------------------------------------------------------------------------------------------+

1 row in set (0.00 sec)

mysql> SELECT @@GLOBAL.sql_mode;

+------------------------------------------------------------------------------------------------------------------------+

| @@GLOBAL.sql_mode                                                                                                      |

+------------------------------------------------------------------------------------------------------------------------+

| STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION |

+------------------------------------------------------------------------------------------------------------------------+

1 row in set (0.00 sec)