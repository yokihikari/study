声明：使用JDK9、MYSQL8、idea 

**1.报错信息是这样的;**

![img](https://img-blog.csdn.net/20180907174454789?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMzgwMg==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

**处理：**提示信息表明数据库驱动com.mysql.jdbc.Driver'已经被弃用了、应当使用新的驱动com.mysql.cj.jdbc.Driver'

所以，按照提示更改jdbc.properties配置 .com.mysql.jdbc.Driver **改为** com.mysql.cj.jdbc.Driver

![img](https://img-blog.csdn.net/20180907174821346?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMzgwMg==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

运行结果如下：PS 已经没有这一条报错或者警示了，一条一条处理报错。

![img](https://img-blog.csdn.net/20180907174912133?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMzgwMg==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

**2.仍存在报错信息**

Fri Sep 07 17:48:01 GMT+08:00 2018 WARN: Establishing SSL connection without server's identity verification 

![img](https://img-blog.csdn.net/20180907175254478?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMzgwMg==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

**处理：**根据报错我们知道这是时间报错，没有指定明确的时区，是因为新版的mysql会询问是否SSL连接，返回一个Boolean值，我们需要手动指定true或者false。所以再次更改配置文件中的 url 满足其要求即可，如下：

```html
 "jdbc:mysql://localhost:3306/mybatis?characterEncoding=utf-8&amp;serverTimezone=UTC&amp;useSSL=false"
```

​    **2.1、**这里是配置了jdbc.properties，完整配置如下：

```java
jdbc.DriverClassName=com.mysql.cj.jdbc.Driver



jdbc.url =jdbc:mysql://localhost:3306/student?serverTimezone=UTC&useSSL=false



jdbc.username=root



jdbc.password=root
```

   **2.2、**mybatis-config.xml 中引入配置文件、配置数据源如下：

```html
<environments default="development">



<environment id="development">



		<!-- 使用jdbc事务管理，事务控制由mybatis-->



			<transactionManager type="JDBC" />



		<!-- 数据库连接池，由mybatis管理-->



			<dataSource type="POOLED">



				<property name="driver" value="${jdbc.DriverClassName}" />



				<property name="url" value="${jdbc.url}" />



				<property name="username" value="${jdbc.username}" />



				<property name="password" value="${jdbc.password}" />



			</dataSource>



</environment>



 



</environments>
```

运行结果如下：

![img](https://img-blog.csdn.net/20180907181837594?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMzgwMg==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

**3、警告处理**

WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access by org.apache.ibatis.reflection.Reflector (file:/G:/Develop/20180907am/lib/mybatis-3.4.6.jar) to method java.lang.Class.checkPackageAccess(java.lang.SecurityManager,java.lang.ClassLoader,boolean)
WARNING: Please consider reporting this to the maintainers of org.apache.ibatis.reflection.Reflector
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release

```
译文如下：



 



警告:发生了非法的反射访问操作



 



警告:通过org.apache. ibatis.com .reflection非法访问。方法java.lang.Class.checkPackageAccess(java.lang.SecurityManager,java.lang.ClassLoader,boolean)



 



警告:请考虑向org.apache. ibatistics . reflection.reflector的维护者报告此情况



 



警告:使用——非法访问=warn启用进一步的非法反射访问操作的警告



 



警告:所有非法访问操作将在未来版本中被拒绝
```

![img](https://img-blog.csdn.net/20180907182654931?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80MjMyMzgwMg==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

**原因：**JDK9的问题，非法反射，用回JDK8就可以了。JDK9版本作为小版本，相对JDK8,以及新出的JDK11这些长期版本来讲、有些许BUG正常，针对这个问题，我看了官方社区，有给出答案说是在未来可能对该问题进行优化处理，并不影响使用；

PS：在框架以后，建议小伙伴都使用JDK8版本开发学习，毕竟好多产品最近更新都已支持到了JDK8、并且、使用重量级的WEB服务器 weblogic 也仅仅支持到JDK8；