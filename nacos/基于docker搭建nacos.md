## 操作步骤

> 这里说的也是**单机**版部署教程

```powershell
docker start nacos
```

用起来非常简单, 每次需要用到执行上面的命令即可.

> 打开nacos控制台 http://ip:8848/nacos



## docker run部署

- 获取镜像

```powershell
docker pull nacos/nacos-server
```

- 初始化nacos数据库

  > 初始化[nacos数据sql](https://github.com/alibaba/nacos/blob/master/config/src/main/resources/META-INF/nacos-db.sql)

- 运行镜像

```powershell
docker run --env MODE=standalone --name nacos -d -p 8848:8848 nacos/nacos-server
docker exec -it <CONTAINER ID> bash
vim conf/application.properties
// 修改对应mysql参数
docker restart nacos/nacos-server                                               
docker restart <CONTAINER ID>                                                             
```

对应参数:

| name                          | description                     | option                                 |
| ----------------------------- | ------------------------------- | -------------------------------------- |
| MODE                          | cluster模式/standalone模式      | cluster/standalone default **cluster** |
| NACOS_SERVERS                 | nacos cluster地址               | eg. ip1,ip2,ip3                        |
| PREFER_HOST_MODE              | 是否支持hostname                | hostname/ip default **ip**             |
| NACOS_SERVER_PORT             | nacos服务器端口                 | default **8848**                       |
| NACOS_SERVER_IP               | 多网卡下的自定义nacos服务器IP   |                                        |
| SPRING_DATASOURCE_PLATFORM    | standalone 支持 mysql           | mysql / empty default empty            |
| MYSQL_MASTER_SERVICE_HOST     | mysql 主节点host                |                                        |
| MYSQL_MASTER_SERVICE_PORT     | mysql 主节点端口                | default : **3306**                     |
| MYSQL_MASTER_SERVICE_DB_NAME  | mysql 主节点数据库              |                                        |
| MYSQL_MASTER_SERVICE_USER     | 数据库用户名                    |                                        |
| MYSQL_MASTER_SERVICE_PASSWORD | 数据库密码                      |                                        |
| MYSQL_SLAVE_SERVICE_HOST      | mysql从节点host                 |                                        |
| MYSQL_SLAVE_SERVICE_PORT      | mysql从节点端口                 | default :3306                          |
| MYSQL_DATABASE_NUM            | 数据库数量                      | default :2                             |
| JVM_XMS                       | -Xms                            | default :2g                            |
| JVM_XMX                       | -Xmx                            | default :2g                            |
| JVM_XMN                       | -Xmn                            | default :1g                            |
| JVM_MS                        | -XX:MetaspaceSize               | default :128m                          |
| JVM_MMS                       | -XX:MaxMetaspaceSize            | default :320m                          |
| NACOS_DEBUG                   | 开启远程调试                    | y/n default :n                         |
| TOMCAT_ACCESSLOG_ENABLED      | server.tomcat.accesslog.enabled | default :false                         |

![img](https://s2.ax1x.com/2020/03/10/8FsnM9.gif)



## docker-compose up部署

- Clone 项目

  ```powershell
  git clone https://github.com/nacos-group/nacos-docker.git
  cd nacos-docker
  ```

- 修改mysql配置

```powershell
## \nacos-docker\env\nacos-standlone-mysql.env
PREFER_HOST_MODE=hostname
MODE=standalone
SPRING_DATASOURCE_PLATFORM=mysql
MYSQL_SERVICE_HOST=192.168.31.219
MYSQL_SERVICE_DB_NAME=barm_nacos
MYSQL_SERVICE_PORT=3306
MYSQL_SERVICE_USER=root
MYSQL_SERVICE_PASSWORD=123456
```

- 初始化nacos数据库

  > 初始化[nacos数据sql](https://github.com/alibaba/nacos/blob/master/config/src/main/resources/META-INF/nacos-db.sql)

- 修改standalone-mysql.yaml

注释掉mysql,prometheus,grafana(健康相关的暂时用不上)

```yaml
version: "2"
services:
  nacos:
    image: nacos/nacos-server:latest
    container_name: nacos-standalone-mysql
    env_file:
      - ../env/nacos-standlone-mysql.env
    volumes:
      - ./standalone-logs/:/home/nacos/logs
      - ./init.d/custom.properties:/home/nacos/init.d/custom.properties
    ports:
      - "8848:8848"
      - "9555:9555"
    # depends_on:
    #   - mysql
    restart: on-failure
  # mysql:
  #   container_name: mysql
  #   image: mysql:5.7.29
  #   env_file:
  #     - ../env/mysql.env
  #   volumes:
  #     - ./mysql:/var/lib/mysql
  #   ports:
  #     - "3306:3306"
  # prometheus:
  #   container_name: prometheus
  #   image: prom/prometheus:latest
  #   volumes:
  #     - ./prometheus/prometheus-standalone.yaml:/etc/prometheus/prometheus.yml
  #   ports:
  #     - "9090:9090"
  #   depends_on:
  #     - nacos
  #   restart: on-failure
  # grafana:
  #   container_name: grafana
  #   image: grafana/grafana:latest
  #   ports:
  #     - 3000:3000
  #   restart: on-failure
```

- 单机模式 Mysql

  ```powershell
  docker-compose -f example/standalone-mysql.yaml up
  ```

- 服务注册

  ```powershell
  curl -X POST 'http://127.0.0.1:8848/nacos/v1/ns/instance?serviceName=nacos.naming.serviceName&ip=20.18.7.10&port=8080'
  ```

- 服务发现

  ```powershell
  curl -X GET 'http://127.0.0.1:8848/nacos/v1/ns/instances?serviceName=nacos.naming.serviceName'
  ```

- 发布配置

  ```powershell
  curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos.cfg.dataId&group=test&content=helloWorld"
  ```

- 获取配置

  ```powershell
    curl -X GET "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos.cfg.dataId&group=test"
  ```



## Common property configuration

| name                          | description                     | option                                 |
| ----------------------------- | ------------------------------- | -------------------------------------- |
| MODE                          | cluster模式/standalone模式      | cluster/standalone default **cluster** |
| NACOS_SERVERS                 | nacos cluster地址               | eg. ip1,ip2,ip3                        |
| PREFER_HOST_MODE              | 是否支持hostname                | hostname/ip default **ip**             |
| NACOS_SERVER_PORT             | nacos服务器端口                 | default **8848**                       |
| NACOS_SERVER_IP               | 多网卡下的自定义nacos服务器IP   |                                        |
| SPRING_DATASOURCE_PLATFORM    | standalone 支持 mysql           | mysql / empty default empty            |
| MYSQL_MASTER_SERVICE_HOST     | mysql 主节点host                |                                        |
| MYSQL_MASTER_SERVICE_PORT     | mysql 主节点端口                | default : **3306**                     |
| MYSQL_MASTER_SERVICE_DB_NAME  | mysql 主节点数据库              |                                        |
| MYSQL_MASTER_SERVICE_USER     | 数据库用户名                    |                                        |
| MYSQL_MASTER_SERVICE_PASSWORD | 数据库密码                      |                                        |
| MYSQL_SLAVE_SERVICE_HOST      | mysql从节点host                 |                                        |
| MYSQL_SLAVE_SERVICE_PORT      | mysql从节点端口                 | default :3306                          |
| MYSQL_DATABASE_NUM            | 数据库数量                      | default :2                             |
| JVM_XMS                       | -Xms                            | default :2g                            |
| JVM_XMX                       | -Xmx                            | default :2g                            |
| JVM_XMN                       | -Xmn                            | default :1g                            |
| JVM_MS                        | -XX:MetaspaceSize               | default :128m                          |
| JVM_MMS                       | -XX:MaxMetaspaceSize            | default :320m                          |
| NACOS_DEBUG                   | 开启远程调试                    | y/n default :n                         |
| TOMCAT_ACCESSLOG_ENABLED      | server.tomcat.accesslog.enabled | default :false                         |



## Nacos + Grafana + Prometheus



### 有兴趣可以看看这个:

参考：[Nacos监控指南](https://nacos.io/zh-cn/docs/monitor-guide.html)

**Note**: grafana创建一个新数据源时，数据源地址必须是 **[http://prometheus:9090](http://prometheus:9090/)**



## 上传阿里云docker私有镜像

登录阿里云创建自己对应的仓库

![img](https://s2.ax1x.com/2020/03/10/8F2AgS.gif)

```powershell
docker login --username=username registry.cn-aliyuncs.com
docker tag [ImageId] registry.cn-hangzhou.aliyuncs.com/username/barm-nacos:[镜像版本号]
docker push registry.cn-hangzhou.aliyuncs.com/allenalan/barm-nacos:[镜像版本号]
```

好了就这些, 欢迎关注,评论,点赞, 转发~