### 1 拉取nacos镜像并启动

```shell
docker pull nacos/nacos-server
1
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191231110715178.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM0ODA3NDI5,size_16,color_FFFFFF,t_70)

### 2 启动nacos命令

```shell
docker run -d --name nacos -p 8848:8848 -e PREFER_HOST_MODE=hostname -e MODE=standalone nacos/nacos-server
1
```

至此，我们已经可以使用nacos服务，UI地址:http://:8848/nacos 账号:nacos 密码:nacos

![在这里插入图片描述](https://img-blog.csdnimg.cn/20191231110028171.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM0ODA3NDI5,size_16,color_FFFFFF,t_70)
​上述方式是最简便的方式启动，但这样的话有一点小瑕疵，nacos所有元数据都会保存在容器内部。倘若容器迁移则nacos源数据则不复存在，所以通常我们通常会将nacos元数据保存在mysql中。下面附上配置方式：

### 3 修改配置文件

```shell
#1 查看docker容器，nacos启动成功
docker ps 
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              PORTS                    NAMES
8149bca96437        nacos/nacos-server   "bin/docker-startup.…"   4 minutes ago       Up About a minute   0.0.0.0:8848->8848/tcp   nacos
#2 进入容器
docker exec -it 8149bca96437 /bin/bash
#3 修改 conf/application.properties 内容如下：
vi conf/application.properties
12345678
```

#### 数据库脚本

nacos脚本

| /*   |                                                              |
| ---- | ------------------------------------------------------------ |
|      | * Copyright 1999-2018 Alibaba Group Holding Ltd.             |
|      | *                                                            |
|      | * Licensed under the Apache License, Version 2.0 (the "License"); |
|      | * you may not use this file except in compliance with the License. |
|      | * You may obtain a copy of the License at                    |
|      | *                                                            |
|      | *      http://www.apache.org/licenses/LICENSE-2.0            |
|      | *                                                            |
|      | * Unless required by applicable law or agreed to in writing, software |
|      | * distributed under the License is distributed on an "AS IS" BASIS, |
|      | * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
|      | * See the License for the specific language governing permissions and |
|      | * limitations under the License.                             |
|      | */                                                           |
|      |                                                              |
|      | /******************************************/                 |
|      | /*   数据库全名 = nacos_config   */                          |
|      | /*   表名称 = config_info   */                               |
|      | /******************************************/                 |
|      | CREATE TABLE `config_info` (                                 |
|      | `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',        |
|      | `data_id` varchar(255) NOT NULL COMMENT 'data_id',           |
|      | `group_id` varchar(255) DEFAULT NULL,                        |
|      | `content` longtext NOT NULL COMMENT 'content',               |
|      | `md5` varchar(32) DEFAULT NULL COMMENT 'md5',                |
|      | `gmt_create` datetime NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间', |
|      | `gmt_modified` datetime NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间', |
|      | `src_user` text COMMENT 'source user',                       |
|      | `src_ip` varchar(20) DEFAULT NULL COMMENT 'source ip',       |
|      | `app_name` varchar(128) DEFAULT NULL,                        |
|      | `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',      |
|      | `c_desc` varchar(256) DEFAULT NULL,                          |
|      | `c_use` varchar(64) DEFAULT NULL,                            |
|      | `effect` varchar(64) DEFAULT NULL,                           |
|      | `type` varchar(64) DEFAULT NULL,                             |
|      | `c_schema` text,                                             |
|      | PRIMARY KEY (`id`),                                          |
|      | UNIQUE KEY `uk_configinfo_datagrouptenant` (`data_id`,`group_id`,`tenant_id`) |
|      | ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info'; |
|      |                                                              |
|      | /******************************************/                 |
|      | /*   数据库全名 = nacos_config   */                          |
|      | /*   表名称 = config_info_aggr   */                          |
|      | /******************************************/                 |
|      | CREATE TABLE `config_info_aggr` (                            |
|      | `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',        |
|      | `data_id` varchar(255) NOT NULL COMMENT 'data_id',           |
|      | `group_id` varchar(255) NOT NULL COMMENT 'group_id',         |
|      | `datum_id` varchar(255) NOT NULL COMMENT 'datum_id',         |
|      | `content` longtext NOT NULL COMMENT '内容',                  |
|      | `gmt_modified` datetime NOT NULL COMMENT '修改时间',         |
|      | `app_name` varchar(128) DEFAULT NULL,                        |
|      | `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',      |
|      | PRIMARY KEY (`id`),                                          |
|      | UNIQUE KEY `uk_configinfoaggr_datagrouptenantdatum` (`data_id`,`group_id`,`tenant_id`,`datum_id`) |
|      | ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='增加租户字段'; |
|      |                                                              |
|      |                                                              |
|      | /******************************************/                 |
|      | /*   数据库全名 = nacos_config   */                          |
|      | /*   表名称 = config_info_beta   */                          |
|      | /******************************************/                 |
|      | CREATE TABLE `config_info_beta` (                            |
|      | `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',        |
|      | `data_id` varchar(255) NOT NULL COMMENT 'data_id',           |
|      | `group_id` varchar(128) NOT NULL COMMENT 'group_id',         |
|      | `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',     |
|      | `content` longtext NOT NULL COMMENT 'content',               |
|      | `beta_ips` varchar(1024) DEFAULT NULL COMMENT 'betaIps',     |
|      | `md5` varchar(32) DEFAULT NULL COMMENT 'md5',                |
|      | `gmt_create` datetime NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间', |
|      | `gmt_modified` datetime NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间', |
|      | `src_user` text COMMENT 'source user',                       |
|      | `src_ip` varchar(20) DEFAULT NULL COMMENT 'source ip',       |
|      | `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',      |
|      | PRIMARY KEY (`id`),                                          |
|      | UNIQUE KEY `uk_configinfobeta_datagrouptenant` (`data_id`,`group_id`,`tenant_id`) |
|      | ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info_beta'; |
|      |                                                              |
|      | /******************************************/                 |
|      | /*   数据库全名 = nacos_config   */                          |
|      | /*   表名称 = config_info_tag   */                           |
|      | /******************************************/                 |
|      | CREATE TABLE `config_info_tag` (                             |
|      | `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',        |
|      | `data_id` varchar(255) NOT NULL COMMENT 'data_id',           |
|      | `group_id` varchar(128) NOT NULL COMMENT 'group_id',         |
|      | `tenant_id` varchar(128) DEFAULT '' COMMENT 'tenant_id',     |
|      | `tag_id` varchar(128) NOT NULL COMMENT 'tag_id',             |
|      | `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',     |
|      | `content` longtext NOT NULL COMMENT 'content',               |
|      | `md5` varchar(32) DEFAULT NULL COMMENT 'md5',                |
|      | `gmt_create` datetime NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间', |
|      | `gmt_modified` datetime NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间', |
|      | `src_user` text COMMENT 'source user',                       |
|      | `src_ip` varchar(20) DEFAULT NULL COMMENT 'source ip',       |
|      | PRIMARY KEY (`id`),                                          |
|      | UNIQUE KEY `uk_configinfotag_datagrouptenanttag` (`data_id`,`group_id`,`tenant_id`,`tag_id`) |
|      | ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info_tag'; |
|      |                                                              |
|      | /******************************************/                 |
|      | /*   数据库全名 = nacos_config   */                          |
|      | /*   表名称 = config_tags_relation   */                      |
|      | /******************************************/                 |
|      | CREATE TABLE `config_tags_relation` (                        |
|      | `id` bigint(20) NOT NULL COMMENT 'id',                       |
|      | `tag_name` varchar(128) NOT NULL COMMENT 'tag_name',         |
|      | `tag_type` varchar(64) DEFAULT NULL COMMENT 'tag_type',      |
|      | `data_id` varchar(255) NOT NULL COMMENT 'data_id',           |
|      | `group_id` varchar(128) NOT NULL COMMENT 'group_id',         |
|      | `tenant_id` varchar(128) DEFAULT '' COMMENT 'tenant_id',     |
|      | `nid` bigint(20) NOT NULL AUTO_INCREMENT,                    |
|      | PRIMARY KEY (`nid`),                                         |
|      | UNIQUE KEY `uk_configtagrelation_configidtag` (`id`,`tag_name`,`tag_type`), |
|      | KEY `idx_tenant_id` (`tenant_id`)                            |
|      | ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_tag_relation'; |
|      |                                                              |
|      | /******************************************/                 |
|      | /*   数据库全名 = nacos_config   */                          |
|      | /*   表名称 = group_capacity   */                            |
|      | /******************************************/                 |
|      | CREATE TABLE `group_capacity` (                              |
|      | `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID', |
|      | `group_id` varchar(128) NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群', |
|      | `quota` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配额，0表示使用默认值', |
|      | `usage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用量', |
|      | `max_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个配置大小上限，单位为字节，0表示使用默认值', |
|      | `max_aggr_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '聚合子配置最大个数，，0表示使用默认值', |
|      | `max_aggr_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值', |
|      | `max_history_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大变更历史数量', |
|      | `gmt_create` datetime NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间', |
|      | `gmt_modified` datetime NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间', |
|      | PRIMARY KEY (`id`),                                          |
|      | UNIQUE KEY `uk_group_id` (`group_id`)                        |
|      | ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='集群、各Group容量信息表'; |
|      |                                                              |
|      | /******************************************/                 |
|      | /*   数据库全名 = nacos_config   */                          |
|      | /*   表名称 = his_config_info   */                           |
|      | /******************************************/                 |
|      | CREATE TABLE `his_config_info` (                             |
|      | `id` bigint(64) unsigned NOT NULL,                           |
|      | `nid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,           |
|      | `data_id` varchar(255) NOT NULL,                             |
|      | `group_id` varchar(128) NOT NULL,                            |
|      | `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',     |
|      | `content` longtext NOT NULL,                                 |
|      | `md5` varchar(32) DEFAULT NULL,                              |
|      | `gmt_create` datetime NOT NULL DEFAULT '2010-05-05 00:00:00', |
|      | `gmt_modified` datetime NOT NULL DEFAULT '2010-05-05 00:00:00', |
|      | `src_user` text,                                             |
|      | `src_ip` varchar(20) DEFAULT NULL,                           |
|      | `op_type` char(10) DEFAULT NULL,                             |
|      | `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',      |
|      | PRIMARY KEY (`nid`),                                         |
|      | KEY `idx_gmt_create` (`gmt_create`),                         |
|      | KEY `idx_gmt_modified` (`gmt_modified`),                     |
|      | KEY `idx_did` (`data_id`)                                    |
|      | ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='多租户改造'; |
|      |                                                              |
|      |                                                              |
|      | /******************************************/                 |
|      | /*   数据库全名 = nacos_config   */                          |
|      | /*   表名称 = tenant_capacity   */                           |
|      | /******************************************/                 |
|      | CREATE TABLE `tenant_capacity` (                             |
|      | `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID', |
|      | `tenant_id` varchar(128) NOT NULL DEFAULT '' COMMENT 'Tenant ID', |
|      | `quota` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配额，0表示使用默认值', |
|      | `usage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用量', |
|      | `max_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个配置大小上限，单位为字节，0表示使用默认值', |
|      | `max_aggr_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '聚合子配置最大个数', |
|      | `max_aggr_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值', |
|      | `max_history_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大变更历史数量', |
|      | `gmt_create` datetime NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间', |
|      | `gmt_modified` datetime NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间', |
|      | PRIMARY KEY (`id`),                                          |
|      | UNIQUE KEY `uk_tenant_id` (`tenant_id`)                      |
|      | ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='租户容量信息表'; |
|      |                                                              |
|      |                                                              |
|      | CREATE TABLE `tenant_info` (                                 |
|      | `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',        |
|      | `kp` varchar(128) NOT NULL COMMENT 'kp',                     |
|      | `tenant_id` varchar(128) default '' COMMENT 'tenant_id',     |
|      | `tenant_name` varchar(128) default '' COMMENT 'tenant_name', |
|      | `tenant_desc` varchar(256) DEFAULT NULL COMMENT 'tenant_desc', |
|      | `create_source` varchar(32) DEFAULT NULL COMMENT 'create_source', |
|      | `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',         |
|      | `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',       |
|      | PRIMARY KEY (`id`),                                          |
|      | UNIQUE KEY `uk_tenant_info_kptenantid` (`kp`,`tenant_id`),   |
|      | KEY `idx_tenant_id` (`tenant_id`)                            |
|      | ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='tenant_info'; |
|      |                                                              |
|      | CREATE TABLE users (                                         |
|      | username varchar(50) NOT NULL PRIMARY KEY,                   |
|      | password varchar(500) NOT NULL,                              |
|      | enabled boolean NOT NULL                                     |
|      | );                                                           |
|      |                                                              |
|      | CREATE TABLE roles (                                         |
|      | username varchar(50) NOT NULL,                               |
|      | role varchar(50) NOT NULL,                                   |
|      | constraint uk_username_role UNIQUE (username,role)           |
|      | );                                                           |
|      |                                                              |
|      | CREATE TABLE permissions (                                   |
|      | role varchar(50) NOT NULL,                                   |
|      | resource varchar(512) NOT NULL,                              |
|      | action varchar(8) NOT NULL,                                  |
|      | constraint uk_role_permission UNIQUE (role,resource,action)  |
|      | );                                                           |
|      |                                                              |
|      | INSERT INTO users (username, password, enabled) VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', TRUE); |
|      |                                                              |
|      | INSERT INTO roles (username, role) VALUES ('nacos', 'ROLE_ADMIN'); |

#### application.properties 内容替换为

```properties
# spring
server.contextPath=/nacos
server.servlet.contextPath=/nacos
server.port=8848
management.metrics.export.elastic.enabled=false
management.metrics.export.influx.enabled=false
server.tomcat.accesslog.enabled=true
server.tomcat.accesslog.pattern=%h %l %u %t "%r" %s %b %D %{User-Agent}i
server.tomcat.basedir=
nacos.security.ignore.urls=/,/**/*.css,/**/*.js,/**/*.html,/**/*.map,/**/*.svg,/**/*.png,/**/*.ico,/console-fe/public/**,/v1/auth/login,/v1/console/health/**,/v1/cs/**,/v1/ns/**,/v1/cmdb/**,/actuator/**,/v1/console/server/**
spring.datasource.platform=mysql
db.num=1
db.url.0=jdbc:mysql://<ip>:<port>/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=root
db.password=password
123456789101112131415
```

### 4 退出容器

```shell
exit
1
```

### 5 重启容器

```shell
docker restart 8149bca96437
```