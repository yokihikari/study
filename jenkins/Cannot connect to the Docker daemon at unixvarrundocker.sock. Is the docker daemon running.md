After the installation of Jenkins and Docker. **Add jenkins user to dockergroup (like you did)**

```
sudo gpasswd -a jenkins docker
```

**Edit the following file**

```
vi /usr/lib/systemd/system/docker.service
```

And edit this rule to expose the API :

```
ExecStart=/usr/bin/docker daemon -H unix:// -H tcp://localhost:2375
```





安装docker后，使用docker命令一直正常。后来设置了root密码，系统重启动后，
执行 docker ps 命令时，报错了：

[root@iz2zei0x4t16rv0e5buzvhz /]# sudo docker ps
Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
1
2
问题原因：
系统重启动后，docker服务没有启动，所以在相应的 /var/run/ 路径下找不到docker的进程。

解决方法：
执行 service docker start 命令，启动docker服务。

然后再执行 docker ps 命令时，正常了。

[root@iz2zei0x4t16rv0e5buzvhz /]# docker ps
CONTAINER ID     IMAGE      COMMAND     CREATED     STATUS     PORTS     NAMES



