![img](https://img-blog.csdnimg.cn/20200330124817279.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

Jenkins可以实现自动化部署项目，如上图所示，jenkins可以连接代码管理平台，下载代码，进行项目打包及部署。下面我们基于docker部署jenkins，并实现项目的自动化部署。

1.安装启动jenkins
#1.拉取Jenkins镜像
[root@localhost ~]# docker pull jenkins

#2.启动jenkins容器
[root@localhost ~]#docker run  -it  -d  -p 8081:8081 -p 8080:8080 -p 50000:50000 -v jenkins_data:/var/jenkins_home jenkinsci/blueocean

查看启动日志

[root@localhost ~]# docker logs 4c193b44d91e >jenkins2.log 2>&1

启动成功后可以看到这里有显示admin用户的密码

![img](https://img-blog.csdnimg.cn/20200330124844718.png)

2.登录jenkins初始化配置
#1.登录Jenkins
 http://192.168.1.110:8080



1.输入默认生成的密码

![img](https://img-blog.csdnimg.cn/20200330124910123.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

#2.安装插件
选择安装插件，建议初学者安装推荐的插件

![img](https://img-blog.csdnimg.cn/20200330124923955.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)



![img](https://img-blog.csdnimg.cn/20200330125010345.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

等待所有插件安装完成，可能会耗时很久，而且会出现安装失败的情况。

选择系统管理Manage Jenkins—>选择插件管理Manage Plugins-->选择高级Advanced，修改Update Sie。【详细的Jenkins的镜像地址查询：http://mirrors.jenkins-ci.org/status.html】

![img](https://img-blog.csdnimg.cn/20200330125041437.png)



如果依然超时下载失败，可以自行下载.hpi文件，并上传

![img](https://img-blog.csdnimg.cn/20200330125056749.png)

详细的解决方案可以参考https://www.cnblogs.com/sxdcgaq8080/p/10489326.html

#3.创建用户

![img](https://img-blog.csdnimg.cn/20200330125128248.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

#4.配置jenkinsURL

![img](https://img-blog.csdnimg.cn/20200330125148384.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)


接着我们就进入了jenkins主页

3.配置jenkins运行环境
#1.配置jdk
进入主页，选择ManageJenkinss，选择Global Tool Configuration

![img](https://img-blog.csdnimg.cn/20200330125315885.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

选择JDK，填写jdk的名称及安装目录，取消勾选自动安装

![img](https://img-blog.csdnimg.cn/20200330125337511.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

Jenkins默认已经安装了jdk，进入Jenkins容器，可以查看Jdk版本，及安装目录

docker exec -it 8ed4f97783c8 /bin/bash

echo $JAVA_HOME

![img](https://img-blog.csdnimg.cn/20200330125352707.png)

#2.安装maven
选择maven安装配置项，填写相关信息

![img](https://img-blog.csdnimg.cn/20200330125408358.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

安装完成后，到jenkins容器中修改maven的settings.xml配置文件，配置阿里云镜像地址

![img](https://img-blog.csdnimg.cn/20200330125427139.png)

![img](https://img-blog.csdnimg.cn/20200330125450985.png)

#3.安装maven插件
选择manage jenkins-->选择Manage plugins，点击Available，搜索maven integration，选择安装后重启jenkins。

![img](https://img-blog.csdnimg.cn/20200330125510164.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)



4.使用jenkins自动部署项目
在配置自动部署前，我们需要先将项目部署到Git平台，如码云gitee、gitlab、github等。

#1.新建任务
选择新建任务New Item，输入项目名，选择maven项目

![img](https://img-blog.csdnimg.cn/20200330125646993.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

配置Git项目的地址

![img](https://img-blog.csdnimg.cn/20200330125657127.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

项目构建

![img](https://img-blog.csdnimg.cn/20200330125710517.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

配置项目自动运行脚本

![img](https://img-blog.csdnimg.cn/202003301257223.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

```

#!/bin/bash
#服务名称
SERVER_NAME=meite_mayi
# 源jar路径,mvn打包完成之后，target目录下的jar包名称，也可选择成为war包，war包可移动到Tomcat的webapps目录下运行，这里使用jar包，用java -jar 命令执行  
JAR_NAME=meite_mayi-1.0-SNAPSHOT
# 源jar路径  
#/usr/local/jenkins_home/workspace--->jenkins 工作目录
#demo 项目目录
#target 打包生成jar包的目录
JAR_PATH=/var/jenkins_home/workspace/meite_mayi/target/
# 打包完成之后，把jar包移动到运行jar包的目录--->work_daemon，work_daemon这个目录需要自己提前创建
JAR_WORK_PATH=/var/jenkins_home/workspace/meite_mayi/target/
 
echo "查询进程id-->$SERVER_NAME"
PID=`ps -ef | grep "$SERVER_NAME" |grep jar| awk '{print $1}'`
echo "得到进程ID：$PID"
echo "结束进程"
for id in $PID
do
	kill -9 $id  
	echo "killed $id"  
done
echo "结束进程完成"
 
#复制jar包到执行目录
echo "复制jar包到执行目录:cp $JAR_PATH/$JAR_NAME.jar $JAR_WORK_PATH"
cp $JAR_PATH/$JAR_NAME.jar $JAR_WORK_PATH
echo "复制jar包完成"
cd $JAR_WORK_PATH
#修改文件权限
chmod 755 $JAR_NAME.jar
 
BUILD_ID=dontKillMe nohup java -jar  $JAR_NAME.jar  &
```


### ***\**\*#2.为项目的端口配置映射\*\**\***

由于jenkins容器运行时只配置了自身的端口，如果项目部署在jenkins容器里面，容器外部也无法访问项目的端口。因此我们需要为项目的端口配置映射

第一种方法是停掉jenkins容器并删除后，重新启动jenkins容器，添加项目的端口

```
docker stop 1358f3f7ce75
docker rm 1358f3f7ce75
docker run  -it  -d  -p 8081:8081 -p 8080:8080 -p 50000:50000 -v jenkins_data:/var/jenkins_home  jenkinsci/blueocean
```



第二种方法是修改hostconfig.json和config.v2.json文件，添加项目的端口，然后重启docker

docker inspect 容器id，可以查看容器的完整id

然后进入到 /var/lib/docker/containers/[容器ID]/目录，可以看到hostconfig.json文件和config.v2.json文件
![img](https://img-blog.csdnimg.cn/20200330125945845.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

修改hostconfig.json文件，找到portbindings，增加项目的8081端口

![img](https://img-blog.csdnimg.cn/20200330130004414.png)

修改config.v2.json文件，增加项目端口8081，这个文件有两处需要修改，

![img](https://img-blog.csdnimg.cn/20200330130015250.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

修改完成后重启docker

### ***\**\*#3.立即构建项目\*\**\***

配置完成后，点击立即构建Build Now

![img](https://img-blog.csdnimg.cn/20200330130441545.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

选择本次构建，点击控制台输出Console Output，即可看到项目构建情况

![img](https://img-blog.csdnimg.cn/2020033013045893.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

![img](https://img-blog.csdnimg.cn/20200330130513591.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lpc2hpaHVha2Fp,size_16,color_FFFFFF,t_70)

项目启动完成后即可在外部进行访问了

![img](https://img-blog.csdnimg.cn/20200330130528974.png)




