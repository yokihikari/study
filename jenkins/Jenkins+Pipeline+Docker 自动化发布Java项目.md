# 写在前面：

> 在前面的博客中写过流水线发布Java后端项目，但还没有涉及到容器操作，这一篇就是写关于流水线发布docker 镜像部署过程

# 部署环境

| 主机名  | IP地址          | 部署软件                         | 内存 |
| ------- | --------------- | -------------------------------- | ---- |
| harbor  | 192.168.154.129 | docker、docker-compose、harbor   | 2G   |
| gitlab  | 192.168.154.131 | gitlab 、git、mysql              | 3G   |
| jenkins | 192.168.154.128 | jdk、git、jenkins、docker、maven | 2G   |

# harbor服务器上部署：

## 一、搭建harbor仓库

### 1.安装harbor仓库，上传tomcat镜像

> 参考博客 https://blog.51cto.com/13760351/2532347

## 二、搭建数据库MySQL

### 1.安装mysql5.7

> 参考博客 https://blog.51cto.com/13760351/2497200

### 2.登录数据库，授权

> mysql -uroot -pMypass@123#!

```
mysql> grant all privileges on *.* to 'root'@'%' identified by 'Mypass@123#!' with grant option; #设置权限
mysql> flush privileges;
```

### 2.导入数据

> create database quartz
> source /etc/quartz.sql

### 3.查看表

> use quartz;
> show tables;

![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/8cff065a7299c2f18d0590895a554649.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

**注意：**

> 若出现不识别表名或库名，可能是大小写的问题

**解决方案：**

> vim /etc/my.cnf

```
#忽略大小写
lower_case_table_names=1  
```

![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/818fd10f8ba9afa11275ec9a301cacd3.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

### 重启数据库

> systemctl restart mysqld

# gitlab服务器上部署

## 搭建gitlab仓库

### 1.安装gitlab仓库

> 参考博客 https://blog.51cto.com/13760351/2467477

### 2.上传java代码到gitlab仓库

> 参考博客 https://blog.51cto.com/13760351/2526052

# Jenkins服务器上部署

## 一、搭建Jenkins服务器

### 1.安装Jenkins服务

> 参考博客 https://blog.51cto.com/13760351/2368452

### 2.安装maven插件及配置

> 参考博客 https://blog.51cto.com/13760351/2527263

### 3.安装docker

#### 1.添加docker-ce源

> cd /etc/yum.repos.d
> wget http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

#### 2.安装docker

> yum -y install docker-ce

#### 3.启动docker

> systemctl start docker

## 二、编写流水线脚本

![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/9158460198480f2a92af6c83920a46b7.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

### 1.编写Jenkinsfile

```
pipeline {

  agent {
    node {
      label 'master'
    }
  }
    // 拉取代码
  stages {
    stage('GetCode') {
      steps {
        sleep 5
      }
    }

    // 代码打包
    stage('Maven Build') {
        steps {
          sh '''
          export JAVA_HOME=/usr/local/java
          /usr/local/apache-maven-3.6.3/bin/mvn clean install -Ptest
          '''
        }
    }

    // 推送到镜像仓库
    stage('Push Image') {
      steps {
      sh '''
        REPOSITORY=192.168.154.129/maven-test01/maven-quartz:${branch}
                //创建镜像
        docker build -t $REPOSITORY .
                //登录，上传镜像到仓库
        docker login 192.168.154.129 -u admin -p Harbor12345
        docker push $REPOSITORY
        '''
      }
    }

    // 部署到Docker主机
    stage('Deploy to Docker') {
      steps {
        sh '''
                //推送shell脚本到docker主机
        scp images.sh root@harbor:/root/ 
                //远程执行脚本
        ssh -p 22 root@harbor "chmod +x images.sh && sh /root/images.sh"
        '''
      }
    } 
  }
}
```

### 2.编写Dockerfile

```
#从harbor拉取镜像
FROM 192.168.154.129/maven-test01/tomcat:v1
#镜像构建人信息（可选）
MAINTAINER zhao
#删除webapps目录下文件
RUN rm -rf /usr/local/tomcat/webapps/*
#复制打的最新war包到webapps目录
ADD target/quartz.war /usr/local/tomcat/webapps/
```

### 3.编写镜像shell脚本

```
#!/bin/bash
Img=`docker images |grep master |awk -F ' '  '{ print $3}'`
Con=`docker ps |grep :88 |awk -F ' '  '{ print $1}'`
#删除旧的镜像
docker stop $Con 
docker rmi -f $Img
#下载新镜像，运行
docker pull 192.168.154.129/maven-test01/maven-quartz:master
sleep 2
#后台运行容器
docker run -d -p 88:8080 192.168.154.129/maven-test01/maven-quartz:master
```

## 三、新建项目pipline-docker

![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/5cb1a6034010c2d30fa0839507c54717.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)
![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/6c45f2ae9567873c97fcfdadbd9bb915.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/88cd93dd71b91b9c68672f25b38b4766.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

## 四、构建测试

![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/a8aa10dc947df2183f299b98d39d9f97.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)
![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/2f3b98605b8d169432b1fde354149e14.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

## 五、查看新镜像

![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/af62c7157904c19497eda19948706b65.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

**温馨提示：**

> 同一标签的镜像上传后，会自动覆盖之前镜像，不用特地删除旧的镜像！

## 六、查看容器运行

> docker ps

![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/13/2606888a67214b9a8207613d1c4d334c.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

## 七、java项目测试

![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/5c3c36c26a837df2100197993294974e.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)
![[ 企业实战！] Jenkins+Pipeline+Docker 自动化发布Java项目](https://s4.51cto.com/images/blog/202009/12/6dfbc948e6251de656dc8f83b7f7325b.png?x-oss-process=image/watermark,size_16,text_QDUxQ1RP5Y2a5a6i,color_FFFFFF,t_100,g_se,x_10,y_10,shadow_90,type_ZmFuZ3poZW5naGVpdGk=)

## 总结

> 1.这里其实就是修改了3个脚本文件，前提是在其他环境都部署好的条件下。
> 2.为了避免文章篇幅过长，好多步骤，只给了博客链接地址，仅供参考，如果公司已经有环境，可以不用理会。
>
> 1. 参考文档 ：https://blog.51cto.com/13043516/2365284