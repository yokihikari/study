 项目需求要用到javejar包，不得一在网上查资料 结果发现它这个还不是仓库(maven)的，只能通过手动引入。无奈之下又上度娘查了好多人技术博客 开始看的很蒙蔽。不多说下面的操作一番就清楚了

**第一步 下载好需要的jar包存到一个文件夹**

[![maven项目中引入第三方的jave-1.0.2jar包](http://static.zuidaima.com/images/241622/201906/20190610142428457_w_650x100.jpg)](http://static.zuidaima.com/images/241622/201906/20190610142428457_w.jpg)我的jave-1.0.2  地址为D:\yingyong\jave-1.0.2

**第二步 通过cmd进入到maven中的bin目录下**

本人电脑中的maven地址以下

D:\Program Files\apache-maven-3.5.4\bin>

**第三步 通过mvn下载的方式将本地下好的包下到本地仓库中去**

mvn install:install-file -Dfile=D:\yingyong\jave-1.0.2\jave-1.0.2.jar  -DgroupId=it.sauronsoftware -DartifactId=jave -Dversion=1.0.2  -Dpackaging=jar

**第四步 以下提示说明将第三方过的jar包下到仓库里了**

[![maven项目中引入第三方的jave-1.0.2jar包](http://static.zuidaima.com/images/241622/201906/20190610142958124_w_650x140.jpg)](http://static.zuidaima.com/images/241622/201906/20190610142958124_w.jpg)

**第五步 将对应的jar包信息添加到maven项目中的pom文件里**

**[![maven项目中引入第三方的jave-1.0.2jar包](http://static.zuidaima.com/images/241622/201906/20190610143127991_w_650x70.jpg)](http://static.zuidaima.com/images/241622/201906/20190610143127991_w.jpg)**

虽说简单步骤 但是我自己挤破脑袋弄好的。以此将自己的经历和知识点在渊博下。